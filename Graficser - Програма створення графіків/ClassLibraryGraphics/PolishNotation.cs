﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace ClassLibraryGraphics
{  [Serializable]
   public class PolishNotation
    {

        protected string Formula;
        // заходить назва функції... повертає спец символ для заміни....
        protected string NewSymbol(string fanc){
            switch(fanc)
            {
                case "log": return "z";
                case "-": return "w";
                case"sqrt": return "a";  
                case"cos": return "b"; 
                case"sin":  return "c";
                case "tg": return "d"; 
                case"ctg": return "f"; 
                case"arcsin": return "g";
                case "arccos":return "h";
                case"arctg":return "i"; 
                case "arcctg":return "j"; 
                case "ln":return "k";
                case "lg":return "l"; 
                case "e":return "m"; 
                case "sh": return "n";
                case "th": return "o";
                case "cth": return "q";
                case "abs": return "r";
                case "ch": return "y";
            }
            return "0";
        }
       // перевірка на функцію або число......
       protected bool IsNumber(string a)
        {
            Regex x = new Regex(@"[a-z0-9.]");
            Match tmp;
            tmp = x.Match(a);
            if (tmp.Success) return true;
            else return false;     
        }
        // приходить регулярка, формула, символ операції на який замінюємо....
        //замінюємо функції на певні символи-операцій для подальшого переводу в польський запис.....
        private string  SignChange(string formula,string regular,string size){
            string tmp = formula;
            string arg = "0";
            string viraz = tmp;
            int LeftBracket = 0;
            int RightBracket = 0;
            string s = "0";
            for (int i = 0; i < 10; i++)
            {
                Regex regex = new Regex(@regular);
                Match m = regex.Match(tmp);
                while (m.Success)
                {
                    s = m.Value;
                    s = s.Substring(size.Length);
                    for (int j = 0; j < s.Length; j++)
                    {   
                        arg = s;
                        arg = arg.Substring(j, 1);
                        if (arg == "(") ++LeftBracket;
                        if (arg == ")") ++RightBracket;
                        if (LeftBracket == RightBracket && LeftBracket != 0)
                        {
                            if (j + 1 == s.Length) s = s + NewSymbol(size);
                            else
                            {
                              
                                s = s.Insert(j+1, NewSymbol(size));
                                LeftBracket = 0;
                                RightBracket = 0;
                            }
                            break;
                        }
                    }

                    tmp = tmp.Replace(m.Value, s);
                    m = m.NextMatch();
                }

            }
            return tmp;
        }
     //  унарні мінуси заміняємо на спец символ і піготовка для калькулятора
        private string ChangeUnaryMinus(string formula)
        {
            Match tmp;
            string temp;
            int LeftBracket = 0;
            int RightBracket = 0;
            string arg;
            for (;;)
            {
                Regex minus = new Regex(@"(^-\(.+\))|(\(-.+\))|([^x)+\-/*0-9](-[0-9.]+))");
                tmp = minus.Match(formula);
                temp = tmp.Value;
                if (tmp.Success)
                {
                    for (int j = 0; j < temp.Length - 1; j++)
                    {
                        temp = tmp.Value;
                        arg = tmp.Value;
                        temp = temp.Substring(j, 2);
                        if (temp == "(-")
                        {
                            for (int k = j; k < tmp.Value.Length; k++)
                            {
                                temp = tmp.Value;
                                temp = temp.Substring(k, 1);
                                if (temp == "(") ++LeftBracket;
                                if (temp == ")") ++RightBracket;
                                if (LeftBracket == RightBracket && LeftBracket != 0)
                                {
                                    if (k + 1 == tmp.Value.Length) { arg = arg + NewSymbol("-"); }
                                    else
                                    {
                                        arg = arg.Insert(k + 1, NewSymbol("-"));
                                    }
                                    arg = arg.Substring(2);
                                    arg = "(" + arg;
                                    formula = formula.Replace(tmp.Value, arg);
                                    break;
                                }
                            }
                        }
                        else
                            if (temp == "-(" && j == 0)
                            {

                                for (int k = j; k < tmp.Value.Length; k++)
                                {
                                    temp = tmp.Value;
                                    temp = temp.Substring(k, 1);
                                    if (temp == "(") ++LeftBracket;
                                    if (temp == ")") ++RightBracket;
                                    if (LeftBracket == RightBracket && LeftBracket != 0)
                                    {
                                        arg = arg.Insert(k + 1, NewSymbol("-"));
                                        arg = arg.Substring(1);
                                        formula = formula.Replace(tmp.Value, arg);
                                        break;
                                    }
                                }
                            }
                    }
                }
                else break;
            }
            return formula;
        }
    // метод відловлює операції і викликає метод зміну назви функції на спец символ
         private string CompareFunction(string formula){
             Match tmp;
             Regex loog = new Regex(@"log\((.+);(.+)\)");
             tmp = loog.Match(formula);
             string arg;
             if (tmp.Success)
             {
                 formula = SignChange(formula, @"log\((.+);(.+)\)", "log");
                 Calculator a = new Calculator(tmp.Groups[1].Value);
                 Calculator b = new Calculator(tmp.Groups[2].Value);
                 arg = formula;
                 arg= arg.Replace(tmp.Groups[1].Value, a.GetRes().ToString());
                 arg = arg.Replace(tmp.Groups[2].Value, b.GetRes().ToString());
                 formula = arg;

             }
             // шукаємо унарні мінуси
             formula = ChangeUnaryMinus(formula);
             // шукаємо abs
             Regex abs = new Regex(@"abs");
             tmp = abs.Match(formula);
             if (tmp.Success)
             {
                 formula = SignChange(formula, @"(abs\([^()]*\))|abs(\(.+\))", "abs");
             }
             // шукаємо cth
             Regex cth = new Regex(@"cth");
             tmp = cth.Match(formula);
             if (tmp.Success)
             {
                 formula = SignChange(formula, @"(cth\([^()]*\))|cth(\(.+\))", "cth");
             }
             // шукаємо th
             Regex th = new Regex(@"th");
             tmp = th.Match(formula);
             if (tmp.Success)
             {
                 formula = SignChange(formula, @"(th\([^()]*\))|th(\(.+\))", "th");
                 
             }
             // шукаємо сh 
             Regex ch = new Regex(@"ch");
             tmp = ch.Match(formula);
             if (tmp.Success)
             {
                 formula = SignChange(formula, @"(ch\([^()]*\))|sh(\(.+\))", "ch");

             }
             // шукаємо sh
             Regex sh = new Regex(@"sh");
             tmp = sh.Match(formula);
             if (tmp.Success)
             {
                 formula = SignChange(formula, @"(sh\([^()]*\))|sh(\(.+\))", "sh");
                
             }
             // експоненту до степені
             Regex exp = new Regex(@"e");
             tmp = exp.Match(formula);
             if (tmp.Success)
             {
                 formula = SignChange(formula, @"(e\([^()]*\))|e(\(.+\))", "e");
             }
             // шукаємо десятковий логорифм
             Regex lg = new Regex(@"lg");
             tmp = lg.Match(formula);
             if (tmp.Success)
             {
                 formula = SignChange(formula, @"(lg\([^()]*\))|lg(\(.+\))", "lg");
             }
             //шукаэмо натуральний логорифм
             Regex ln = new Regex(@"ln");
             tmp = ln.Match(formula);
             if (tmp.Success)
             {
                 formula = SignChange(formula, @"(ln\([^()]*\))|ln(\(.+\))", "ln");
             }
             // шукаємо арккотангенс
             Regex arcctg = new Regex(@"arcctg");
             tmp = arcctg.Match(formula);
             if (tmp.Success)
             {
                 formula = SignChange(formula, @"(arcctg\([^()]*\))|arcctg(\(.+\))", "arcctg");
             }
             //шукаємо арктангенс
             Regex arctg = new Regex(@"arctg");
             tmp = arctg.Match(formula);
             if (tmp.Success)
             {
                 formula = SignChange(formula, @"(arctg\([^()]*\))|arctg(\(.+\))", "arctg");
             }
             //шукаємо арккосинус
             Regex arccos = new Regex(@"arccos");
             tmp = arccos.Match(formula);
             if (tmp.Success)
             {
                 formula = SignChange(formula, @"(arccos\([^()]*\))|arccos(\(.+\))", "arccos");
             }
             // шукаэмо арксинус
             Regex arcsin = new Regex(@"arcsin");
             tmp = arcsin.Match(formula);
             if (tmp.Success)
             {
                 formula = SignChange(formula, @"(arcsin\([^()]*\))|arcsin(\(.+\))", "arcsin");
             }
             // шукаємо котангенс
             Regex ctg = new Regex(@"ctg");
             tmp = ctg.Match(formula);
             if (tmp.Success)
             {
                 formula = SignChange(formula, @"(ctg\([^()]*\))|ctg(\(.+\))", "ctg");
             }
             // шукаємо тангенс
             Regex tg = new Regex(@"tg");
             tmp = tg.Match(formula);
             if (tmp.Success)
             {
                 formula = SignChange(formula, @"(tg\([^()]*\))|tg(\(.+\))", "tg");
             }
             //шукаємо sqrt
             Regex sqrt = new Regex(@"sqrt");
                tmp = sqrt.Match(formula);
                if (tmp.Success)
                {
                    formula = SignChange(formula, @"(sqrt\([^()]*\))|sqrt(\(.+\))", "sqrt");
                }
             // шукаємо cos
                Regex cos = new Regex(@"cos");
                tmp = cos.Match(formula);
                if (tmp.Success)
                {
                    formula = SignChange(formula, @"(cos\([^()]*\))|cos(\(.+\))", "cos");
                }
                // шукаємо sin
                Regex sin = new Regex(@"sin");
                tmp = sin.Match(formula);
                if (tmp.Success)
                {
                    formula = SignChange(formula, @"(sin\([^()]*\))|sin(\(.+\))", "sin");
                }
                return formula;
         }
        // спочатку переводимо значення унарних функцій в спеціальні знаки і підготовлюємо до переводу в польський запис
        public PolishNotation(string formula)
        {
          TransformationFormula(formula);
            }
        public PolishNotation(PolishNotation obj)
        {
            Formula = obj.Formula;
        }
        public PolishNotation()
        {
            Formula = "1";
        }
      // метод для трансформації виразу в польський запис
       protected void TransformationFormula(string formula){
            formula=CompareFunction(formula);
            /// перевод числа в польський зворот...
           string tmp = formula;
           string fin = "";
           string arg = "0";
           Stack<string> stack = new Stack<string>();
           string a = "1";
           for (int i = 0; i < tmp.Length; i++)
           {
               arg = tmp;
               arg = arg.Substring(i, 1);
               if (i < tmp.Length - 1 && IsNumber(arg) && IsNumber(tmp.Substring(i + 1, 1)) == false) fin = fin + arg + ",";
               else
                   if (IsNumber(arg)) fin = fin + arg;
                   else
                       if (arg == "(") stack.Push(arg);
                       else
                           if (arg == "/" || arg == "*")
                           {
                               if (stack.Count > 0)
                               {
                                   a = stack.Pop();
                                   if (a == "/" || a == "*" || a == "^")
                                       fin = fin + a;
                                   else stack.Push(a);
                                   stack.Push(arg);
                               }
                               else { stack.Push(arg); }
                           }
                           else
                               if (arg == "^")
                               {
                                   while (true)
                                   {
                                       if (stack.Count != 0)
                                           a = stack.Pop();
                                       else break;
                                       if (a == "^")
                                       {
                                           fin = fin + a;

                                       }
                                       else { stack.Push(a); break; }
                                   }
                                   stack.Push(arg);
                               }
               if (arg == "+" || arg == "-")
               {
                   while (true)
                   {
                       if (stack.Count != 0)
                           a = stack.Pop();
                       else break;
                       if (a == "+" || a == "-" || a == "*" || a == "/" || a == "^")
                       {
                           fin = fin + a;

                       }
                       else { stack.Push(a); break; }
                   }
                   stack.Push(arg);
               }

               else
                   if (arg == ")")
                   {
                       while (true)
                       {
                           a = stack.Pop();
                           if (a != "(") { fin = fin + a; }
                           else { break; }
                       }
                   }

           }
           if (stack.Count > 0)
           {
               for (int i = 0; i < stack.Count + 1; i++)
               {
                   fin = fin + stack.Pop();
               }
           }
           Formula = fin;
          
       }

          public string GetFormula()
        {
            return Formula;
        }
          public void SetFormula(string formula){
            TransformationFormula(formula);
          }
    }
 
}
