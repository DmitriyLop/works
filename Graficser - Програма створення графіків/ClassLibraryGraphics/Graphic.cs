﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibraryGraphics;
using System.Windows;
using System.Windows.Input;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Drawing;



//Коректор формули(Перевірка формули і коректування);
//График(формулу,калькулятор, координати центра, горизонтальну вісь, 
//вертикальну вісь, точки графіка);
//Горизонтальна вісь(точка центра, макс і мін значення,колір осі );
//Вертикальна вісь(точка центра, макс і мін значення, колір осі);
//Промальовщик(точки,промальовка осей);
namespace ClassLibraryGraphics
{

    [Serializable]
    public class Graphic
    {
       protected List<double> X = new List<double>();
       protected List<double> Y = new List<double>();
     
       protected Color ColorGraf = Color.FromName("Black");
       protected Color ColorAxes = Color.FromName("Black");
       protected PolishNotation Formula;
       public int WidthGrafLines { get; set; }
       public int WidthAxes { get; set; }
       public string NameX {get;set;}
       public string NameY { get; set; }
       public double MinValueX { get; set; }
       public double MaxValueX { get; set; }
       public double MinValueY { get; set; }
       public double MaxValueY{ get; set; }
       public int NumberPoints { get; set; }
       public void SetColorAxes(int r,int g,int b)
       {
           ColorAxes = Color.FromArgb(r,g,b);
       }
       public List<byte> GetColorAxes()
       {
           List<byte> RGB = new List<byte>();
           RGB.Add(ColorAxes.R);
           RGB.Add(ColorAxes.G);
           RGB.Add(ColorAxes.B);
           return RGB;
       }
       public void SetColorGraf(int r,int g,int b)
       {
           ColorGraf = Color.FromArgb(r, g, b);
       }
       public List<byte> GetColorGraf()
       {
           List<byte> RGB = new List<byte>();
           RGB.Add(ColorGraf.R);
           RGB.Add(ColorGraf.G);
           RGB.Add(ColorGraf.B);
           return RGB;
       }
       public List<double> GetX()
       {
           return X;
       }
       public List<double> GetY(){
            return Y;
    }
       public double Step { get; set;}
       public bool GrafIsLinear{get;set;}
       public Graphic()
       {
           Formula = new PolishNotation("2");
           NameX = "X";
           NameY = "Y";
           MinValueX = -10;
           MaxValueX = 10;
           MinValueY = -10;
           MaxValueY = 10;
           NumberPoints = 100;
           Step = (Math.Abs(-10) + Math.Abs(10)) / (double)100;
           // заповнюэмо Х
           double tmp = -10;
           for (int i = 0; i < 100; i++)
           {
               X.Add(tmp);
               tmp += Step;
           }
           Calculator calc = new Calculator();
           for (int i = 0; i < 100; i++)
           {
               calc.Calc(Formula.GetFormula(), X[i]);
           }
           Y = calc.GetResult();
       }
       public Graphic(Graphic obj)
       {
           NameY = obj.NameY;
           NameX = obj.NameX;
           ColorAxes = obj.ColorAxes;
           ColorGraf = obj.ColorGraf;
           MinValueX = obj.MinValueX;
           MaxValueX = obj.MaxValueX;
           MinValueY = obj.MinValueY;
           MaxValueY = obj.MaxValueY;
           NumberPoints = obj.NumberPoints;
           Step = obj.Step;
           X = obj.X;
           Y = obj.Y;
           GrafIsLinear = obj.GrafIsLinear;
           WidthGrafLines = obj.WidthGrafLines;
           WidthAxes = obj.WidthAxes;
           Formula = obj.Formula;

       }
       public Graphic(string nameX,string nameY,double minValueX,double maxValueX,double minValueY,double maxValueY,int numberPoints,string formula,bool grafIsLeaner){
          WidthAxes = 4;
          WidthGrafLines = 4;
          NameX = nameX;
          NameY = nameY;
          MinValueX = minValueX;
          MaxValueX = maxValueX;
          MinValueY = minValueY;
          MaxValueY = maxValueY;
          CalcNumberPoints();
          Formula = new PolishNotation(formula);
          GrafIsLinear = grafIsLeaner;
           // рахуємо точки на графіку
          CountPoints();
      }
         
        protected void CountPoints(){
             Step = (Math.Abs(MinValueX) + Math.Abs(MaxValueX)) /(double) NumberPoints;
          // заповнюэмо Х
          double tmp=MinValueX;
          CalcNumberPoints();
          for (int i = 0; i < NumberPoints; i++)
          {
              X.Add(tmp);
              tmp += Step;
          }
          Calculator calc = new Calculator();
          for (int i = 0; i < NumberPoints; i++)
          {
              calc.Calc(Formula.GetFormula(), X[i]);
          }
          Y = calc.GetResult();
        }
        public void NewPoints(int minX,int maxX){
            MinValueX = minX;
            MaxValueX = maxX;
            CalcNumberPoints();
            ////}
            X.Clear();
            CountPoints();
        }
       protected void CalcNumberPoints(){
           if (MaxValueX < 30) NumberPoints = 800;
           if (MaxValueX < 50) NumberPoints = 1000;
            else if (MaxValueX <= 100) NumberPoints = 1100;
            else if (MaxValueX <= 200) NumberPoints = 1200;
            else NumberPoints = 1300;
        }
    }
}
