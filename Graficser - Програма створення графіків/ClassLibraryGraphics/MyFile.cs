﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using ClassLibraryGraphics;
using System.IO;
namespace ClassLibraryGraphics
{
   public class MyFile
    {
        public string Link{get;set;}
       public MyFile()
        {

        }
       public MyFile(string link){
           Link = link;
       }
       public MyFile(MyFile obj)
       {
           Link = obj.Link;
       }
       public void SaveFile(Graphic a)
       {
           Graphic graf = new Graphic(a);
           // создаем объект BinaryFormatter
           BinaryFormatter formatter = new BinaryFormatter();
           // получаем поток, куда будем записывать сериализованный объект
           using (FileStream fs = new FileStream(Link, FileMode.OpenOrCreate))
           {
               formatter.Serialize(fs, graf);
           }
       }
       public Graphic OpenFile() {
           BinaryFormatter formatter = new BinaryFormatter();
           using (FileStream fs = new FileStream(Link, FileMode.OpenOrCreate))
           {
               try
               {
                   Graphic graf = (Graphic)formatter.Deserialize(fs);
                   return graf;
               }
               catch { return null; }
           }
       }
    }
}
