﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using ClassLibraryGraphics;

namespace ClassLibraryGraphics
{
    public class CheckingFormula
    {
      protected  string Formula;
        public CheckingFormula(string formula)
        {
          Formula = PreparationFormula(formula);
        }
        public CheckingFormula(CheckingFormula obj)
        {
            Formula = obj.Formula;
        }
        public string GetFormula()
        {
            return Formula;
        }
        
        public string PreparationFormula(string formula)
        {
           // переводимо до малого регістру;
            formula = formula.ToLower();
            // забираємо непотрібні пробіли
            formula = formula.Replace(" ","");
           
            // доставляємо множення біля X
            formula = formula.Replace(")x", ")*x");
            formula = formula.Replace("x(", "x*(");
            string tmp;
            Regex x = new Regex(@"(([0-9.]*[^.+\-/*(])x)");
            Match mm;
            mm = x.Match(formula);
            while(mm.Success) {
                if (mm.Groups[1].Value != null) {
                    tmp = mm.Groups[1].Value;
                    tmp = tmp.Replace("x","*x");
                    //tmp = CheckOperationMull(tmp);
                    formula = formula.Replace(mm.Groups[1].Value,tmp);
                }
                mm = mm.NextMatch();
            }
           
            return formula;
        }
        // перевірка на те, щоб першим ніколи не була операція ділення або множення
        protected string CheckOperationMull(string formula)
        {
            if (formula.Substring(0, 1) == "*" || formula.Substring(0, 1) == "/") return formula.Substring(1);

            else return formula;
           
        }
        // Загальна перевірка.... Якщо виникає помилка в калькуляторі то переписати
        public bool CheckFinish()
        {
            
            
            Regex x = new Regex(@"x[0-9]");
            Match mm;
            mm = x.Match(Formula);
            if (mm.Success) { return true; }
            else
            {
                try
                {
                    Calculator calc = new Calculator();
                    PolishNotation formula = new PolishNotation(Formula);
                    calc.Calc(formula.GetFormula(), 1);
                }
                catch
                {
                    return true;
                }
            }
            return false;
        }
        // перевірка на те чи э дужки біля функцій кос і тд
        public bool BracketsIsNotWithFunc()
        {
            Regex l = new Regex(@"(abs[^(])|(log[^(])|(cth[^(])|(th[^(])|(sh[^(])|(exp[^(])|(lg[^(])|(ln[^(])|(arcctg[^(])|(arccos[^(])|(arcsin[^(])|(ctg[^(])|(tg[^(])|(sqrt[^(])|(cos[^(])|(sin[^(])");
            Match mm;
            mm = l.Match(Formula);
            if (mm.Success) { return true; }
            else return false;
        }
        // перевірка для логорифма
        public bool CheckLog()
        {
            Regex l = new Regex(@"log");
            Match mm;
            mm = l.Match(Formula);
            if (mm.Success == false) { return false; }
            
            Regex llg = new Regex(@"(log\((.*x.*);(.*x.*)\))|(log\((.*);(.*x.*)\))|(log\((.*x.*);(.*)\))");
            Match m;
            m = llg.Match(Formula);
            if (m.Success) { return true; }
            else
            {
                // перевірка на від'ємні числа
                Regex ln = new Regex(@"log\(([()0-9+\-/*]+);([()0-9+\-/*]+)\)");
                Match match;
                match = ln.Match(Formula);
                double x1 = double.MinValue;
                double x2 = double.MinValue;
                if (match.Success)
                {
                    try
                    {
                        Calculator a = new Calculator(match.Groups[1].Value);
                        x1 = a.GetRes();
                        Calculator b = new Calculator(match.Groups[2].Value);
                        x2 = b.GetRes();

                    }
                    catch
                    {
                        return true;
                    }
                }
                else
                    if (x1 < 0 || x1 == 1 || x2 < 0) return true;
                    else return false;
            }
            return false;
        }
        // перевірка на те , що нат. лог.  0 (дає безкінечність)
        public bool LnIsNull()
        {
            Regex ln = new Regex(@"(ln\(0\))");
            if (ln.IsMatch(Formula)) { return true; }
            else return false; 
        }
        // перевірка на символи, які не відносяться до формули
        public bool OtherSymbols(){
            Regex symbol = new Regex(@"[^+(0-9cosib.sh;a^nx+\-/*qrtglep)]");
            if (symbol.IsMatch(Formula)) { return true; }
            else return false;
        }
        // перевірка чи операції плюс, мінус, ділення не зустрічаються разом більше 1 разуу
        public bool ManySize()
        {
            Regex size = new Regex(@"[\*/\+-]{2,100}");
            if (size.IsMatch(Formula)) { return true; }
            else return false;
        }
        // перевірка чи всі дужки
        public bool AllBrackets()
        {
            int leftBrackets = 0;
            int rightBracket = 0;
            string arg = "";
            for (int i = 0; i < Formula.Length; i++)
            {
                arg = Formula;
                arg = arg.Substring(i, 1);
                if (arg == "(") ++leftBrackets;
                if (arg == ")") ++rightBracket;
                if (rightBracket > leftBrackets) return false;
            }
            if (leftBrackets == rightBracket) return true;
            else return false;
        }

    }
    
}
