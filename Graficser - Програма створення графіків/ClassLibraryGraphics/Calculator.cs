﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryGraphics
{
   public class Calculator : PolishNotation
    {
       List<double> Result = new List<double>();
       public Calculator() 
       :base("1+1")
       { }
        // конструктор приймає лише вирази без змінних
        public Calculator(string formula)
        :base(formula)
        {
            Calculation();
        }
        public Calculator(Calculator obj)
        {
            Result = obj.Result;
        }
       // метод замінює х на значення яке передається і викликається підрахунок виразу
        public List<double> Calc(string formula,double x)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            // похибка поблизу Х
            if (Math.Abs(x) < 0.0001) x = 0;
            if (x <0)
            {
                x = x * (-1);
                Formula = formula = formula.Replace("x", x.ToString() + ",w,");
            }
            else
            {
                Formula = formula = formula.Replace("x", x.ToString() + ",");
            }
            Calculation();
            return Result;
        }
        // обрахунок польського виразу
        protected void Calculation()
        {
            
            double x1 = 1, x2 = 1;
            double res = 0;
            string arg;
            string tmp;
            Stack<double> stac = new Stack<double>();
            bool flagNumber = false;
            for (int i = 0; i < Formula.Length; i++)
            {   

                arg =Formula;
                arg = arg.Substring(i, 1);
                if (arg == ",") continue;
                if (arg=="0"||arg=="1"||arg=="2"||arg=="3"||arg=="4"||arg=="5"||arg=="6"||arg=="7"||arg=="8"||arg=="9")
                {
                    tmp = arg;
                    for (int j=i+1; j < Formula.Length;j++)
                    {
                        arg = Formula;
                        arg = arg.Substring(j, 1);
                        if (IsNumber(arg)==false|| arg==",") {
                            if (j + 1 == Formula.Length) { i = j - 1; break; }
                            else { i = j; break; }
                        }
                        tmp = tmp + arg;
                    }
                    if (tmp.Length == Formula.Length) { Result.Add(double.Parse(tmp)); flagNumber = true; break; }
                    else
                        stac.Push(double.Parse(tmp));
                }
                    
                else
                {
                    if (arg=="/"|| arg=="*"||arg=="+"||arg=="-"||arg=="^"|| arg=="z")
                    {
                        x1 = stac.Pop();
                        x2 = stac.Pop();
                    }
                    else { x1 = stac.Pop(); }
                    switch (arg)
                    {
                        case "y": res = (Math.Exp(x1)+Math.Exp(-x1))/2; break;
                        case "z": res = Math.Log(x2) / Math.Log(x1); break;
                        case "w": res = x1 * (-1); break;
                        case "r": res = Math.Abs(x1); break;
                        case "q": res = (1 / Math.Tanh(x1)); break;
                        case "o": res = Math.Tanh(x1); break;
                        case "n": res = Math.Sinh(x1); break;
                        case "m": res = Math.Exp(x1); break;
                        case "l": res = Math.Log10(x1); break;
                        case "k": res = Math.Log(x1); break;
                        case "j": res = (Math.PI / 2) - Math.Atan(x1); break;
                        case "i": res = Math.Atan(i); break;
                        case "h": res = Math.Acos(x1); break;
                        case "g": res = Math.Asin(x1); break;
                        case "f": res = Math.Cos(x1) / Math.Sin(x1); break;
                        case "d": res = Math.Tan(x1); break;
                        case "c": res = Math.Sin(x1); break;
                        case "a": res = Math.Sqrt(x1); break;
                        case "b": res = Math.Cos(x1); break;
                        case "+": res = x2 + x1; break;
                        case "-": res = x2 - x1; break;
                        case "/": res = x2 / x1; break;
                        case "*": res = x2 * x1; break;
                        case "^": res = Math.Pow(x2, x1); break;

                    }
                    stac.Push(res);
                    res = 0;
                }
            }
            if(flagNumber!=true)
            Result.Add(stac.Pop());
           
        }
        public double GetRes()
        {
            double res = Result[0];
            return res;
        }
        public List<double> GetResult()
        {
            return Result;
        }
    }
}
