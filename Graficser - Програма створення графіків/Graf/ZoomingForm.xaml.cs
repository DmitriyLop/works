﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Graf
{
    /// <summary>
    /// Логика взаимодействия для ZoomingForm.xaml
    /// </summary>
    public partial class ZoomingForm : Window
    {
        protected int MinX=1;
        protected int MaxX=1;
        protected int MinY=1;
        protected int MaxY=1;
        public int GetMinX(){
            return MinX;
        }
        public int GetMaxX()
        {
            return MaxX;
        }
        public int GetMinY() { 
            return MinY;
        }
        public int GetMaxY() { 
            return MaxY;
        }
        public ZoomingForm()
        {
            InitializeComponent();
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            switch (ComboBoxX.SelectedIndex)
            {
                case 1: MinX = -10; MaxX = 10; break;
                case 3: MinX = -20; MaxX = 20; break;
                case 5: MinX = -30; MaxX = 30; break;
                case 7: MinX = -50; MaxX = 50; break;
                case 9: MinX = -100; MaxX = 100; break;
                case 11: MinX = -200; MaxX = 200; break;
                case 13: MinX = -250; MaxX = 250; break;
            }
            switch (ComboBoxY.SelectedIndex)
            {
                case 1: MinY = -10; MaxY = 10; break;
                case 3: MinY = -20; MaxY = 20; break;
                case 5: MinY = -30; MaxY = 30; break;
                case 7: MinY = -50; MaxY = 50; break;
                case 9: MinY= -100; MaxY = 100; break;
                case 11: MinY = -200; MaxY = 200; break;
                case 13: MinY = -250; MaxY = 250; break;
            }
            Close();
        }
    }
}
