﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using ClassLibraryGraphics;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Drawing;
using System.IO;
using Microsoft.Win32;
using System.Threading;
using System.Diagnostics;


namespace Graf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Saver saver = new Saver();
        Graphic graf;
        protected bool flag = false;
        public MainWindow()
        {
            InitializeComponent();

            saver.Show();
            Thread.Sleep(3000);
            saver.Close();
            LabelEror.Visibility = Visibility.Hidden;
           
        }
        private void OpenFile()
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "Назва файлу (*.graf)|*.graf";
            if (openFile.ShowDialog() == true)
            {
                MyFile file = new MyFile(openFile.FileName);
                graf = file.OpenFile();
                if (graf == null) { MessageBox.Show("Файл пошкоджено!"); Canva.Children.Clear(); }
                else
                {
                    Canva.Children.Clear();
                    NewCordinates();
                }
            }
        }
        private void MunuItemOpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFile();
        }
        private void SaveFile()
        {
            if (graf != null)
            {
                SaveFileDialog saveFile = new SaveFileDialog();
                saveFile.Filter = "Назва файлу (*.graf)|*.graf";
                if (saveFile.ShowDialog() == true)
                {
                    MyFile file = new MyFile(saveFile.FileName);
                    file.SaveFile(graf);
                    MessageBox.Show("Графік збережено!");
                }
            }
            
        }
        private void MenuItemSaveFile_Click(object sender, RoutedEventArgs e)
        {
            SaveFile();
        }
       
        //вихід
        protected void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            MainForm.Close();
        }
        //збереження фото
        private void MenuItemSaveImage_Click(object sender, RoutedEventArgs e)
        {
            LabelEror.Visibility = Visibility.Hidden;
           //Rect bounds = VisualTreeHelper.GetDescendantBounds(Canva);
            //Преобразует объект Visual в растровое изображение.
            RenderTargetBitmap bitmap = new RenderTargetBitmap((int)Canva.ActualWidth, (Int32)Canva.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            // Отображает Visual объекта.
            bitmap.Render(Canva);
            RenderTargetBitmap bitmapX = new RenderTargetBitmap((int)CanvaX.ActualWidth, (Int32)CanvaX.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            bitmap.Render(CanvaX);
            RenderTargetBitmap bitmapY= new RenderTargetBitmap((int)CanvaY.ActualWidth, (Int32)CanvaY.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            bitmap.Render(CanvaY);
            //Определяет кодировщик, используемый для кодирования изображений в формате Формат PNG (Portable Network Graphics).
            PngBitmapEncoder image = new PngBitmapEncoder();
            image.Frames.Add(BitmapFrame.Create(bitmap));
            image.Frames.Add(BitmapFrame.Create(bitmapX));
            image.Frames.Add(BitmapFrame.Create(bitmapY));
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "Назва файлу (*.jpeg)|*.jpeg";
            if(saveFile.ShowDialog() == true)
                using (Stream fs = File.Create(saveFile.FileName ))
            {
            image.Save(fs);
            }
        }

        // події при нажиманні на створення на графіг(відкриється нове вікно для вводу формул)
        //маштабування графіка
        protected void MenuItemChangeZoom_Click(object sender, RoutedEventArgs e)
        {
            ZoomingForm zoomForm = new ZoomingForm();
            zoomForm.ShowDialog();
            if(graf!=null){
            // задаємо мін макс У
                if (zoomForm.GetMinY()!=1)
                {
                    graf.MinValueY = zoomForm.GetMinY();
                    graf.MaxValueY = zoomForm.GetMaxY();
                    // змінюємо значення х і проразовуємо нові точки
                    graf.NewPoints(zoomForm.GetMinX(), zoomForm.GetMaxX());
                }
           
            Canva.Children.Clear();
            NewCordinates();
            }
            
        }

       
        // редагування ширини ліній
        protected void MenuItemChangeWidth_Click(object sender, RoutedEventArgs e){
            WidthLinesWindow widthLines = new WidthLinesWindow();

            widthLines.ShowDialog();
            if (graf != null)
            {
                if (widthLines.GetWidthAxes() != 1)
                {
                    graf.WidthAxes = widthLines.GetWidthAxes();
                    graf.WidthGrafLines = widthLines.GetWidthGraf();
                }
                Canva.Children.Clear();
                NewCordinates();
            }
        }
        // редагування кольору
        protected void MenuItemChangeColor_Click(object sender, RoutedEventArgs e)
        {
            ChangeColorWindow colorWindow = new ChangeColorWindow();
            colorWindow.ShowDialog();
            if (graf != null)
            {
                graf.SetColorAxes(colorWindow.GetMycolorAxes().R, colorWindow.GetMycolorAxes().G, colorWindow.GetMycolorAxes().B);
                graf.SetColorGraf(colorWindow.GetMyColofGraf().R, colorWindow.GetMyColofGraf().G, colorWindow.GetMyColofGraf().B);
                Canva.Children.Clear();
                NewCordinates();
            }
           
        }
        protected void MenuItemNewGrafic_click(object sender, RoutedEventArgs e)
        {

            
            FormNewGrafic a = new FormNewGrafic();
            try
            {
                    a.ShowDialog();
                    graf = new Graphic(a.GetGraf());
                    flag = true;
                    Canva.Children.Clear();
                    NewCordinates();
            }
            catch {}
        }
       protected void NewCordinates(){
           LabelEror.Visibility = Visibility.Hidden;
           Line AxisX = new Line();
           Line AxisY = new Line();
           // промальовка підписів;
           DrawTextAxes();
           Canva.Children.Add(LabelXY);
           // встановлюємо колір
           List<byte> RGBaxes = new List<byte>(graf.GetColorAxes());
           List<byte> RGBgraf = new List<byte>(graf.GetColorGraf());
           SolidColorBrush myColorAxes = new SolidColorBrush();
           myColorAxes.Color = Color.FromRgb(RGBaxes[0],RGBaxes[1],RGBaxes[2]);
           SolidColorBrush myColofGraf  = new SolidColorBrush();
           myColofGraf.Color = Color.FromRgb(RGBgraf[0], RGBgraf[1], RGBgraf[2]);

           // колір осей
           AxisY.Stroke = AxisX.Stroke = myColorAxes;
           AxisY.StrokeThickness = AxisX.StrokeThickness = graf.WidthAxes;
           // точки Х,У
                List<double> X = new List<double>(graf.GetX());
                List<double> Y = new List<double>(graf.GetY());
               
                System.Windows.Point[] points = new Point[X.Count];
                int maxValueX = (int)graf.MaxValueX;
                int maxValueY = (int)graf.MinValueY;
                double DelenieX = (double)maxValueX/(double)(Canva.ActualWidth / 2);
                double DelenieY = (double)maxValueY/(double)(Canva.ActualHeight / 2);
                double tempX = 1/DelenieX;
                double tempY = 1/DelenieY;

                for (int i = 0; i < X.Count; i++)
                {
                    X[i] = (Canva.ActualWidth / 2) + (int)(X[i] * tempX);
                    Y[i] = (Canva.ActualHeight / 2) + (int)(Y[i] * tempY);
                }

                for (int i = 0; i < X.Count; i++)
                {
                    points[i] = new Point(X[i], Y[i]);
                }

            // зміна осі Х при розтягуванні вікна
            AxisX.X1 = (int)Canva.ActualWidth / 2;
            AxisX.Y1 = 0;
            AxisX.X2 = (int)Canva.ActualWidth / 2;
            AxisX.Y2 = (int)Canva.ActualHeight;

            // зміна осі Y при розтягуванні вікна
            AxisY.X1 = 0;
            AxisY.Y1 = (int)Canva.ActualHeight / 2;
            AxisY.X2 = (int)Canva.ActualWidth;
            AxisY.Y2 = (int)Canva.ActualHeight / 2;
            // додаємо новий елемент
           
                Canva.Children.Add(AxisX);
                Canva.Children.Add(AxisY);

              // промальовка графіка
                int[] tmp = new int[points.Length];
                int number = 0;        
                    for (int i = 0; i < points.Length - 1; i++)
                    {
                        //if (points[i].Y < 0 && points[i + 1].Y < 0) continue;
                        //else
                        //else if (points[i].Y > Canva.ActualHeight && points[i + 1].Y > Canva.ActualHeight) continue;
                        //else
                        if ( Math.Abs((points[i].Y) - (points[i + 1].Y)) > Canva.ActualHeight / 6)
                        {
                            tmp[i] = 1;
                            ++number;
                        }
                        else tmp[i] = 0;
                    }
                // якщо лінія графіка суцільна
                if (number < 2)
                {
                    Polyline myPoly = new Polyline();
                    myPoly.Stroke = myColofGraf;
                    myPoly.StrokeThickness = graf.WidthGrafLines;
                    if (!graf.GrafIsLinear) myPoly.StrokeDashArray = new DoubleCollection() {1,2};
                    myPoly.FillRule = FillRule.EvenOdd;
                    PointCollection myPointCollection1 = new PointCollection();
                    for (int i = 0; i < points.Length; i++)
                    {
                        //if (points[i].Y < -10000) {
                        //    points[i].Y = Canva.ActualHeight + 30;
                        //} 
                    
                         myPointCollection1.Add(points[i]);
                    }
                    myPoly.Points = myPointCollection1;
                    Canva.Children.Add(myPoly);
                }
                    // якщо періодична лінія.. .Наприклад tg(x)
                else
                {
                    Polyline[] myPolyline = new Polyline[number + 1];
                    PointCollection[] myPointCollection2 = new PointCollection[number+1];
                    for (int i = 0; i < number + 1; i++)
                    {
                        myPolyline[i] = new Polyline();
                        myPointCollection2[i] = new PointCollection();
                        myPolyline[i].Stroke = myColofGraf;
                        myPolyline[i].StrokeThickness = graf.WidthGrafLines;
                        if (!graf.GrafIsLinear) myPolyline[i].StrokeDashArray = new DoubleCollection() { 1, 2 };
                        myPolyline[i].FillRule = FillRule.EvenOdd;
                    }
                    int t = 0;
                    int flg = 0;
                    Point x = new Point(); 
                    for (int i = 0; i <= number; i++)
                    {
                        for (int j = t; j < points.Length; j++)
                        {
                            if (flg != 1) myPointCollection2[i].Add(points[j]);
                            flg = 0;              
                            if (tmp[j] == 1 && j<points.Length) {
                                //if (points[j].Y < Canva.ActualHeight / 2) { x.Y = 0; x.X = points[j].X; flg = 1; myPointCollection2[i].Add(x); }
                                //else { x.Y = Canva.ActualHeight-20; x.X = points[j].X; flg = 1; myPointCollection2[i].Add(x); }
                                //if (points[j].Y < Canva.ActualHeight / 2 && points[j + 1].Y < points[j].Y) { x.Y = 0; x.X = points[j].X; myPointCollection2[i].Add(x); }
                                //else
                                //    if (points[j].Y < Canva.ActualHeight / 2 && points[j + 1].Y > points[j].Y) { x.Y = Canva.ActualHeight; x.X = points[j].X; myPointCollection2[i].Add(x); }
                                    //else
                                    //if (i < number && points[j + 1].Y < points[j].Y) { x.Y = Canva.ActualHeight; x.X = points[j].X; myPointCollection2[i + 1].Add(x); }
                                t = j + 1;
                                break; 
                            }
                        }
                      //x= (Point)myPointCollection2[i];
                      myPolyline[i].Points = myPointCollection2[i];
                     
                        Canva.Children.Add(myPolyline[i]);
                    }
                    if (number > 6)
                    {
                        if (graf.MaxValueY >= 20)
                        {
                            //Canva.Children.Add(LabelEror);
                            LabelEror.Visibility = Visibility;
                        }
                    }
                    else
                        if (graf.MaxValueY >= 40)
                        {
                            //Canva.Children.Add(LabelEror);
                            LabelEror.Visibility = Visibility;
                        }
                    
                   
                }
                        //for (int i = 0; i < X.Count; i++)
                        //{
                        //    myPointCollection2.Add(points[i]);
                        //}
                        //myPolyline.Points = myPointCollection2;
                        //Canva.Children.Add(myPolyline);

                        //myPolyline.Points = myPointCollection1;
                        //Canva.Children.Add(myPolyline);
                    
                    ////графік точковий
                    //else
                    //{
                    //    Line[] lines = new Line[points.Length];

                    //    for (int i = 0; i < points.Length; i++)
                    //    {
                    //        if (points[i].Y <= 0 || points[i].Y >= Canva.ActualHeight || points[i].X >= Canva.ActualWidth || points[i].X <= 0) continue;
                    //        lines[i] = new Line();
                    //        lines[i].Stroke = myColofGraf;
                    //        lines[i].StrokeThickness = graf.WidthGrafLines;
                    //        lines[i].X1 = points[i].X;
                    //        lines[i].Y1 = points[i].Y;
                    //        lines[i].X2 = points[i].X;
                    //        lines[i].Y2 = points[i].Y - graf.WidthGrafLines;
                    //        Canva.Children.Add(lines[i]);
                    //    }

                    //}
                
          
            // малюємо сітку//////////////////////
            // кількість вертикальних ліній сітки 
            int numberStepX = 10;
            // кількість горизонтальних ліній сітки
            int numberStepY = 10;
            // відстань між вертикальними лініями сітки
            int stepLineX = (int)(Canva.ActualWidth / 2) / numberStepX;
            //відстань між горизонтальними лініями сітки
            int stepLineY = (int)(Canva.ActualHeight / 2) / numberStepY;
            //верхня горизонтальна сітка
            for (int i = ((int)Canva.ActualHeight / 2) - stepLineY; i >= 0; i -= stepLineY)
            {
                Line topGorizontalLines = new Line();
                topGorizontalLines.Stroke = System.Windows.Media.Brushes.Black;
                topGorizontalLines.StrokeThickness = 0.2;
                topGorizontalLines.X1 = 0;
                topGorizontalLines.Y1 = i;
                topGorizontalLines.X2 = Canva.ActualWidth;
                topGorizontalLines.Y2 = i;
                Canva.Children.Add(topGorizontalLines);
            }
            // нижня горизонтальна сітка
            for (int i = ((int)Canva.ActualHeight / 2) + stepLineY; i <= Canva.ActualHeight; i += stepLineY)
            {
                Line topGorizontalLines = new Line();
                topGorizontalLines.Stroke = System.Windows.Media.Brushes.Black;
                topGorizontalLines.StrokeThickness = 0.2;
                topGorizontalLines.X1 = 0;
                topGorizontalLines.Y1 = i;
                topGorizontalLines.X2 = Canva.ActualWidth;
                topGorizontalLines.Y2 = i;
                Canva.Children.Add(topGorizontalLines);
            }
            // права вертикальна сітка
            for (int i = 0; i < Canva.ActualWidth / 2; i += stepLineX)
            {
                if (i == 0) continue;
                Line leftVerticalLines = new Line();
                leftVerticalLines.Stroke = System.Windows.Media.Brushes.Black;
                leftVerticalLines.StrokeThickness = 0.2;
                leftVerticalLines.X1 = (Canva.ActualWidth / 2) + i;
                leftVerticalLines.Y1 = 0;
                leftVerticalLines.X2 = (Canva.ActualWidth / 2) + i;
                leftVerticalLines.Y2 = Canva.ActualHeight;
                Canva.Children.Add(leftVerticalLines);
            }
            //ліва вертикальна сітка
            for (int i = (int)(Canva.ActualWidth / 2) - stepLineX; i > 0; i -= stepLineX)
            {
                if (i == 0) continue;
                Line leftVerticalLines = new Line();
                leftVerticalLines.Stroke = System.Windows.Media.Brushes.Black;
                leftVerticalLines.StrokeThickness = 0.2;
                leftVerticalLines.X1 = i;
                leftVerticalLines.Y1 = 0;
                leftVerticalLines.X2 = i;
                leftVerticalLines.Y2 = Canva.ActualHeight;
                Canva.Children.Add(leftVerticalLines);

            }
            // задаємо назву вертикальної осі!
            TextBlock textY = new TextBlock();
            textY.FontSize = 14;
            textY.FontFamily = new FontFamily("Arial");
            textY.Text = graf.NameY;
            textY.Width = 100;
            textY.Height = 20;
            textY.HorizontalAlignment = HorizontalAlignment.Center;
            textY.VerticalAlignment = VerticalAlignment.Center;
            textY.Margin = new Thickness((Canva.ActualWidth/2)+20, 15, 0, 0);
            Canva.Children.Add(textY);
           // задаємо назву горизонтальній осі
            textY.Background = Brushes.Transparent;
            TextBlock textX = new TextBlock();
            textX.FontSize = 14;
            textX.FontFamily = new FontFamily("Arial");
            textX.Text = graf.NameX;
            textX.Width = 100;
            textX.Height = 20;
            textX.HorizontalAlignment = HorizontalAlignment.Center;
            textX.VerticalAlignment = VerticalAlignment.Center;
            textX.Margin = new Thickness((Canva.ActualWidth)-80,(Canva.ActualHeight/2)+20,0, 0);
            Canva.Children.Add(textX);
            textX.Background = Brushes.Transparent;
        }
        private void Canva_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Canva.Children.Clear();
            if (flag == true)
            { NewCordinates();}
        }

   
        private void ButtonQuikOpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFile();
        }

        private void ButtonQuikSaveFile_Click(object sender, RoutedEventArgs e)
        {
            SaveFile();
        }

        private void ButtonQuikZoomMinus_Click(object sender, RoutedEventArgs e)
        {
            if (graf != null)
            {
                switch ((int)graf.MaxValueX)
                {
                    case 10: graf.NewPoints(-20, 20); break;
                    case 20: graf.NewPoints(-30, 30); break;
                    case 30: graf.NewPoints(-50, 50); break;
                    case 50: graf.NewPoints(-100, 100); break;
                    case 100: graf.NewPoints(-200, 200); break;
                    case 200: graf.NewPoints(-250, 250); break;
                    case 250: break;
                }

                switch ((int)graf.MaxValueY)
                {
                    case 10: graf.MinValueY = -20; graf.MaxValueY = 20; break;
                    case 20: graf.MinValueY = -30; graf.MaxValueY = 30; break;
                    case 30: graf.MinValueY = -50; graf.MaxValueY = 50; break;
                    case 50: graf.MinValueY = -100; graf.MaxValueY = 100; break;
                    case 100: graf.MinValueY = -200; graf.MaxValueY = 200; break;
                    case 200: graf.MinValueY = -250; graf.MaxValueY = 250; break;
                    case 250: break;
                }
                Canva.Children.Clear();
                NewCordinates();
            }
            
        }

        private void ButtonQuikZoomPlus_Click(object sender, RoutedEventArgs e)
        {
            if (graf != null)
            {
                switch ((int)graf.MaxValueX)
                {
                    case 10: break;
                    case 20: graf.NewPoints(-10, 10); break;
                    case 30: graf.NewPoints(-20, 20); break;
                    case 50: graf.NewPoints(-30, 30); break;
                    case 100: graf.NewPoints(-50, 50); break;
                    case 200: graf.NewPoints(-100, 100); break;
                    case 250: graf.NewPoints(-200, 200); break;
                }

                switch ((int)graf.MaxValueY)
                {
                    case 10: break;
                    case 20: graf.MaxValueY = 10; graf.MinValueY = -10; break;
                    case 30: graf.MaxValueY = 20; graf.MinValueY = -20; break;
                    case 50: graf.MaxValueY = 30; graf.MinValueY = -30; break;
                    case 100: graf.MaxValueY = 50; graf.MinValueY = -50; break;
                    case 200: graf.MaxValueY = 100; graf.MinValueY = -100; break;
                    case 250: graf.MaxValueY = 200; graf.MinValueY = -200; break;
                }
                Canva.Children.Clear();
                NewCordinates();
            }
        }

        private void Canva_MouseMove(object sender, MouseEventArgs e)
        {
            LabelXY.Visibility = Visibility.Visible;
            // координати на канвасі....
            Point point = Mouse.GetPosition(Canva);

            if (point.X > Canva.ActualWidth - 50) {
                Canvas.SetLeft(LabelXY, point.X-50);
                Canvas.SetTop(LabelXY, point.Y +20);
            }
            else
                if (point.Y > Canva.ActualHeight - 60) {
                    Canvas.SetLeft(LabelXY, point.X);
                    Canvas.SetTop(LabelXY, point.Y -30);
                }
                else
            {
                Canvas.SetLeft(LabelXY, point.X);
                Canvas.SetTop(LabelXY, point.Y + 10);
            }
           

            if (graf != null)
            {
              // розрахунок координат в системі 
                if (point.X < Canva.ActualWidth/2)
                {
                    point.X = (((graf.MaxValueX * 2) * point.X) / Canva.ActualWidth) -graf.MaxValueX;
                }
                else point.X = (((graf.MaxValueX * 2) * point.X) / Canva.ActualWidth) - graf.MaxValueX;
               
                if (point.Y > Canva.ActualHeight/ 2)
                {
                    point.Y = -(((graf.MaxValueY * 2) * point.Y) / Canva.ActualHeight) +graf.MaxValueY;
                }
                else point.Y = Math.Abs((((graf.MaxValueY * 2) * point.Y) / Canva.ActualHeight) - graf.MaxValueY);
                
                LabelXY.Content = Math.Round(point.X,2) + " : " + Math.Round(point.Y,2); 
            }
        }

        private void Canva_MouseLeave(object sender, MouseEventArgs e)
        { //курсор виходить за канву
            LabelXY.Visibility = Visibility.Hidden;
        }
        private void MenuItemHelper_Click(object sender, RoutedEventArgs e)
        {
            Helper help = new Helper();
            help.Show();

        }
        //промальовка підписів 
        private void DrawTextAxes()
        {
         
            
            if (graf != null)
            {
                CanvaY.Children.Clear();
                CanvaX.Children.Clear();
                // малюємо підписи по Х
                ContentPresenter[] pointsXLeft = new ContentPresenter[10];
                ContentPresenter[] pointsRaight = new ContentPresenter[10];
                int stepContentX = (int)graf.MaxValueX / 10;
                int stepCordX = (int)((CanvaX.ActualWidth / 2) / 10);
                // права частина
                for (int i = 0; i < pointsRaight.Length; i++) 
                {   pointsRaight[i] = new ContentPresenter();
                    pointsRaight[i].Content = (stepContentX*i).ToString();
                    pointsRaight[i].Margin = new Thickness((CanvaX.ActualWidth/2)+(i*stepCordX)-3, 10, 0, 0);
                    CanvaX.Children.Add(pointsRaight[i]);
                }
               // ліва частина
                for (int i = 0; i < pointsXLeft.Length; i++)
                {
                    if (i == 0) continue;
                    pointsXLeft[i] = new ContentPresenter();
                    pointsXLeft[i].Content = (-1 * (stepContentX * i)).ToString();
                    pointsXLeft[i].Margin = new Thickness((CanvaX.ActualWidth / 2) - (i * stepCordX)-5, 10, 0, 0);
                    CanvaX.Children.Add(pointsXLeft[i]);
                }

                // малюємо підписи по Y
                ContentPresenter[] pointsYTop = new ContentPresenter[10];
                ContentPresenter[] pointsYBottom = new ContentPresenter[10];
                int stepContentY = (int)graf.MaxValueY / 10;
                int stepCordY = (int)((CanvaY.ActualHeight / 2) / 10);
                //промальовуємо верх У
                for (int i = 0; i < pointsYTop.Length; i++)
                {
                    pointsYTop[i] = new ContentPresenter();
                    pointsYTop[i].Content = (stepContentY * i).ToString();
                    pointsYTop[i].Margin = new Thickness(3,((CanvaY.ActualHeight/2)-stepCordY*i)-10, 0, 0);
                    CanvaY.Children.Add(pointsYTop[i]);
                }
                //промальовуємо низ У
                for (int i = 1; i < pointsYTop.Length; i++)
                {
                    pointsYBottom[i] = new ContentPresenter();
                    pointsYBottom[i].Content = "-"+(stepContentY * i).ToString();
                    pointsYBottom[i].Margin = new Thickness(3, ((CanvaY.ActualHeight / 2) + stepCordY * i) - 10, 0, 0);
                    CanvaY.Children.Add(pointsYBottom[i]);
                }
            }
           

        }
       

       
    }
}
