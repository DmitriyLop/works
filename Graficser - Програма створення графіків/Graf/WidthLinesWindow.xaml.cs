﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Graf
{
    /// <summary>
    /// Логика взаимодействия для WidthLinesWindow.xaml
    /// </summary>
    public partial class WidthLinesWindow : Window
    {
        protected int WidthAxes=1;
        protected int WidthGraf=1;
        public int GetWidthGraf()
        {
            return WidthGraf;
        }
        public int GetWidthAxes(){
            return WidthAxes;
        }
        public WidthLinesWindow()
        {
            InitializeComponent();
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            switch (ComboBoxWidthAxes.SelectedIndex)
            {
                case 1: WidthAxes = 2; break;
                case 3: WidthAxes = 3; break;
                case 5: WidthAxes = 5; break;
                case 7: WidthAxes = 10; break;
                case 9: WidthAxes = 15; break;
                case 11: WidthAxes = 20; break;
                case 13: WidthAxes = 25; break;
            }
            switch (ComboBoxWidthGraf.SelectedIndex)
            {
                case 1: WidthGraf = 2; break;
                case 3: WidthGraf = 3; break;
                case 5: WidthGraf = 5; break;
                case 7: WidthGraf= 10; break;
                case 9: WidthGraf= 15; break;
                case 11: WidthGraf = 20; break;
                case 13: WidthGraf = 25; break;
            }
            Close();
        }

    }
}
