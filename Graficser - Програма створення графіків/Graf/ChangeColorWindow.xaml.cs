﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Drawing;


namespace Graf
{
    /// <summary>
    /// Логика взаимодействия для ChangeColorWindow.xaml
    /// </summary>
    public partial class ChangeColorWindow : Window
    {
         protected Color MyColorAxes = new Color();
         protected Color MyColofGraf  = new Color();

        public ChangeColorWindow()
        {
            InitializeComponent();
        }
        public Color GetMyColofGraf()
        {
            return MyColofGraf;
        }
        public Color GetMycolorAxes()
        {
            return MyColorAxes;
        }
        private void ButtonApplyColors_Click(object sender, RoutedEventArgs e)
        {
            // задаємо колір осям...................
            switch (ComboBoxAxesColor.SelectedIndex)
            {
                case 0: MyColorAxes = Color.FromArgb(100,0,0,0); break;
                case 1: MyColorAxes = Color.FromArgb(100, 255, 0, 0); break;
                case 2: MyColorAxes  = Color.FromArgb(100, 255, 209, 0); break;
                case 3: MyColorAxes  = Color.FromArgb(100, 39, 222, 89); break;
                case 4: MyColorAxes = Color.FromArgb(100, 0, 255, 243); break;
                case 5: MyColorAxes = Color.FromArgb(100, 0, 58, 255); break;
                case 6: MyColorAxes = Color.FromArgb(100, 117, 0, 160); break;
                case 7: MyColorAxes = Color.FromArgb(100, 255, 0, 139); break;

            }
            // задаємо колір графіку
            switch (ComboBoxGrafColor.SelectedIndex)
            {
                case 0: MyColofGraf = Color.FromArgb(100, 0, 0, 0); break;
                case 1: MyColofGraf = Color.FromArgb(100, 255, 0, 0); break;
                case 2: MyColofGraf = Color.FromArgb(100, 255, 209, 0); break;
                case 3: MyColofGraf = Color.FromArgb(100, 39, 222, 89); break;
                case 4: MyColofGraf = Color.FromArgb(100, 0, 255, 243); break;
                case 5: MyColofGraf = Color.FromArgb(100, 0, 58, 255); break;
                case 6: MyColofGraf = Color.FromArgb(100, 117, 0, 160); break;
                case 7: MyColofGraf = Color.FromArgb(100,255,0,139); break;
            }
            Close();
        }
    }
}
