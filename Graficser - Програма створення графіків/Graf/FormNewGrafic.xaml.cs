﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClassLibraryGraphics;

namespace Graf
{
    /// <summary>
    /// Логика взаимодействия для FormNewGrafic.xaml
    /// </summary>
    public partial class FormNewGrafic : Window
    {
        protected Graphic Graf;
         public Graphic GetGraf(){
             return Graf;
         }
       //public PolishNotation formula= new Calculator("sd");
        public FormNewGrafic()
        {
            InitializeComponent();
        }
        // подія нажаття кнопки для промальовки графіка
        private void ButtonCreateGrafic_Click(object sender, RoutedEventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            // перевіряємо правильність введення формули
            string formula = textBoxFormula.Text;
            CheckingFormula check = new CheckingFormula(formula);
            formula = check.GetFormula();
            if (check.AllBrackets() == false) CheckLabel.Content = "Не однакова кількість дужок!";
            else if (check.ManySize()) CheckLabel.Content = "Не правильно введені бінарні операції!";
            else if (check.OtherSymbols()) CheckLabel.Content = "Недопустимі символи в формулі!";
            else if (check.LnIsNull()) CheckLabel.Content = "Значення натурального лагорифма недопустиме!";
            else if (check.CheckLog()) CheckLabel.Content = "Помилка в підрахунку логорифма!Не допускаються змінні,від'ємні числа!Лише операції(-,+,/,*)";
            else if (check.BracketsIsNotWithFunc()) CheckLabel.Content = "Операції cos,sin і тд. Повинні записуватися з дужками! Наприклад cos(3). ";
            else if (check.CheckFinish()) CheckLabel.Content = "Помилка при зчитування формули. Перевірте будь ласка формулу!";
            else
            {
                double minX = 0;
                double maxX = 0;
                double minY = 0;
                double maxY = 0;
                // межі Х,У
                switch (ComboBoxX.SelectedIndex)
                {
                    case 0: minX = -10; maxX = 10; break;
                    case 1: minX = -20; maxX = 20; break;
                    case 2: minX = -30; maxX = 30; break;
                    case 3: minX = -50; maxX = 50; break;
                    case 4: minX = -100; maxX =100; break;
                    case 5: minX = -200; maxX = 200; break;
                    case 6: minX = -250; maxX = 250; break;
                }
                switch (ComboBoxY.SelectedIndex)
                {
                    case 0: minY = -10; maxY = 10; break;
                    case 1: minY = -20; maxY = 20; break;
                    case 2: minY = -30; maxY = 30; break;
                    case 3: minY = -50; maxY = 50; break;
                    case 4: minY = -100; maxY = 100; break;
                    case 5: minY = -200; maxY = 200; break;
                    case 6: minY = -250; maxY = 250; break;
                }

                // графік лінія чи ні!
                bool GrafIsLeaner;
                // кількість точок
                int numberPoints = 100;
                // визначаємо який буде графік
                if (RadioButtonLeanerGraf.IsChecked == true)
                {
                    GrafIsLeaner = true;
                    //numberPoints = 500;
                }
                else
                {
                    GrafIsLeaner = false;
                    //if (maxX < 50) numberPoints = 200;
                    //else if (maxX <= 100) numberPoints = 300;
                    //else if (maxX <= 200) numberPoints = 600;
                    //else numberPoints = 600;
                }
                // график
                Graf = new Graphic(textBoxNameHorizontalAxes.Text,textBoxNameVerticalAxes.Text,minX, maxX, minY, maxY, numberPoints, formula, GrafIsLeaner);
                this.Close();
            }
        }


 

        

     

      

      
    }
}
