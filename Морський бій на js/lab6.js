createFormRequest();
var gameMatrix = [];
var maxWidth = 0;
var maxHeight = 0;
var mineCountRes = 0;
var flagCount = 0;
var timer = 0;
var timerEvent;

//таймер
function updateTime() {
    var timeBlock  = document.getElementById("timer");
    if(timeBlock != undefined){
        timer = timer + 1;
        timeBlock.innerHTML = "Час гри(в секундах): " + timer;
    }
}
//створюємо форму запиту про кількість клітинок
function  createFormRequest() {
    var requestForm = document.createElement("form");
    requestForm.id = "requestForm";
    requestForm.addEventListener("click", checkingValueRequest);

    var labelWidth = document.createElement("label");
    labelWidth.innerHTML = "Кількість клітинок по ширині (не > 25):"

    var inputWidth = document.createElement("input");
    inputWidth.setAttribute("type","number");
    inputWidth.setAttribute("id", "widthTable");

    var labelHeight = document.createElement("label");
    labelHeight.innerHTML = "Кількість клітинок по висоті (не > 100):"

    var inputHeight = document.createElement("input");
    inputHeight.setAttribute("type", "number");
    inputHeight.setAttribute("id", "heightTable");

    var labelMine = document.createElement("label");
    labelMine.innerHTML = "Кількість мін:"

    var inputCouuntMine = document.createElement("input");
    inputCouuntMine.setAttribute("type", "number");
    inputCouuntMine.setAttribute("id", "countMine");

    var buttonEl = document.createElement("button")
    buttonEl.innerHTML = "Розпочати гру"

    requestForm.appendChild(labelHeight);
    requestForm.appendChild(inputHeight);

    requestForm.appendChild(labelWidth);
    requestForm.appendChild(inputWidth);

    requestForm.appendChild(labelMine);
    requestForm.appendChild(inputCouuntMine);

    requestForm.appendChild(buttonEl);

    document.body.appendChild(requestForm);
}

// перевірка форми
function  checkingValueRequest(e) {
    e.preventDefault();
    if( e.target.tagName == "BUTTON" ){
        var widthTabl = document.getElementById("widthTable").value;
        var heightTabl = document.getElementById("heightTable").value;
        var mineCount =  document.getElementById("countMine").value;

        if (widthTabl > 0 && heightTabl > 0 && mineCount > 0 && mineCount < widthTabl * heightTabl && widthTabl<= 25 && heightTabl <=100){
            document.getElementById("requestForm").remove();
            startGame(widthTabl,heightTabl,mineCount);
        }
        else{
            addMessage(document.getElementById("requestForm"), "Помилка задання значень грі. Введіть інші значення.");
        }
    }
}

//функція для повідомлень
function addMessage(element, message) {
     var lastMessage = document.getElementById("message")

     if(lastMessage){
         lastMessage.remove();
     }

     var childrenElement = document.createElement("p");
     childrenElement.setAttribute("id", "message");
     childrenElement.innerHTML = message;
     element.appendChild(childrenElement);
}

//розпочинаємо гру
function  startGame(widthT, heightT, mineCount) {
    timer = 0;
    maxHeight = heightT;
    maxWidth = widthT;
    mineCountRes = mineCount;
    flagCount = mineCount;

    timerEvent  = setInterval(updateTime, 1000);

    var gameTable = createTable(widthT,heightT);
    gameTable.mine = mineCount;

    gameTable.addEventListener('click',openWindow);
    gameTable.addEventListener("contextmenu",operatingWithFlag);

    var mainBlock = document.createElement("div");
    mainBlock.setAttribute("id","mainBlock");

    var counterFlagP = document.createElement("p");
    counterFlagP.setAttribute("id","flagCounter");
    counterFlagP.innerHTML = mineCount;
    var pTime = document.createElement("p");
    pTime.innerHTML = "Час гри(в секундах): 0";
    pTime.setAttribute("id","timer");
    
    var buttonRestart = document.createElement("button");
    buttonRestart.setAttribute("id","restart");
    buttonRestart.innerHTML = "Перезапустити";
    buttonRestart.addEventListener("click",restartGame);
    
    var buttonNewGame = document.createElement("button");
    buttonNewGame.setAttribute("id","newGame");
    buttonNewGame.innerHTML = "Нова гра";
    buttonNewGame.addEventListener("click",newGame);

    mainBlock.appendChild(counterFlagP);
    mainBlock.appendChild(gameTable);
    mainBlock.appendChild(pTime);
    mainBlock.appendChild(buttonRestart);
    mainBlock.appendChild(buttonNewGame);

    document.body.appendChild(mainBlock);
}
//переграти гру
function restartGame() {
    var mainBlock = document.getElementById("mainBlock");
    mainBlock.remove();
    clearInterval(timerEvent);
    startGame(maxWidth, maxHeight, mineCountRes);


}
//нова гра
function newGame() {
    location.reload();
}
//додає флажки
function operatingWithFlag(e) {
        if(e.target.classList.contains("flag")){
                e.target.remove();
                flagCount = flagCount + 1;
                e.target.classList.remove("flag");
            }
        else{
            if(flagCount != 0 && !e.target.classList.contains("open") && e.target.tagName != "SPAN"){
                flagCount = flagCount - 1;
                var flagImg = document.createElement("img");
                flagImg.setAttribute("src","flag.png");
                flagImg.classList.add("flag");
                e.target.appendChild(flagImg);
            }
            else if(flagCount == 0){
                checkOpening();
            }
        }
    var counter = document.getElementById("flagCounter");
    counter.innerHTML = flagCount;
}
//створюємо поле гри
function createTable(widthTable, heightTable) {
    var gameTable = document.createElement("table");
    gameTable.setAttribute("id", "gameTable");

    var tR = 0;
    var tD = 0;

    for( let i = 0; i < heightTable; i++){
        tR = document.createElement("tr");

        for(let j = 0; j < widthTable; j++){
            tD = document.createElement("td");
            tD.firstIndex = i;
            tD.secondIndex = j;
            tR.appendChild(tD);
        }

        gameTable.appendChild(tR);
    }
    return gameTable;
}

// відкриває клітинки з подальшим обробленням іншими функціями
function openWindow (e) {
    // якщо перше відкриття - генеруємо міни
    if( firstOpening() ){
        generateMatrix(addingOpenClass(e.target));
    }
    else{

        if(!e.target.classList.contains("open")){
         var indexes = addingOpenClass(e.target);
         updateGameTable(indexes);
        }
    }
}

// додає класи до клітинок, які відкриті і повертає їхні індекси
function  addingOpenClass(element) {
    var rows = document.getElementsByTagName("tr");
    var columns = 0;
    // знаходимо елемент на який натиснули
    for( var i = 0; i < rows.length; i++){
        columns = rows[i].getElementsByTagName("td");

        for( var j = 0; j < columns.length; j++ ){

            if( element == columns[j]){
                element.classList.add("open");
                var res = [i,j];
                i = rows.length;
                break
            }
        }
    }
    return res;
}
// перевіряє чи є хоч она відкрити клітинка
function firstOpening(){
    var gameWindow = document.getElementsByClassName("open");

    if(gameWindow.length <= 0){
        return true;
    }
    else{
        return false;
    }
}

//відкриття всіх клітинок
function allOpening() {


    var tDs = document.getElementsByTagName("td");
    for( let i = 0; i < tDs.length; i++){
        if(!tDs[i].classList.contains("open")){
            var row = tDs[i].firstIndex;
            var column = tDs[i].secondIndex;
            tDs[i].classList.add("open");
            if(gameMatrix[row][column] == "mine"){
                var mineImg = document.createElement("img");
                mineImg.setAttribute("src","bomba.jpg");
                tDs[i].appendChild(mineImg);
            }
            else if(gameMatrix[row][column] > 0){
                var spanBlock = document.createElement("span");
                spanBlock.innerHTML = gameMatrix[row][column];
                tDs[i].appendChild(spanBlock);
            }
        }
    }
}

// генеруємо початкову матрицю
function  generateMatrix(indexes) {
    for(let i = 0; i < maxHeight; i++ ){
        gameMatrix[i] = [];
        for( let j = 0; j < maxWidth; j++){
            gameMatrix[i][j] = 0;
        }
    }

    var x = 0;
    var y = 0;
    for(let i = 0; i < mineCountRes; i++ ){
        x = randValue(0, maxWidth - 1);
        y = randValue(0, maxHeight - 1);
        if(x == indexes[1] && y == indexes[0]){
            i = i - 1;
            continue;
        }
        for( let j = y; j < maxHeight; j++){
            for(let k = x ; k < maxWidth; k++){
                if(gameMatrix[j][k] == 0 && x == k && y == j){
                    gameMatrix[j][k] = "mine";
                }
                else if (x == k && y == j && gameMatrix[j][k]!= 0){
                    i = i - 1;
                    break;
                }
            }
        }
    }
  //console.log(gameMatrix);

    calculateDistance();
    updateGameTable(indexes);
}

//обчислення відстаней до бомби
function  calculateDistance() {
    var matrix = gameMatrix.slice();
    var indexes = [
        [1,0],
        [1,-1],
        [1,1],
        [0,-1],
        [0,1],
        [-1,0],
        [-1,1],
        [-1,-1]
    ]

    for( let i = 0; i < maxHeight; i++ ){
        for( let j = 0; j < maxWidth; j++){

            if(matrix[i][j] == "mine"){

            for( let l = 0; l < indexes.length; l++){
                if(i + indexes[l][0] >= 0 && i + indexes[l][0] < maxHeight && j + indexes[l][1] >= 0 && j + indexes[l][1] < maxWidth){

                    if( matrix[i + indexes[l][0]][j + indexes[l][1]] != "mine"){

                        matrix[i + indexes[l][0]][j + indexes[l][1]] += 1;
                    }

                }
            }

            }
        }
    }
    gameMatrix = matrix;
    //console.log(gameMatrix);
}

//оновлення ігрового поля
function updateGameTable(indexes) {
    var tDs = document.getElementsByTagName("td");
    var gameTd = 0;

    for( let i = 0; i < tDs.length; i++){
        if( tDs[i].firstIndex == indexes[0] && tDs[i].secondIndex == indexes[1] ){
            gameTd = tDs[i];
            break;
        }
    }

    if(gameMatrix[indexes[0]][indexes[1]] == 0 ){
        bigOpening(tDs,gameTd,indexes);
    }
    else if(gameMatrix[gameTd.firstIndex][gameTd.secondIndex] == "mine"){
        var imgBomb = document.createElement("img");
        imgBomb.setAttribute("src", "bomba.jpg");
        gameTd.appendChild(imgBomb);
        gameOver();
        return;
    }
    else {
        var text = document.createElement("span");
        text.innerHTML = gameMatrix[indexes[0]][indexes[1]];
        gameTd.appendChild(text);
    }
    checkOpening();
}

//перевірка чи відкриті всі поля
function checkOpening() {
    var tDs = document.getElementsByClassName("open");
    if(tDs.length == maxWidth * maxHeight - mineCountRes){
        gameWin();
    }
}

//перемога в грі
function gameWin() {
    stopedGame();
    allOpening();
    var mainBlock = document.getElementById("flagCounter");
    mainBlock.innerHTML = "ПЕРЕМОГА! ГРА ЗАВЕРШЕНА!";
    deleteFlags();
}

function stopedGame() {
    var gameTable = document.getElementById("gameTable");
    gameTable.removeEventListener('click',openWindow);
    gameTable.removeEventListener("contextmenu",operatingWithFlag);

    clearInterval(timerEvent);
}
// розширене відкриття полів
function bigOpening(tDs,gameTd,indexes) {
    var neigborIndex = [
        [1, 0],
        [0, -1],
        [0, 1],
        [-1, 0]
    ]

    var tdArray = [];
    var count = 0;
    for( let a = 0; a < maxHeight; a++){
        tdArray[a] = [];
        for(let z = 0; z < maxWidth; z++){
            tdArray[a][z] = tDs[count];
            count = count + 1;
        }
    }

    var queue  = [];
    var  row = 0;
    var column = 0;
    queue.push(indexes);

    while (indexes != undefined){
        indexes = queue.shift();

        for( let i = 0; i < neigborIndex.length; i++){
            if(indexes != undefined){
                row = indexes[0] + neigborIndex[i][0];
                column = indexes[1] + neigborIndex[i][1];
            }

            if(row >= 0 && row < maxHeight && column >=0 && column < maxWidth){

                if(gameMatrix[row][column] != "mine" && !tdArray[row][column].classList.contains("open")){

                    var flag = tdArray[row][column].getElementsByClassName("flag");

                    if( flag[0]!= undefined){
                        continue;
                    }

                    tdArray[row][column].classList.add("open");

                    if(gameMatrix[row][column] > 0){
                        var span = document.createElement("span");
                        span.innerHTML = gameMatrix[row][column];
                        tdArray[row][column].appendChild(span);
                    }
                    else {
                        queue.push([row, column]);
                    }
                }


            }

        }
    }


}


//видалення флагів
function deleteFlags() {
    var flags = document.getElementsByTagName("img");
    //alert("кількість флагів: " + flags.length);
    for(let j = 0; j < flags.length; j++){

        if(flags[j].classList.contains("flag")){
            flags[j].style.display = "none";
        }

    }
}
// функція програшу
function gameOver() {
    stopedGame();
    allOpening();
    var mainBlock = document.getElementById("flagCounter");
    mainBlock.innerHTML = "ВИ ПРОГРАЛИ!";
    gameTable.removeEventListener('click',openWindow);
    gameTable.removeEventListener("contextmenu",operatingWithFlag);
    deleteFlags();
}
// функція для псевдовипадкового числа
function  randValue(min,max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}