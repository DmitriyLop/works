﻿using Assets.Script;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlappyMonster : Monster, IMoving {
    new private Rigidbody2D Rigidbody;
    [SerializeField]
    protected float Speed = 0.01f;
    private Vector3 vector=new Vector3(-0.03f, 0, 0);
    public void Moving()
    {
        transform.position = Vector3.MoveTowards(transform.position,transform.position+vector, Speed + Time.deltaTime);
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position + transform.right * vector.x * 35f, 0.4f);
        if (colliders.Length > 0 && !ThisIsHero(colliders))
        {
            Destroy(gameObject);
        }
    }
    private bool ThisIsHero(Collider2D[] collider)
    {
        for (int i = 0; i < collider.Length; i++)
        {
            if (collider[i].GetComponent<Hero>() is Hero) return true;
        }
        return false;
    }
    void Start () {
        Rigidbody = GetComponent<Rigidbody2D>();
    }
    void FixedUpdate () {
        Moving();
	}
}
