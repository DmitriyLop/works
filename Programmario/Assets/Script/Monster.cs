﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Monster : LivingObject {

    public override void Killing(LivingObject hero)
    {
        Vector3 direction;
        hero = (Hero)hero;
        if(transform.position.x<hero.transform.position.x)
        direction = new Vector3(hero.transform.position.x + 1.5f, hero.transform.position.y + 1.2f, 0);
        else
        direction = new Vector3(hero.transform.position.x - 1.5f, hero.transform.position.y + 1.2f, 0);
        hero.transform.position = direction;
        if (hero.GetLive() > 1) {
            hero.MinusLive();
           
        }
        else hero.Demage();
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            LivingObject obj = collision.gameObject.GetComponent<LivingObject>();
            if (gameObject.tag == "Monster")
            {
                if (Mathf.Abs(collision.gameObject.transform.position.x - transform.position.x) < 0.7f &&
                    collision.gameObject.transform.position.y > transform.position.y) {
                    obj = (Hero)obj;
                    obj.Killing(gameObject.GetComponent<LivingObject>());
                    Demage();
                }
                else
                {
                    Killing(obj);
                }  
            }
            else
            if(gameObject.tag=="SpikyMonster")
            {
                Killing(obj);
            }
        }
    }
    public override void Demage()
    {
        Destroy(gameObject);
    }
}
