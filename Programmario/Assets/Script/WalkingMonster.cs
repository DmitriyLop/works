﻿using Assets.Script;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingMonster : Monster, IMoving {
    [SerializeField]
    protected float Speed = 0.25f;
    private float Direction = 0.03f;
    private Vector3 vector = new Vector3(0.03f, 0, 0);
    protected SpriteRenderer sprite;
    private int Counter = 1;
    protected void Awake()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
    }
    public void Moving()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position +transform.up*0.5f+ transform.right*vector.x*20f, 0.1f);
        if (colliders.Length > 0 && !ThisIsHero(colliders))
        {
            vector.x *= -1f;
        }
        transform.position = Vector3.MoveTowards(transform.position, transform.position + vector, Speed * Time.deltaTime);
    }
     private bool ThisIsHero(Collider2D[] collider)
    {
        for(int i = 0; i < collider.Length; i++)
        {
            if (collider[i].GetComponent<Hero>() is Hero) return true;
        }
        return false;
    }
	void FixedUpdate () {
        
        Moving();
	}
}
