﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class BlockStorage : MonoBehaviour {
    [SerializeField]
    protected int BlockCounter;
    [SerializeField]
    protected GameObject Block;
    public void AddBlock(GameObject block)
    {
        if (block != null)
        {
            Destroy(block);
            ++BlockCounter;
        }  
    }
    public void AddBlock()
    {
        ++BlockCounter;
    }
    public void BildBlock(GameObject hero)
    {
        if (BlockCounter != 0)
        {
            --BlockCounter;
            Block.transform.position = hero.transform.position;
           GameObject.Instantiate(Block);
            
        }
    }
    public int GetCounter()
    {
        return BlockCounter;
    }
}
