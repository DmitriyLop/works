﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LivingObject : MonoBehaviour {

    [SerializeField]
    protected int Lives = 5;
    public void PlusLive()
    {
        Lives++;
    }
    public void MinusLive()
    {
        --Lives;
    }
    public int GetLive()
    {
        return Lives;
    }
    public void SetLive(int lives)
    {
        Lives = lives;
    }
    public abstract void Killing( LivingObject obj);
    public abstract void Demage();
}
