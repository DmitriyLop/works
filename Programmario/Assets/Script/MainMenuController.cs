﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {
    [SerializeField]
    protected string Name;
    [SerializeField]
    protected AudioClip Din;
    private AudioSource Audio;
    public void Start()
    {
        Audio = GetComponent<AudioSource>();
    }
    public void NextLevel()
    {
    
        SceneManager.LoadScene(Name);
       
    }
    public void Exit()
    {
        Application.Quit();
    }
    void OnMouseDown()
    {
        if (Audio != null)
            Audio.PlayOneShot(Din);
        DontDestroyOnLoad(Audio);
        if (Name != "Exit") NextLevel();
        else Exit();
    }
}
