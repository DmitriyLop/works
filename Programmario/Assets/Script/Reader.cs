﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection.Emit;
using UnityEngine;
using UnityEngine.UI;

public class Reader : MonoBehaviour {

    [SerializeField]
    protected string Tag;
    private Hero Hero;
    private Text Txt;
    private Coin Coin;
    private void Awake()
    {
      Txt = gameObject.GetComponent<Text>();
      GameObject player = GameObject.FindGameObjectWithTag("Player");
        Hero = player.GetComponent<Hero>();
        Coin = GetComponent<Coin>();
    }
    void Update () {
        if (System.DateTime.Today.Second%2==0)
        UpdateInfornation();
	}
    private void UpdateInfornation()
    {
        if (Txt != null && Hero!=null) 
        if (Tag=="Lives")
        {
           Txt.text= Hero.GetLive().ToString();
        }
        else if (Tag == "Coins")
        {
                if (Coin != null)
           Txt.text= Coin.GetCounter().ToString();
        }
        if (Tag == "Blocks")
        {   if(Hero!=null)
            Txt.text= Hero.GetComponent<BlockStorage>().GetCounter().ToString();
        }
        
    }
}
