﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Saver : MonoBehaviour
{
    [SerializeField]
    protected int CountLevels;
    //[SerializeField]
    protected string NameFile = "coin.mario";
    [SerializeField]
    protected int LevelId;
    protected static string[] Txt;
    protected Text Text;
    private int Key = 5;
   
    void Start()
    {
        if (CountLevels > 0 && Txt == null)
        {
            Txt = new string[CountLevels];
            GetInformation();
        }
        Text = gameObject.GetComponent<Text>();
        if (Text != null )
        {
            if(Txt[LevelId - 1] != "0")
            Text.text = "Score:" + Txt[LevelId - 1];
        else
            Text.text = "-----";
        }
           

    }
    public void GetInformation()
    {
         FileInfo fileinfo = new FileInfo(NameFile);
        if (fileinfo.Length == 0)
        {
            for (int i = 0; i < Txt.Length; i++)
                Txt[i] = "0";
        }
        else
        {
            using (BinaryReader reader = new BinaryReader(File.Open(NameFile, FileMode.Open)))
            {
                for (int i = 0; i < Txt.Length; i++)
                { 
                        Txt[i] = reader.ReadString();
                }
                reader.Close();
            }
            Uncoding();
        }
      
    }
    public void SetInformation()
    {

        using (BinaryWriter writer = new BinaryWriter(File.Open(NameFile, FileMode.OpenOrCreate)))
        {
            for (int i = 0; i < Txt.Length; i++)
            {
                writer.Write(Coding(Txt[i]));
            }
            writer.Close();
        }
    }
    public void UpdateInformation(int coinCounter)
    {
        if (Txt != null)
        {
            if (int.Parse(Txt[LevelId - 1]) < coinCounter)
                Txt[LevelId - 1] = coinCounter.ToString();
            SetInformation();
        }
    }
    void Uncoding()
    {
        for (int i = 0; i < Txt.Length; i++)
            Txt[i] = (int.Parse(Txt[i]) >> Key).ToString();
    }
    public string Coding(string words)
    {
           return (int.Parse(words) << Key).ToString();
    }
}
