﻿using Assets.Script;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Hero : LivingObject, IMoving
{
    //[SerializeField]
    private float Speed = 6;
    [SerializeField]
    private double PowerJump = 10;
    new private Rigidbody2D Rigidbody;
    private Animator Animator;
    private SpriteRenderer Sprite;
    private bool OnGround = false;
    private Weapon Weapon;
    private BlockStorage Storage;
    [SerializeField]
    protected AudioClip Din;
    [SerializeField]
    protected AudioClip SoundKilling;
    [SerializeField]
    protected AudioClip SoundJump;
    private AudioSource Audio;
    private CharState State
    {
        get { return (CharState)Animator.GetInteger("State"); }
        set { Animator.SetInteger("State", (int)value); }
    }
    private void Awake()
    {
        Application.targetFrameRate = 100;
        Rigidbody = GetComponent<Rigidbody2D>();
        Animator = GetComponent<Animator>();
        Sprite = GetComponentInChildren<SpriteRenderer>();
        Weapon = Resources.Load<Weapon>("Weapon");
        Storage = GetComponent<BlockStorage>();
        Audio = GetComponent<AudioSource>();
        Coin coin = new Coin();
        coin.StaticPosition();
        coin.ClearCounter();
    }
    private void Update()
    {
        if(State !=CharState.Dead)
        {
            if (Input.GetButtonDown("Fire1")) Fire();
            if (OnGround) State = CharState.Idle;
            if (Input.GetButton("Horizontal")) Run();
            if (OnGround && Input.GetButtonDown("Jump")) Jump();
            if (Input.GetKeyDown(KeyCode.E)) AddBlock();
            if (Input.GetKeyDown(KeyCode.Q)) BildBlock();
            CheckGround();
        }
    }
    private void BildBlock()
    {
        Storage.BildBlock(gameObject);
    }
    private void AddBlock()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 1f);
        if(colliders.Length>0)
        {
            Storage.AddBlock(BickBlock(colliders));
        }
    }
    private GameObject BickBlock(Collider2D[] collider)
    {
        for(int i = 0; i < collider.Length; i++)
        {
            if (collider[i].tag == "BlickBlock") return collider[i].gameObject;
        }
        return null;
    }
    private void Fire()
    {
        Vector3 posit = transform.position; posit.y += 0.3f; posit.x+=(Sprite.flipX ? (-1f) : (1f));
        Weapon newWepon = Instantiate(Weapon, posit, Weapon.transform.rotation);
        newWepon.Direction = newWepon.transform.right * (Sprite.flipX ? (-1f) : (1f));
    }
    private void Run()
    {
        Moving();
    }
    private void Jump()
    {
        if (Audio != null)
            Audio.PlayOneShot(SoundJump);
        State = CharState.Jump;
        Rigidbody.AddForce(transform.up * (float)PowerJump, ForceMode2D.Impulse);
    }
    private void  CheckGround()
    {  
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.25f);
        OnGround = colliders.Length > 1;
        if (!OnGround) State = CharState.Jump;
    }
    public void Moving()
    {
        if (OnGround) State = CharState.Run;
        Vector3 vector = transform.right * Input.GetAxis("Horizontal");
        transform.position = Vector3.MoveTowards(transform.position, transform.position + vector, Speed * Time.deltaTime);
        Sprite.flipX = vector.x < 0;
    }
    public override void Killing(LivingObject gameObject)
    {
        if (Audio != null)
            Audio.PlayOneShot(SoundKilling);
    }
    public override void Demage()
    {
        if (Audio != null)
           Audio.PlayOneShot(Din);
        State = CharState.Dead;
        Destroy(gameObject, 0.7f);
        Exitor exitor = new Exitor();
        exitor.GameOver();
    }
    public enum CharState
    {
        Idle,
        Run,
        Jump,
        Dead
    }
}


