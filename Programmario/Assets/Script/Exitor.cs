﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Exitor : MonoBehaviour {
    [SerializeField]
    protected string SceneName;
	void Update () {
        if (SceneName != null && Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(SceneName);
        }
    }
    public void GameOver()
    {
        GameObject menu = GameObject.FindWithTag("BackMenu");
        if (menu != null)
        {
            GameObject camera = GameObject.FindWithTag("MainCamera");
            if (camera != null)
            {
                Vector3 vect = new Vector3(camera.transform.position.x, 3.1f, 4.8f);
                menu.transform.position = vect;
                Destroy(GameObject.FindWithTag("Ground"));
                Coin coin = new Coin();
                coin.ChangeBodyType();
            }
            GameObject[] buttons = GameObject.FindGameObjectsWithTag("Button");
            if (buttons != null)
                for (int i = 0; i < buttons.Length; i++)
                {
                    buttons[i].GetComponent<Enabling>().UnEnabling();
                }
            GameObject mainSound = GameObject.FindGameObjectWithTag("Sound");
            if (mainSound != null) mainSound.GetComponent<AudioSource>().Stop();
            GameObject[] blocks = GameObject.FindGameObjectsWithTag("BlickBlock");
            if (blocks != null)
                for (int i = 0; i < blocks.Length; i++)
                    Destroy(blocks[i]);
        }
    }
}
