﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {
    [SerializeField]
    protected AudioClip Din;
    private AudioSource Audio; 
    private static int CoinCounter = 0;
    private static bool IsStaticPosition = true;
    private static bool IsDead = false;
    private Rigidbody2D Rigidbody;
    void Start()
    {
        Rigidbody = gameObject.GetComponent<Rigidbody2D>();
         Audio = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (!IsStaticPosition && Rigidbody!=null)
        {
            Rigidbody.bodyType = RigidbodyType2D.Dynamic;
        }
        if (IsDead) Destroy(gameObject);
    }
    public int GetCounter() {
        return CoinCounter;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll == true && coll.tag=="Player")
        {
            gameObject.GetComponent<Collider2D>().isTrigger = true;
            AddCoin();
            if (Audio != null)
                Audio.PlayOneShot(Din);
            Destroy(gameObject.GetComponent<Collider2D>());
            Destroy(gameObject.GetComponentInChildren<SpriteRenderer>());
            Destroy(gameObject, 0.7f);
        }
    }
    public void  AddCoin()
    {
        ++CoinCounter;
    }
    public void ChangeBodyType()
    {
        IsStaticPosition = false;
    }
    public void StaticPosition()
    {
        CoinCounter = 0;
        IsStaticPosition = true;
        if(Rigidbody!=null)
        Rigidbody.bodyType = RigidbodyType2D.Static;
    }
    public void AllDead()
    {
        IsDead = true;
    }
    public void ClearCounter()
    {
        CoinCounter = 0;
    }
}
