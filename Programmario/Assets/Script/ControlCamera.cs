﻿using Assets.Script;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ControlCamera : MonoBehaviour, IMoving {

    [SerializeField]
    protected float Speed = 10f;
    [SerializeField]
    protected Transform Target;
    private float CoordZ = -10f;
    private float CoordY = 3.1f;
    void Start () {
        if (!Target) Target = FindObjectOfType<Hero>().transform;
    }
	void Update () {
        Moving();
	}

    public void Moving()
    {
        if (Target != null)
        {
            Vector3 vect = Target.position; vect.z = CoordZ; vect.y = CoordY;
            transform.position = Vector3.Lerp(transform.position, vect, Speed * Time.deltaTime);
        }
    }
}
