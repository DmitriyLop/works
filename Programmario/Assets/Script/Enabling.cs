﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enabling : MonoBehaviour {
    [SerializeField]
    protected bool Enable;
    Collider2D Coll;
    void Start () {

        Coll = gameObject.GetComponent<Collider2D>();
        Coll.enabled = Enable;
    }
    public void UnEnabling()
    {
         
        if (Coll != null) Coll.enabled = true;
    }
    public void Enablin()
    {
        if (Coll != null) Coll.enabled = false;
    }
}
