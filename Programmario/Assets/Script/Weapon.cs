﻿using Assets.Script;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Weapon : MonoBehaviour, IMoving {

    protected float Speed = 10f;
    protected Vector3 direction;
    public Vector3 Direction
    {
        set
        {
            direction = value;
        }
    }
    protected SpriteRenderer sprite;
    [SerializeField]
    protected AudioClip Din;
    private AudioSource Audio;
    private void Start()
    {
        Destroy(gameObject, 2f);
    }
    private void Awake()
    {
        Audio = GetComponent<AudioSource>();
        if (Audio != null)
            Audio.PlayOneShot(Din);
        sprite = GetComponentInChildren<SpriteRenderer>();
        switch (ColorRandom())
        {
            case (int)Palette.Black:
                sprite.color = Color.black;
                break;
            case (int)Palette.Red:
                sprite.color = Color.red;
                break;
            case (int)Palette.Green:
                sprite.color = Color.green;
                break;
            case (int)Palette.Blue:
                sprite.color = Color.blue;
                break;
            case (int)Palette.Yellow:
                sprite.color = Color.yellow;
                break;
        }
    }
    private int ColorRandom()
    {
        System.Random rand = new System.Random();
        return rand.Next(0,5);
    }
	void Update () {
        Moving();
	}
    public void Moving()
    {
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, Time.deltaTime * Speed);
    }
    public void Kill(LivingObject obj)
    {
        Destroy(obj);
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        // Если вы хотите проверять, с кем вы столкнулись,
        // то, как правило, должны использовать теги, а не имена
        if (collision.gameObject.GetComponent<Monster>())
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
       
    }
    enum Palette
    {
        Black,
        Red,
        Green,
        Blue,
        Yellow
    }
}
