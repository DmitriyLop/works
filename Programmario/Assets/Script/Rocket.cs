﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour {
    [SerializeField]
    protected AudioClip Din;
    private AudioSource Audio;
    [SerializeField]
    protected string NameScence;
    private bool Flag = false;
    private float transf;
	// Use this for initialization
	void Start () {
        Audio = GetComponent<AudioSource>();
        gameObject.GetComponent<Animator>().speed = 0;
        transf =  gameObject.transform.position.y;
    }
	
	// Update is called once per frame
	void Update () {
        if (Flag == true)
        {
            Flight();
            if (transform.position.y - transf > 0.85f)
            {
                SceneManager.LoadScene(NameScence);
            }
          
        }
    }
    void Flight()
    {
        Vector3 vector = new Vector3(0, 0.006f, 0);
        transform.position = transform.position + vector;
    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.collider == true && coll.collider.tag == "Player")
        {
            GameObject sound = GameObject.FindGameObjectWithTag("Sound");
            if (sound != null) sound.GetComponent<AudioSource>().Stop();
            if (Audio != null)
                Audio.PlayOneShot(Din);
            Flag = true;
            gameObject.GetComponent<Animator>().speed = 3;
            coll.collider.gameObject.GetComponent<Hero>().enabled = false;
            Destroy(coll.collider.gameObject.GetComponentInChildren<SpriteRenderer>());
            Saver saver = gameObject.GetComponent<Saver>();
            Coin coin = new Coin();
            saver.UpdateInformation(coin.GetCounter());
            coin.ClearCounter();
        }
    }
 
}
