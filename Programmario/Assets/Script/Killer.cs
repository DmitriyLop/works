﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killer : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.collider == true && coll.collider.tag == "Player")
        {
            Hero hero = coll.collider.gameObject.GetComponent<Hero>();
            hero.Demage();
        }
        else
        {
            if (coll.collider == true && coll.collider.GetComponent<Coin>() != null)
                coll.collider.GetComponent<Coin>().StaticPosition();
            Destroy(coll.collider.gameObject);
        }
            
    }
}
