﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionBlock : MonoBehaviour {
    [SerializeField]
    protected int IndexPrize;
    [SerializeField]
    protected AudioClip Din;
    private AudioSource Audio = new AudioSource();
    void Start()
    {
        Audio = GetComponent<AudioSource>();
    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.collider == true && coll.collider.tag == "Player" &&
           Math.Abs(gameObject.transform.position.y) - Math.Abs(coll.collider.gameObject.transform.position.y)>1.25f)
        {
            AddPrize(coll.gameObject);
            if (Audio != null)
                Audio.PlayOneShot(Din);
            Destroy(gameObject.GetComponent<Collider2D>());
            Destroy(gameObject.GetComponentInChildren<SpriteRenderer>());
            Destroy(gameObject, 0.7f);
        }
    }
    private void AddPrize(GameObject gameObject )
    {
        Hero hero = gameObject.GetComponent<Hero>();
        if(hero!=null)
            if (IndexPrize == (int)Prize.Live)
            {
                hero.PlusLive();
            }
            else
                if (IndexPrize == (int)Prize.Block)
            {
                hero.GetComponent<BlockStorage>().AddBlock();
            }
            else
                if (IndexPrize == (int)Prize.Coin)
            {
                Coin coin = new Coin();
                coin.AddCoin();
            }
    }
    public enum Prize
    {
        Live,
        Block,
        Coin
    }
}

