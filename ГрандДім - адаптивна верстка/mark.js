var mark = document.querySelector(".mark");

mark.addEventListener("click", changeStar);
mark.addEventListener("mouseover", mouseOverStar)
mark.addEventListener("mouseout",mouseEnterStar);

function changeStar (event) {
    var elem = event.target;
    if ((elem.classList.contains("check") || elem.classList.contains("non-check") || elem.classList.contains("last-check"))) {

        var divClass = "check";

        for (let i = 0; i < mark.children.length; i++) {

            mark.children[i].setAttribute("class", divClass);

            if (mark.children[i] == elem) {
                divClass = "non-check"
            }
        }

    }
}

function  mouseOverStar(event) {
    var elem = event.target;

    if(elem.parentNode == mark){
        for( let i = 0; i < mark.children.length; i++){

            mark.children[i].classList.add("last-check");

            if(mark.children[i] == elem){
                break;
            }

        }
    }

}

function mouseEnterStar(event) {

    var elem = event.target;

    if(elem.parentNode == mark){

        for(let i = 0; i < mark.children.length; i++) {
            mark.children[i].classList.remove("last-check");
        }

    }

}

