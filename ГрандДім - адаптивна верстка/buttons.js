var button = document.getElementById("mainButton");
var form = document.getElementById("butForm");
var inputs = form.getElementsByTagName("input");
form.addEventListener("click",changeButton);

function changeButton(event) {
    if( event.target.tagName == "BUTTON"){
        event.preventDefault();

        var name = inputs[0].value;
        var minLen = inputs[1].getAttribute("min");

        var maxLen = inputs[1].value;
        maxLen = maxLen - 5;

            var checkButton = document.createElement("button");
            checkButton.style.padding = button.style.padding;
            checkButton.innerHTML = name;
            checkButton.style.visibility = "hidden";
            checkButton.style.fontSize = "14px";

        var addingDot = false;

        if(maxLen >= minLen){
            for(;;) {
                document.body.appendChild(checkButton);

                if(checkButton.offsetWidth > maxLen){
                    name = name.slice(0, name.length-1);
                    checkButton.innerHTML = name;
                    if(!addingDot){
                        addingDot = true;
                        maxLen = maxLen - 5;
                    }
                }
                else{
                    if(addingDot){
                        name += "..";
                        maxLen += 5;
                    }

                    maxLen = maxLen + 5;
                    button.style.width = maxLen + "px";
                    button.innerHTML = name;
                    break;
                }

                document.body.removeChild(checkButton);
            }
        }
    }
}




