﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class CurrencyConverter : MonoBehaviour {
    protected decimal Grivna;
    public CurrencyConverter(decimal value, string name)
    {   if (value <= 0) Grivna = 0;
         else
            if (name =="UAH") Grivna = value;
                else
                ChangeCurrency(value,name);
    }
    protected void ChangeCurrency(decimal value, string name)
    {
        Grivna = value * ParseCoeficient(name);
    }
    protected decimal ParseCoeficient(string name)
    {
        Informator informator = new Informator();
        Regex rgx = new Regex("rate>(.+)<.+\n.+cc>("+name+")<");
        if (informator.GetInformation() != null)
        {
            Match match = rgx.Match(informator.GetInformation());
            decimal Coeficient;
            if (decimal.TryParse(match.Groups[1].Value, out Coeficient) && match.Groups[1].Value.Length > 0)
                return Coeficient;
        }
        return 1;
    }
    public decimal GetGrivna()
    {
        return Grivna;
    }
    public decimal GetDolar()
    {
        return Grivna / ParseCoeficient("USD");
    }
    public decimal GetEvro()
    {
        return Grivna / ParseCoeficient("EUR");
    }
    public decimal GetZlotiy()
    {
        return Grivna / ParseCoeficient("PLN");
    }
    public decimal GetRubl()
    {
        return Grivna / ParseCoeficient("RUB");
    }
    public decimal GetBilRubl()
    {
        return Grivna / ParseCoeficient("BYN");
    }
    public decimal GetFuntSterling()
    {
        return Grivna / ParseCoeficient("GBP");
    }
}
