﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class NumberConverter : MonoBehaviour {
    protected int DecimalNumber;
    public NumberConverter(string value, string name)
    {
        if (CheckCorrectness(value, name))
        {
            ChangeSystem(value, name);
        }
        else DecimalNumber = 0;

    }
    protected void ChangeSystem(string value, string name)
    {
        char[] number = value.ToCharArray();
        for (int i = 0; i < value.Length; i++)
        {
            DecimalNumber += ReturnValue(number[value.Length-i-1]) * (int)Math.Pow(int.Parse(name),i); 
        }
    }
    private int ReturnValue(char number)
    {
        int num;
        if (int.TryParse(number.ToString(), out num)) return num;
        else
        {
            if (number == 'A' || number == 'a') return 10;
            if (number == 'B' || number == 'b') return 11;
            if (number == 'C' || number == 'c') return 12;
            if (number == 'D' || number == 'd') return 13;
            if (number == 'E' || number == 'e') return 14;
            if (number == 'F' || number == 'f') return 15;
        }

        return 0;
    }
    private bool CheckCorrectness(string value, string name)
    {
        try
        {
            char[] number = value.ToCharArray();
            if (name == "2" || name == "8" || name == "10")
            {
                int system = Convert.ToInt32(name);
                for (int i = 0; i < value.Length; i++)
                    if (Convert.ToInt32(number[i].ToString()) > system)
                        return false;
            }
            else
            {
                for (int j = 0; j < value.Length; j++)
                {
                    if (ReturnValue(number[j]) == 0) return false;
                }

            }
        }
        catch
        {
            return false;
        }
        return true;
    }
    public string GetDecimalNumber()
    {
        return DecimalNumber.ToString();
    }
    public string GetBinaryNumber()
    {
        return Convert.ToString(DecimalNumber, 2);
    }
    public string GetEightNumber()
    {
        return Convert.ToString(DecimalNumber,8);
    }
    public string GetSixteenNumber()
    {
        return Convert.ToString(DecimalNumber,16);
    }
}
