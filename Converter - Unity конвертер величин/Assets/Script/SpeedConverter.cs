﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedConverter : MonoBehaviour {
    protected decimal KmH;
    public SpeedConverter(decimal value, string name)
    {
        ChangeSpeed(value,name);
    }
    private void ChangeSpeed(decimal value, string name)
    {
        decimal maxValue = 999999999999999999999999m;
        switch (name)
        {
            case "kmh":
                KmH = (value > maxValue) ? 0 : (value);
                break;
            case "mph":
                KmH = (value > maxValue) ? 0 : (value / 0.62137119223733m);
                break;
            case "ms":
                KmH = (value > maxValue) ? 0 : (value / 0.27777777777778m);
                break;
            case "fps":
                KmH = (value > maxValue) ? 0 : (value / 0.91134444444444m);
                break;
            case "mmin":
                KmH = (value > maxValue) ? 0 : (value / 16.666666666667m);
                break;
            case "ftmin":
                KmH = (value > maxValue) ? 0 : (value / 54.680666666667m);
                break;
            case "ma":
                KmH = (value > maxValue) ? 0 : (value / 0.00081699346405229m);
                break;
        }
    }
    public decimal GetKilometrPerHour()
    {
        return KmH;
    }
    public decimal GetMilePerHour()
    {
        return KmH * 0.62137119223733m;
    }
    public decimal GetMeterPerSecond()
    {
        return KmH * 0.27777777777778m;
    }
    public decimal GetFootPerSecond()
    {
        return KmH * 0.91134444444444m;
    }
    public decimal GetMeterPerMinute()
    {
        return KmH * 16.666666666667m;
    }
    public decimal GetFootPerMinute()
    {
        return KmH * 54.680666666667m;
    }
    public decimal GetMah()
    {
        return KmH * 0.00081699346405229m;
    }
}
