﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {
    [SerializeField]
    protected string BackScene;
    [SerializeField]
    protected string NextScene;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
            if ((Input.GetKeyDown(KeyCode.Escape)) && (BackScene!=null && BackScene.Length>1))
                SceneManager.LoadScene(BackScene);
    }
    public void Next()
    {
        if (NextScene != null)
            SceneManager.LoadScene(NextScene);
    }
    public void Exit()
    {
        Application.Quit();
    }
    public void ClearInput()
    {
        InputFieldController controller = new InputFieldController();
        controller.AllClear();
    }
    public void Conversion()
    {
        InputFieldController controller = new InputFieldController();
        controller.Convert();
    }
}
