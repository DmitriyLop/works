﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemperatureConverter : MonoBehaviour {
    protected decimal Celsiy;
    public TemperatureConverter(decimal value, string name)
    {   if (Math.Abs(value) < (decimal.MaxValue / 2m))
            ChangeTemperature(value, name);
        else
            Celsiy = 0;
    }
    private void ChangeTemperature(decimal value, string name)
    {
        switch (name)
        {
            case "celsiy":
                Celsiy = value;
                break;
            case "farangeit":
                Celsiy = (value / 1.8m) - 32m;
                break;
            case "kelvin":
                Celsiy =  value - 273.15m;
                break;
            case "rankin":
                Celsiy = (value/1.8m)- 491.67m;
                break;
            case "delil":
                Celsiy = (value/1.5m)+100m;
                break;
            case "reomyur":
                Celsiy = (value/0.8m);
                break;
            case "remer":
                Celsiy = (value/ 0.525m)-7.5m;
                break;
        }

    }
    public decimal GetCelsiy()
    {
        return Celsiy;
    }
    public decimal GetFarengeit()
    {
        return (Celsiy*1.8m)+32m;
    }
    public decimal GetKelvin()
    {
        return Celsiy+273.15m;
    }
    public decimal GetRankin()
    {
        return (Celsiy*1.8m)+491.67m;
    }
    public decimal GetDelil()
    {
        return (Celsiy*1.5m)-100m;
    }
    public decimal GetReomyur()
    {
        return Celsiy*0.8m;
    }
    public decimal GetRemer()
    {
        return (Celsiy*0.525m)+7.5m;
    }
}
