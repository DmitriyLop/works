﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldController : MonoBehaviour {
    [SerializeField]
    protected string ValueName;
    [SerializeField]
    protected int ConvertorId;
    protected InputField InputF;
    protected static decimal Value;
    protected static string StringValue;
    protected static string Name;
    protected static string StringName;
    public string GetStingValue()
    {
        return StringValue;
    }
    public string GetValueName()
    {
        return ValueName;
    }
    public int GetId()
    {
        return ConvertorId;
    }
    void Start () {
        InputF = GetComponent<InputField>();
	}
	public void Write () {
        if (InputF.text.Length > 0)
            ClearInput();
    }
    void ClearInput()
    {
        InputField[] inputs = GameObject.FindObjectsOfType<InputField>();
        for (int i = 0; i < inputs.Length; i++)
            if (inputs[i].GetComponent<InputField>() != InputF)
                inputs[i].text ="";
    }
    public  void AllClear()
    {
        InputField[] inputs = GameObject.FindObjectsOfType<InputField>();
        for (int i = 0; i < inputs.Length; i++) 
                inputs[i].text = "";
    }
    public void  Convert()
    {
        string result;
        InputField[] inputs = GameObject.FindObjectsOfType<InputField>();
        if(CheckValue(inputs))
        for (int i = 0; i < inputs.Length; i++)
        {   
            InputFieldController controller = inputs[i].GetComponent<InputFieldController>();
               result = ChangeConvertor(controller.GetId(),controller.GetValueName());
                if (result == "0") 
                    inputs[i].text = "Значення не є допустимим для підрахунку!";
                else
                inputs[i].text = result;
        }     
    }
    private bool CheckValue(InputField[] inputs)
    {
       
        for (int i = 0; i < inputs.Length; i++)
            if (inputs[i].text.Length > 0)
            {
                if (inputs[i].GetComponent<InputFieldController>().GetId() == (int)Convertor.Number)
                {
                    StringValue = inputs[i].text;
                    StringName = inputs[i].GetComponent<InputFieldController>().GetValueName();
                    return true;
                }
                if (decimal.TryParse(inputs[i].text, out Value))
                {
                    Name=inputs[i].GetComponent<InputFieldController>().GetValueName();
                    return true;
                }  
                inputs[i].text = "помилка введення";
            }
        return false;
    }
    protected string ChangeConvertor(int id,string valueName)
    {
        switch (id)
        {
            case (int)Convertor.Time: {

                    return TimeConvert(valueName);
                }
            case (int)Convertor.Lenght:
                {
                    return LenghtConvert(valueName);
                }
            case (int)Convertor.Speed:
                {
                    return SpeedConvert(valueName);
                }
            case (int)Convertor.Number:
                {
                    return NumberConvert(valueName);
                }
            case (int)Convertor.Temperature:
                {
                    return TemperatureConvert(valueName);
                }
            case (int)Convertor.Weight:
                {
                    return WeightConvert(valueName);
                }
            case (int)Convertor.Currency:
                {
                    return CurrencyConvert(valueName);
                }
        }
        return "";
    }
    protected string TimeConvert(string convertName)
    {
        TimeConvertor timeConvertor = new TimeConvertor(Value, Name);
        switch (convertName)
        {
            case "nanosecond":
                return timeConvertor.GetNanosecond().ToString();
            case "microsecond":
                return timeConvertor.GetMicrosecond().ToString();
            case "milisecond":
                return timeConvertor.GetMilisecond().ToString();
            case "second":
                return timeConvertor.GetSecond().ToString();
            case "minute":
                return timeConvertor.GetMinutes().ToString();
            case "hour":
                return timeConvertor.GetHour().ToString();
            case "day":
                return timeConvertor.GetDay().ToString();
        }
        return "";
    }
    protected string LenghtConvert(string convertName)
    {
        LenghtConverter lenghtConverter = new LenghtConverter(Value, Name);
        switch (convertName)
        {
            case "santimeter":
                return lenghtConverter.GetSantimeter().ToString();
            case "inch":
                return lenghtConverter.GetInch().ToString();
            case "foot":
                return lenghtConverter.GetFoot().ToString();
            case "yard":
                return lenghtConverter.GetYard().ToString();
            case "kilometer":
                return lenghtConverter.GetKilometer().ToString();
            case "mile":
                return lenghtConverter.GetMile().ToString();
            case "liga":
                return lenghtConverter.GetLiga().ToString();
        }
        return "";
    }
    protected string SpeedConvert(string convertName)
    {
        SpeedConverter speedConverter = new SpeedConverter(Value, Name);
        switch (convertName)
        {
            case "kmh":
                return speedConverter.GetKilometrPerHour().ToString();
            case "mph":
                return speedConverter.GetMilePerHour().ToString();
            case "ms":
                return speedConverter.GetMeterPerSecond().ToString();
            case "fps":
                return speedConverter.GetFootPerSecond().ToString();
            case "mmin":
                return speedConverter.GetMeterPerMinute().ToString();
            case "ftmin":
                return speedConverter.GetFootPerMinute().ToString();
            case "ma":
                return speedConverter.GetMah().ToString();
        }
        return "";
    }
    protected string NumberConvert(string convertName)
    {
        NumberConverter numberConverter = new NumberConverter(StringValue,StringName);
        switch (convertName)
        {
            case "2":
                return numberConverter.GetBinaryNumber();
            case "8":
                return numberConverter.GetEightNumber();
            case "10":
                return numberConverter.GetDecimalNumber();
            case "16":
                return numberConverter.GetSixteenNumber();
        }
        return "";
    }
    protected string TemperatureConvert(string convertName)
    {
        TemperatureConverter temperature = new TemperatureConverter(Value, Name);
        switch (convertName)
        {
            case "celsiy":
                return temperature.GetCelsiy().ToString();
            case "farangeit":
                return temperature.GetFarengeit().ToString();
            case "kelvin":
                return temperature.GetKelvin().ToString();
            case "rankin":
                return temperature.GetRankin().ToString();
            case "delil":
                return temperature.GetDelil().ToString();
            case "reomyur":
                return temperature.GetReomyur().ToString();
            case "remer":
                return temperature.GetRemer().ToString();
        }
        return "";
    }
    protected string WeightConvert(string convertName)
    {
        WeightConverter weight = new WeightConverter(Value, Name);
        switch (convertName)
        {
            case "kilogram":
                return weight.GetKilogram().ToString();
            case "uncia":
                return weight.GetUncia().ToString();
            case "funt":
                return weight.GetFunt().ToString();
            case "stoun":
                return weight.GetStoun().ToString();
            case "gram":
                return weight.GetGram().ToString();
            case "tona":
                return weight.GetTona().ToString();
            case "karat":
                return weight.GetKarat().ToString();
        }
        return "";
    }
    protected string CurrencyConvert(string convertName)
    {
        CurrencyConverter currency = new CurrencyConverter(Value, Name);
        switch (convertName)
        {
            case "UAH":
                return currency.GetGrivna().ToString();
            case "USD":
                return currency.GetDolar().ToString();
            case "EUR":
                return currency.GetEvro().ToString();
            case "PLN":
                return currency.GetZlotiy().ToString();
            case "RUB":
                return currency.GetRubl().ToString();
            case "BYN":
                return currency.GetBilRubl().ToString();
            case "GBP":
                return currency.GetFuntSterling().ToString();
        }
        return "";
    }
    public enum Convertor
    {
        Time,
        Lenght,
        Speed,
        Number,
        Temperature,
        Weight,
        Currency
    }
}
