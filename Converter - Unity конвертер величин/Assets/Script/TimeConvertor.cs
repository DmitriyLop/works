﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeConvertor : MonoBehaviour
{
    protected decimal Minutes;
    public TimeConvertor(decimal value, string nameValue)
    {
        if (value >= 0)
            Minutes =(decimal)ChangeInMinutes(value, nameValue);
    }
    private decimal ChangeInMinutes(decimal value, string nameValue)
    {
        switch (nameValue)
        {
            case "nanosecond":
                {
                    if (value > 99999999999999999999999999m)
                        return 0;
                    return (value / 60000000000.00000m);
                }
            case "microsecond":
                {
                    if (value > 9999999999999999999999999m)
                        return 0;
                    return (value / 60000000.000000m);
                }
            case "milisecond":
                {
                    if (value > 9999999999999999999999m)
                        return 0;
                    return value / 60000.000000000m;
                }
               
            case "second":
                {
                    if (value > 9999999999999999999)
                        return 0;
                    return value / 60.00000000000m;
                }
                
            case "minute":
                {
                    if (value > 999999999999999999) return 0;
                    return value;
                }
            case "hour":
                {
                    if (value > 9999999999999999) return 0;
                    return value / 0.0166666666666666m;
                }
                
            case "day":
                {
                    if (value > 99999999999999) return 0;
                    return value / 0.0006944444444444m;
                }
                
        }
        return 0;
    }
    public decimal GetNanosecond()
    {
       Minutes=Minutes * 60000000000.000000000m;
        return Minutes;
    }
    public decimal GetMicrosecond()
    {
        return (Minutes * 60000000.000000000m);
    }
    public decimal GetMilisecond()
    {
        return (Minutes * 60000.000000000m);
    }
    public decimal GetSecond()
    {
        return (Minutes * 60.00000000000m);
    }
    public decimal GetMinutes()
    {
        return Minutes;
    }
    public decimal GetHour()
    {
        return Minutes * 0.0166666666666666m;
    }
    public decimal GetDay()
    {
        return Minutes * 0.0006944444444444m;
    }
}
