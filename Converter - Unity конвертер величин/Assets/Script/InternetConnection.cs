﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InternetConnection : MonoBehaviour
{
    private float RepeatTime = 2f; /* Время в секундах */
    private float CurrentTime;
    protected Canvas Canva;
    void Start()
    {
        Canva = GetComponent<Canvas>();
        CurrentTime = RepeatTime;
        if (Canva != null)
        Canva.enabled = false;
        Application.targetFrameRate = 60;
    }
    void Update()
    {
        CurrentTime -= Time.deltaTime;
        if (CurrentTime <= 0)
        {
            Informator informator = new Informator();
            if (Canva != null)
            {
                if (informator.CheckConnection())
                {
                    Canva.enabled = false;
                    informator.UpdateInformation();
                }
                else
                {
                    Canva.enabled = true;
                }
            }
            CurrentTime = RepeatTime;
        }
    }
}
