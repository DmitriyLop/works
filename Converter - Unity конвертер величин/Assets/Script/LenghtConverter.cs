﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LenghtConverter : MonoBehaviour {

    protected decimal Meters;
    public LenghtConverter(decimal value,string name)
    {
        if (value <= 0)
            Meters = 0;
        else
         ChangeLenght(value,name);
    }
    protected void ChangeLenght(decimal value, string name)
    {
        switch (name)
        {
            case "santimeter":
                {
                    Meters=(value > 9999999999999999999999999999m ) ? 0 : (value / 100.000000m);
                    break;
                }
            case "inch":
                {
                    Meters = (value > 9999999999999999999999999999m) ? 0 : (value / 39.370078740157m);
                    break;
                }
            case "foot":
                {
                    Meters =(value > 999999999999999999999999999m) ? 0: (value / 3.2808398950131m);
                    break;
                }
            case "yard":
                {
                    Meters = (value > 99999999999999999999999999m) ? 0 : (value / 1.0936132983377m);
                    break;
                }
            case "kilometer":
                {
                    Meters = (value > 99999999999999999999999m) ? 0 : (value / 0.001m);
                    break;
                }
            case "mile":
                {
                    Meters = (value > 99999999999999999999999m) ? 0 : (value / 0.00062136994937697m);
                    break;
                }
            case "liga":
                {
                    Meters =(value > 99999999999999999999999m) ? 0 : (value / 0.00020712330174427m);
                    break;
                }

        }
    }
    public decimal GetSantimeter()
    {
        return Meters * 100.000000m;
    }
    public decimal GetInch()
    {
        return Meters * 39.370078740157m;
    }
    public decimal GetFoot()
    {
        return Meters * 3.2808398950131m;
    }
    public decimal GetYard()
    {
        return Meters * 1.0936132983377m;
    }
    public decimal GetKilometer()
    {
        return Meters * 0.001m;
    }
    public decimal GetMile()
    {
        return Meters * 0.00062136994937697m;
    }
    public decimal GetLiga()
    {
        return Meters * 0.00020712330174427m;
    }
}
