﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeightConverter : MonoBehaviour {

    protected decimal Kilogram;
    public WeightConverter(decimal value, string name)
    {
        if (value > 0)
            ChangeMass(value, name);
        else Kilogram = 0;
    }
    private void ChangeMass(decimal value, string name)
    {
        switch (name)
        {
            case "kilogram":
                Kilogram = value;
                break;
            case "uncia":
                Kilogram = value/35.274m;
                break;
            case "funt":
                Kilogram = value/ 2.2046m;
                break;
            case "stoun":
                Kilogram = value/ 0.15747m;
                break;
            case "gram":
                Kilogram = value/1000m;
                break;
            case "tona":
                Kilogram = value * 1000;
                break;
            case "karat":
                Kilogram = value/ 5000m;
                break;
        }
    }
    public decimal GetKilogram()
    {
        return Kilogram;
    }
    public decimal GetUncia()
    {
        return Kilogram* 35.274m;
    }
    public decimal GetFunt()
    {
        return Kilogram * 2.2046m;
    }
    public decimal GetStoun()
    {
        return Kilogram * 0.15747m;
    }
    public decimal GetGram()
    {
        return Kilogram * 1000m;
    }
    public decimal GetTona()
    {
        return Kilogram/1000m;
    }
    public decimal GetKarat()
    {
        return Kilogram * 5000m;
    }
}
