
function Tetris (canva, element) {
    var _canva = canva;
    var _width = canva.getAttribute("width");
    var _height = canva.getAttribute("height");
    var _gameMatrix = createMatrix(_gameMatrix);
    var _canva = canva;
    var _context  = _canva.getContext("2d");
    var _size = _width / 10;
    var _shape = ShapeRandom();
    var _rotator = 0;
    var _stoper = false;
    var _element = element;
    var _points = 0;
    var _minHight = 5;

    _shape.createShape(_width / 2,60, _width / 10);

    var _interval = setInterval(gameProcess,1000);

    function gameProcess() {
        if(!_stoper){
            var matrix  = _gameMatrix;
            var deleting = true;
            if(checkMatrix(_shape, matrix, true)){

                _shape.clearBlocks(_context);
                updateGameMatrix(_shape, _gameMatrix, deleting);
                _shape.moveBottom();
                matrix = updateGameMatrix(_shape, _gameMatrix);
                _shape.drawBlocks(_context);
                _gameMatrix = matrix;
            }
            else{

                _shape.drawBlocks(_context);

                _shape = ShapeRandom();
                _shape.createShape(_width / 2, 60, _width / 10);
                clearInterval(_interval);
                _rotator = 0;
                grabMatrix();
                addPoints();

                if(checkGameOver()){
                    clearInterval(_interval);
                }
                else{
                    _interval = setInterval(gameProcess, 1000);
                }

            }

        }
    }

    function checkMatrix(shape, matrix, bottom, left, right, rotate) {
        var blocks = shape._blocks;
        var size = _width / 10;
        var verticalLen = 0;
        var horizontalLen = 0;

        for (let i = 0; i < blocks.length; i++) {
            verticalLen = blocks[i]._Y / size;
            horizontalLen = (blocks[i]._X / size) - 1;

            if (verticalLen > matrix.length - 2 || horizontalLen > matrix[i].length - 2) {
                return false;
            }
            else if (bottom && matrix[verticalLen + 1][horizontalLen + 1] == 1) {
                return false;
            }
            else if (left && horizontalLen >= 0) {
                if (matrix[verticalLen][horizontalLen] == 1) {
                    return false;
                }
            }
                else if (right && matrix[verticalLen][horizontalLen + 2] == 1 && horizontalLen < 10) {
                    return false;
                }
                else if(rotate){
                if(matrix[verticalLen][horizontalLen - 1] == 1 || matrix[verticalLen][horizontalLen + 3] == 1 || matrix[verticalLen + 1][horizontalLen + 1] == 1){
                    return false;
                }
            }
        }
            return true;
    }
    function checkGameOver() {
        for(let i = 0; i < _gameMatrix[0].length; i++){
           if(_gameMatrix[_minHight][i] == 1){
               clearInterval(_interval);
               _element.innerHTML = "GAMEOVER";
               _element.setAttribute("id","gameover");
               window.removeEventListener("keydown", keyController);
               return true;
           }
        }
        return false;
    }
    function addPoints() {
       var points = checkPoints();
       _points = _points + points;
       if(points){
           updateCanvas();
           element.innerHTML = _points * 10;
       }
    }
    function checkPoints() {
        var tmp = 0;
        var flag = 0;
        for(let i = 0; i < _gameMatrix.length; i++){
            for(let j = 0; j < _gameMatrix[i].length; j++){
                if( _gameMatrix[i][j] == 1){
                    ++tmp;
                    }
            }
            if(tmp == _gameMatrix[i].length){
                ++flag;
                for( let t = i; t >= 0; t--){
                    for(let z = 0; z < _gameMatrix[i].length; z++){
                        if(t > 0){
                            _gameMatrix[t][z] = _gameMatrix[t - 1][z];
                        }
                        else{
                            _gameMatrix[t][z] = 0;
                        }
                    }
                }
            }

            tmp = 0;
        }

        return flag;
    }
    function updateCanvas() {
        _context.clearRect(0, 0, _canva.width, _canva.height);
        for(let i = 0; i < _gameMatrix.length; i++){
            for( let j = 0; j < _gameMatrix[i].length; j++){
                if( _gameMatrix[i][j] == 1){

                    var block = new Square(_size * j, _size * i, _size);
                    block.drawSquare(_context);
                }
            }
        }
    }
    function updateGameMatrix(shape, matrix , deleteShape) {
        var blocks = shape._blocks;
        var size = _width / 10;

        for( let i = 0; i < blocks.length; i++){
            var vertical = blocks[i]._Y / size;
            var horizontal = (blocks[i]._X / size);
            if(deleteShape && vertical >= 0 && horizontal >= 0){

                matrix[vertical][horizontal] = 0;
            }
            else if(!deleteShape && vertical >=0 && horizontal >=0){

                matrix[vertical][horizontal] = - 1;
            }

        }
        return matrix;
    }
    function grabMatrix() {
        for(let i = 0; i < _gameMatrix.length; i++){
            for( let j = 0; j < _gameMatrix[0].length; j++){
                if(_gameMatrix[i][j] == -1){
                    _gameMatrix[i][j] = 1;
                }
            }
        }
    }


    function ShapeRandom() {
        var x = getRandom(1,7);
        var shape;

        switch (x) {
            case 1: shape = new Shape_T(); break;
            case 2: shape = new Shape_Z(); break;
            case 3: shape = new Shape_S(); break;
            case 4: shape = new Shape_J(); break;
            case 5: shape = new Shape_L(); break;
            case 6: shape = new Shape_I(); break;
            case 7: shape = new Shape_O(); break;
        }
        return shape;
    }
    function getRandom(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function createMatrix(matrix) {
        matrix = [];
        for( let i = 0; i < 20; i++){
            matrix[i] = [];

            for(let j = 0; j < 10; j++ ){
                matrix[i][j] = 0;
            }
        }
       return matrix;
    }

    window.addEventListener("keydown", keyController);

    function keyController(e) {
        var block = _shape._blocks;
        var codeKey = {UP: 40, LEFT: 37, RIGHT: 39, ROTATE: 38}
        //кнопка вниз
        if(e.keyCode == codeKey.UP && block[0]._Y < _height - _size && block[1]._Y < _height - _size && block[2]._Y < _height - _size && block[3]._Y < _height - _size){
            _shape.clearBlocks(_context);

            if(checkMatrix(_shape, _gameMatrix, true, false, false, false)){
                updateGameMatrix(_shape,_gameMatrix, true);
                _shape.moveBottom();
            }
            _shape.drawBlocks(_context);
        }

        // кнопка вліво
        if(e.keyCode == codeKey.LEFT && block[0]._X > 0 && block[1]._X > 0 && block[2]._X > 0 && block[3]._X > 0 ){
            _shape.clearBlocks(_context);

            if(checkMatrix(_shape, _gameMatrix, false, true, false, false)){
                updateGameMatrix(_shape,_gameMatrix, true);
                _shape.moveLeft();
            }
            _shape.drawBlocks(_context);
        }

        // кнопка вправо
        if(e.keyCode == codeKey.RIGHT && block[0]._X < _width - _size && block[1]._X < _width - _size && block[2]._X < _width - _size && block[3]._X < _width - _size ){
            _shape.clearBlocks(_context);

            if(checkMatrix(_shape, _gameMatrix, false, false, true, false)){
                updateGameMatrix(_shape,_gameMatrix, true);
                _shape.moveRight();
            }

            _shape.drawBlocks(_context);
        }

        // кнопка вгору (обертання)
        if(e.keyCode == codeKey.ROTATE){
            ++_rotator;
            _stoper = true;
            clearInterval(_interval);
            if( _rotator > 4) {
                _rotator = 1;
            }
            _shape.clearBlocks(_context);
                if(checkMatrix(_shape,_gameMatrix, false, false, false, true)){
                    updateGameMatrix(_shape,_gameMatrix, true);
                    _shape.Rotate(_rotator);
                }

            for(let s = 0 ; s < 3; s++ ){
                if(!checkMatrix(_shape, _gameMatrix, false, false, false)){
                    _shape.moveLeft();
                    updateGameMatrix(_shape, _gameMatrix, true);
                    updateGameMatrix(_shape, _gameMatrix);
                }
            }
            _interval = setInterval(gameProcess, 1000);
            _stoper = false;
        }

        updateGameMatrix(_shape,_gameMatrix);
        _shape.drawBlocks(_context);
    }

}

// SQUARE
function Square(x, y, size) {
    this._X = x;
    this._Y = y;
    this._size = size;
    this._color = "black";
}

Square.prototype.drawSquare = function(context){

    context.fillRect(this._X, this._Y, this._size, this._size);
    context.fillStyle = this._color;
}

Square.prototype.clearSquare = function (context) {

    context.clearRect(this._X, this._Y, this._size, this._size);
}

function Shape () {
    this._blocks = [];
}

Shape.prototype.drawBlocks = function (context) {
    for(let i = 0; i < this._blocks.length; i++ ){
       
        this._blocks[i].drawSquare(context);
    }
}

Shape.prototype.clearBlocks = function (context) {
    for(let i = 0; i < this._blocks.length; i++ ){

        this._blocks[i].clearSquare(context);
    }
}

Shape.prototype.moveBottom = function () {
    for( let i = 0; i < this._blocks.length; i++){
        this._blocks[i]._Y = this._blocks[i]._Y + this._blocks[i]._size;
    }
}

Shape.prototype.moveLeft = function () {
    for( let i = 0; i < this._blocks.length; i++){
        this._blocks[i]._X = this._blocks[i]._X - this._blocks[i]._size;
    }
}

Shape.prototype.moveRight = function () {
    for( let i = 0; i < this._blocks.length; i++){
        this._blocks[i]._X = this._blocks[i]._X + this._blocks[i]._size;
    }
}

Shape.prototype.moveTop = function () {
    for( let i = 0; i < this._blocks.length; i++){
        this._blocks[i]._Y = this._blocks[i]._Y - this._blocks[i]._size;
    }
}

function Shape_I () {
    Shape.call(this);
    this.createShape = function (x, y, size) {
        this._blocks.push(new Square(x, y, size));
        this._blocks.push(new Square(x - size, y, size));
        this._blocks.push(new Square(x + size, y, size));
        this._blocks.push(new Square(x - (size * 2), y, size));
    }

    this.Rotate = function (rotator) {
        switch (rotator){
            case 1:
                this._blocks[0]._X = this._blocks[0]._X - this._blocks[0]._size * 2;
                this._blocks[0]._Y = this._blocks[0]._Y - this._blocks[0]._size;

                this._blocks[1]._X = this._blocks[1]._X - this._blocks[1]._size;
                this._blocks[1]._Y = this._blocks[1]._Y - this._blocks[1]._size * 2;

                this._blocks[2]._X = this._blocks[2]._X - this._blocks[2]._size * 3;
                this._blocks[2]._Y = this._blocks[2]._Y;

                this._blocks[3]._X = this._blocks[3]._X;
                this._blocks[3]._Y = this._blocks[3]._Y - this._blocks[3]._size * 3;
                break;

            case 2:
                this._blocks[0]._X = this._blocks[0]._X + this._blocks[0]._size * 2;
                this._blocks[0]._Y = this._blocks[0]._Y + this._blocks[0]._size;

                this._blocks[1]._X = this._blocks[1]._X + this._blocks[1]._size;
                this._blocks[1]._Y = this._blocks[1]._Y + this._blocks[1]._size * 2;

                this._blocks[2]._X = this._blocks[2]._X + this._blocks[2]._size * 3;
                this._blocks[2]._Y = this._blocks[2]._Y;

                this._blocks[3]._X = this._blocks[3]._X;
                this._blocks[3]._Y = this._blocks[3]._Y + this._blocks[3]._size * 3;
                break;
            case 3:
                this._blocks[0]._X = this._blocks[0]._X - this._blocks[0]._size * 2;
                this._blocks[0]._Y = this._blocks[0]._Y - this._blocks[0]._size;

                this._blocks[1]._X = this._blocks[1]._X - this._blocks[1]._size;
                this._blocks[1]._Y = this._blocks[1]._Y - this._blocks[1]._size * 2;

                this._blocks[2]._X = this._blocks[2]._X - this._blocks[2]._size * 3;
                this._blocks[2]._Y = this._blocks[2]._Y;

                this._blocks[3]._X = this._blocks[3]._X;
                this._blocks[3]._Y = this._blocks[3]._Y - this._blocks[3]._size * 3;
                break;
            case 4:
                this._blocks[0]._X = this._blocks[0]._X + this._blocks[0]._size * 2;
                this._blocks[0]._Y = this._blocks[0]._Y + this._blocks[0]._size;

                this._blocks[1]._X = this._blocks[1]._X + this._blocks[1]._size;
                this._blocks[1]._Y = this._blocks[1]._Y + this._blocks[1]._size * 2;

                this._blocks[2]._X = this._blocks[2]._X + this._blocks[2]._size * 3;
                this._blocks[2]._Y = this._blocks[2]._Y;

                this._blocks[3]._X = this._blocks[3]._X;
                this._blocks[3]._Y = this._blocks[3]._Y + this._blocks[3]._size * 3;
                break;
        }
    }

}

function Shape_O() {
    Shape.call(this);
    this.createShape = function(x, y, size){
        this._blocks.push(new Square(x, y, size));
        this._blocks.push(new Square(x - size, y, size));
        this._blocks.push(new Square(x, y - size, size));
        this._blocks.push(new Square(x - size, y - size, size));
    }

    this.Rotate = function (rotator) {
        return true;
    }
}

function Shape_L() {
    Shape.call(this);
    this.createShape = function(x, y, size){
        this._blocks.push(new Square(x - size, y - size, size));
        this._blocks.push(new Square(x - size, y, size));
        this._blocks.push(new Square(x - size, y + size, size));
        this._blocks.push(new Square(x, y + size, size));
    }

    this.Rotate = function (rotator) {
        switch (rotator){
            case 1:
                this._blocks[0]._X = this._blocks[0]._X;
                this._blocks[0]._Y = this._blocks[0]._Y + this._blocks[0]._size * 2;

                this._blocks[1]._X = this._blocks[1]._X + this._blocks[1]._size;
                this._blocks[1]._Y = this._blocks[1]._Y + this._blocks[1]._size;

                this._blocks[2]._X = this._blocks[2]._X + this._blocks[2]._size * 2;
                this._blocks[2]._Y = this._blocks[2]._Y;

                this._blocks[3]._X = this._blocks[3]._X + this._blocks[3]._size;
                this._blocks[3]._Y = this._blocks[3]._Y - this._blocks[3]._size;
                break;

            case 2:
                this._blocks[0]._X = this._blocks[0]._X + this._blocks[0]._size;
                this._blocks[0]._Y = this._blocks[0]._Y;

                this._blocks[1]._X = this._blocks[1]._X;
                this._blocks[1]._Y = this._blocks[1]._Y - this._blocks[1]._size;

                this._blocks[2]._X = this._blocks[2]._X - this._blocks[2]._size;
                this._blocks[2]._Y = this._blocks[2]._Y - this._blocks[2]._size * 2;

                this._blocks[3]._X = this._blocks[3]._X - this._blocks[3]._size * 2;
                this._blocks[3]._Y = this._blocks[3]._Y - this._blocks[3]._size;
                break;

            case 3:
                this._blocks[0]._X = this._blocks[0]._X + this._blocks[0]._size;
                this._blocks[0]._Y = this._blocks[0]._Y - this._blocks[0]._size;

                this._blocks[1]._X = this._blocks[1]._X;
                this._blocks[1]._Y = this._blocks[1]._Y;

                this._blocks[2]._X = this._blocks[2]._X - this._blocks[2]._size;
                this._blocks[2]._Y = this._blocks[2]._Y + this._blocks[2]._size;

                this._blocks[3]._X = this._blocks[3]._X;
                this._blocks[3]._Y = this._blocks[3]._Y + this._blocks[3]._size * 2;
                break;

            case 4:
                this._blocks[0]._X = this._blocks[0]._X - this._blocks[0]._size * 2;
                this._blocks[0]._Y = this._blocks[0]._Y - this._blocks[0]._size;

                this._blocks[1]._X = this._blocks[1]._X - this._blocks[1]._size;
                this._blocks[1]._Y = this._blocks[1]._Y;

                this._blocks[2]._X = this._blocks[2]._X;
                this._blocks[2]._Y = this._blocks[2]._Y + this._blocks[2]._size;

                this._blocks[3]._X = this._blocks[3]._X + this._blocks[3]._size;
                this._blocks[3]._Y = this._blocks[3]._Y;
                break;
        }
    }
}

function Shape_J() {
    Shape.call(this);
    this.createShape = function(x, y, size){
        this._blocks.push(new Square(x, y - size, size));
        this._blocks.push(new Square(x, y, size));
        this._blocks.push(new Square(x, y + size, size));
        this._blocks.push(new Square(x - size, y + size, size));
    }
    this.Rotate = function (rotator) {
        switch (rotator){
            case 1:
                this._blocks[0]._X = this._blocks[0]._X + this._blocks[0]._size;
                this._blocks[0]._Y = this._blocks[0]._Y + this._blocks[0]._size * 2;

                this._blocks[1]._X = this._blocks[1]._X;
                this._blocks[1]._Y = this._blocks[1]._Y + this._blocks[1]._size;

                this._blocks[2]._X = this._blocks[2]._X - this._blocks[2]._size;
                this._blocks[2]._Y = this._blocks[2]._Y;

                this._blocks[3]._X = this._blocks[3]._X;
                this._blocks[3]._Y = this._blocks[3]._Y - this._blocks[3]._size;
                break;

            case 2:
                this._blocks[0]._X = this._blocks[0]._X - this._blocks[0]._size * 2;
                this._blocks[0]._Y = this._blocks[0]._Y;

                this._blocks[1]._X = this._blocks[1]._X - this._blocks[1]._size ;
                this._blocks[1]._Y = this._blocks[1]._Y - this._blocks[1]._size;

                this._blocks[2]._X = this._blocks[2]._X;
                this._blocks[2]._Y = this._blocks[2]._Y - this._blocks[2]._size * 2;

                this._blocks[3]._X = this._blocks[3]._X + this._blocks[3]._size;
                this._blocks[3]._Y = this._blocks[3]._Y - this._blocks[3]._size;
                break;

            case 3:
                this._blocks[0]._X = this._blocks[0]._X;
                this._blocks[0]._Y = this._blocks[0]._Y - this._blocks[0]._size;

                this._blocks[1]._X = this._blocks[1]._X + this._blocks[1]._size;
                this._blocks[1]._Y = this._blocks[1]._Y;

                this._blocks[2]._X = this._blocks[2]._X + this._blocks[2]._size * 2;
                this._blocks[2]._Y = this._blocks[2]._Y + this._blocks[2]._size;

                this._blocks[3]._X = this._blocks[3]._X + this._blocks[3]._size;
                this._blocks[3]._Y = this._blocks[3]._Y + this._blocks[3]._size * 2;
                break;

            case 4:
                this._blocks[0]._X = this._blocks[0]._X + this._blocks[0]._size;
                this._blocks[0]._Y = this._blocks[0]._Y - this._blocks[0]._size;

                this._blocks[1]._X = this._blocks[1]._X;
                this._blocks[1]._Y = this._blocks[1]._Y;

                this._blocks[2]._X = this._blocks[2]._X - this._blocks[2]._size;
                this._blocks[2]._Y = this._blocks[2]._Y + this._blocks[2]._size;

                this._blocks[3]._X = this._blocks[3]._X - this._blocks[3]._size * 2;
                this._blocks[3]._Y = this._blocks[3]._Y;
                break;
        }
    }
}

function Shape_S() {
    Shape.call(this);
    this.createShape = function (x, y, size) {
        this._blocks.push(new Square(x, y - size, size));
        this._blocks.push(new Square(x, y, size));
        this._blocks.push(new Square(x - size, y, size));
        this._blocks.push(new Square(x + size, y - size, size));
    }

    this.Rotate = function (rotator) {
        switch (rotator){
            case 1:
                this._blocks[0]._X = this._blocks[0]._X - this._blocks[0]._size;
                this._blocks[0]._Y = this._blocks[0]._Y;

                this._blocks[1]._X = this._blocks[1]._X;
                this._blocks[1]._Y = this._blocks[1]._Y - this._blocks[1]._size;

                this._blocks[2]._X = this._blocks[2]._X + this._blocks[2]._size;
                this._blocks[2]._Y = this._blocks[2]._Y;

                this._blocks[3]._X = this._blocks[3]._X - this._blocks[3]._size * 2;
                this._blocks[3]._Y = this._blocks[3]._Y - this._blocks[3]._size;
                break;

            case 2:
                this._blocks[0]._X = this._blocks[0]._X + this._blocks[0]._size;
                this._blocks[0]._Y = this._blocks[0]._Y + this._blocks[0]._size;

                this._blocks[1]._X = this._blocks[1]._X;
                this._blocks[1]._Y = this._blocks[1]._Y;

                this._blocks[2]._X = this._blocks[2]._X + this._blocks[2]._size;
                this._blocks[2]._Y = this._blocks[2]._Y - this._blocks[2]._size;

                this._blocks[3]._X = this._blocks[3]._X;
                this._blocks[3]._Y = this._blocks[3]._Y + this._blocks[3]._size * 2;
                break;

            case 3:
                this._blocks[0]._X = this._blocks[0]._X - this._blocks[0]._size;
                this._blocks[0]._Y = this._blocks[0]._Y - this._blocks[0]._size;

                this._blocks[1]._X = this._blocks[1]._X;
                this._blocks[1]._Y = this._blocks[1]._Y;

                this._blocks[2]._X = this._blocks[2]._X - this._blocks[2]._size;
                this._blocks[2]._Y = this._blocks[2]._Y + this._blocks[2]._size;

                this._blocks[3]._X = this._blocks[3]._X;
                this._blocks[3]._Y = this._blocks[3]._Y - this._blocks[3]._size * 2;
                break;

            case 4:
                this._blocks[0]._X = this._blocks[0]._X + this._blocks[0]._size;
                this._blocks[0]._Y = this._blocks[0]._Y;

                this._blocks[1]._X = this._blocks[1]._X;
                this._blocks[1]._Y = this._blocks[1]._Y + this._blocks[1]._size;

                this._blocks[2]._X = this._blocks[2]._X - this._blocks[2]._size;
                this._blocks[2]._Y = this._blocks[2]._Y;

                this._blocks[3]._X = this._blocks[3]._X + this._blocks[3]._size * 2;
                this._blocks[3]._Y = this._blocks[3]._Y + this._blocks[3]._size;
                break;
        }
    }
}

function Shape_Z() {
    Shape.call(this);
    this.createShape = function (x, y, size) {
        this._blocks.push(new Square(x, y - size, size));
        this._blocks.push(new Square(x, y, size));
        this._blocks.push(new Square(x + size, y, size));
        this._blocks.push(new Square(x - size, y - size, size));
    }
    this.Rotate = function (rotator) {
        switch (rotator){
            case 1:
                this._blocks[0]._X = this._blocks[0]._X - this._blocks[0]._size;
                this._blocks[0]._Y = this._blocks[0]._Y;

                this._blocks[1]._X = this._blocks[1]._X;
                this._blocks[1]._Y = this._blocks[1]._Y - this._blocks[1]._size;

                this._blocks[2]._X = this._blocks[2]._X - this._blocks[2]._size;
                this._blocks[2]._Y = this._blocks[2]._Y - this._blocks[2]._size * 2;

                this._blocks[3]._X = this._blocks[3]._X;
                this._blocks[3]._Y = this._blocks[3]._Y + this._blocks[3]._size;
                break;

            case 2:
                this._blocks[0]._X = this._blocks[0]._X + this._blocks[0]._size;
                this._blocks[0]._Y = this._blocks[0]._Y;

                this._blocks[1]._X = this._blocks[1]._X;
                this._blocks[1]._Y = this._blocks[1]._Y + this._blocks[1]._size;

                this._blocks[2]._X = this._blocks[2]._X + this._blocks[2]._size;
                this._blocks[2]._Y = this._blocks[2]._Y + this._blocks[2]._size * 2;

                this._blocks[3]._X = this._blocks[3]._X;
                this._blocks[3]._Y = this._blocks[3]._Y - this._blocks[3]._size;
                break;

            case 3:
                this._blocks[0]._X = this._blocks[0]._X - this._blocks[0]._size;
                this._blocks[0]._Y = this._blocks[0]._Y;

                this._blocks[1]._X = this._blocks[1]._X;
                this._blocks[1]._Y = this._blocks[1]._Y - this._blocks[1]._size;

                this._blocks[2]._X = this._blocks[2]._X - this._blocks[2]._size;
                this._blocks[2]._Y = this._blocks[2]._Y - this._blocks[2]._size * 2;

                this._blocks[3]._X = this._blocks[3]._X;
                this._blocks[3]._Y = this._blocks[3]._Y + this._blocks[3]._size;
                break;

            case 4:
                this._blocks[0]._X = this._blocks[0]._X + this._blocks[0]._size;
                this._blocks[0]._Y = this._blocks[0]._Y;

                this._blocks[1]._X = this._blocks[1]._X;
                this._blocks[1]._Y = this._blocks[1]._Y + this._blocks[1]._size;

                this._blocks[2]._X = this._blocks[2]._X + this._blocks[2]._size;
                this._blocks[2]._Y = this._blocks[2]._Y + this._blocks[2]._size * 2;

                this._blocks[3]._X = this._blocks[3]._X;
                this._blocks[3]._Y = this._blocks[3]._Y - this._blocks[3]._size;
                break;
        }
    }
}

function Shape_T() {
    Shape.call(this);
    this.createShape = function (x, y, size) {
        this._blocks.push(new Square(x, y - size, size));
        this._blocks.push(new Square(x, y, size));
        this._blocks.push(new Square(x + size, y, size));
        this._blocks.push(new Square(x - size, y, size));
    }
    this.Rotate = function (rotator) {
        switch (rotator){
            case 1:
                this._blocks[0]._X = this._blocks[0]._X;
                this._blocks[0]._Y = this._blocks[0]._Y;

                this._blocks[1]._X = this._blocks[1]._X - this._blocks[1]._size;
                this._blocks[1]._Y = this._blocks[1]._Y - this._blocks[1]._size;

                this._blocks[2]._X = this._blocks[2]._X - this._blocks[2]._size * 2;
                this._blocks[2]._Y = this._blocks[2]._Y;

                this._blocks[3]._X = this._blocks[3]._X;
                this._blocks[3]._Y = this._blocks[3]._Y - this._blocks[3]._size * 2;
                break;

            case 2:
                this._blocks[0]._X = this._blocks[0]._X;
                this._blocks[0]._Y = this._blocks[0]._Y;

                this._blocks[1]._X = this._blocks[1]._X + this._blocks[1]._size;
                this._blocks[1]._Y = this._blocks[1]._Y - this._blocks[1]._size;

                this._blocks[2]._X = this._blocks[2]._X;
                this._blocks[2]._Y = this._blocks[2]._Y - this._blocks[2]._size * 2;

                this._blocks[3]._X = this._blocks[3]._X + this._blocks[3]._size * 2;
                this._blocks[3]._Y = this._blocks[3]._Y;
                break;

            case 3:
                this._blocks[0]._X = this._blocks[0]._X;
                this._blocks[0]._Y = this._blocks[0]._Y;

                this._blocks[1]._X = this._blocks[1]._X + this._blocks[1]._size;
                this._blocks[1]._Y = this._blocks[1]._Y + this._blocks[1]._size;

                this._blocks[2]._X = this._blocks[2]._X + this._blocks[2]._size * 2;
                this._blocks[2]._Y = this._blocks[2]._Y;

                this._blocks[3]._X = this._blocks[3]._X;
                this._blocks[3]._Y = this._blocks[3]._Y + this._blocks[3]._size * 2;
                break;

            case 4:
                this._blocks[0]._X = this._blocks[0]._X + this._blocks[0]._size;
                this._blocks[0]._Y = this._blocks[0]._Y;

                this._blocks[1]._X = this._blocks[1]._X;
                this._blocks[1]._Y = this._blocks[1]._Y + this._blocks[1]._size;

                this._blocks[2]._X = this._blocks[2]._X + this._blocks[2]._size;
                this._blocks[2]._Y = this._blocks[2]._Y + this._blocks[2]._size * 2;

                this._blocks[3]._X = this._blocks[3]._X - this._blocks[3]._size;
                this._blocks[3]._Y = this._blocks[3]._Y;
                break;
        }
    }
}

Shape_T.prototype = Object.create(Shape.prototype);
Shape_Z.prototype = Object.create(Shape.prototype);
Shape_S.prototype = Object.create(Shape.prototype);
Shape_J.prototype = Object.create(Shape.prototype);
Shape_L.prototype = Object.create(Shape.prototype);
Shape_I.prototype = Object.create(Shape.prototype);
Shape_O.prototype = Object.create(Shape.prototype);

