<?php
class SimpleUser_Controller{
    protected $view;
    protected $model;
    protected $user_status;
    public function __construct()
    {
        if(isset($_SESSION['user']['user_status']))
        $this->user_status = $_SESSION['user']['user_status'];
        $this->view = new SimpleUser_View();
        $this->model = new SimpleUser_Model();
    }

    public function OfficeAction()
    {
        if($this->user_status == SIMPLE_USER){
            return array(
                'Content' => $this->view->UserOffice(),
                'Title' => 'Власний кабінет'
            );
        }
    }
    public  function  UserAdvertsAction(){
        if(isset($_GET["page"]) && isset($_SESSION['user'])){
            $page = (int)$_GET["page"];
            $maxOnPage = 3;
           $countAdvderts =(int)$this->model->CountAllAdverts($_SESSION['user']['user_login']);
           $countPages = ceil($countAdvderts / $maxOnPage);

           if($countPages){
               $start_point = ($page * $maxOnPage) - $maxOnPage;
               $step = $maxOnPage;

               // витягуємо дані з бази
               $adverts  =  $this->model->GetAdverts($start_point, $step, $_SESSION['user']['user_login']);
               // витягуємо шлях до головних картинок
               $images = $this->model->GetMainImg($adverts);


               return array(
                   "Content" => $this->view->CreateAdvertPage($adverts,$images,$page,$countPages),
                   "Title" => "Мої оголошення" );
           }
           else{
               return array(
                    "Title" => "Оголошень немає!"
               );
           }


        }
    }
    public  function  CreateAdvertAction(){
        if($this->user_status == SIMPLE_USER){
            return array(
                'Content' => $this->view->CreateAdvert(),
                'Title' => 'Створення оголошення'
            );
        }
    }
    public function NewAdvertAjax(){
        if(isset($_SESSION['user'])){
            if(isset($_POST['images']) && isset($_POST['advert'])){

                $form = json_decode($_POST['advert']);

                $id_advert = $this->model->CreateAdvert($form);

                $files = json_decode($_POST['images']);

                $this->model->SaveImages($id_advert,$files);
                return array('location' => "http://carbuy/simpleUser/successAdding");
            }
        }
    }
    public function RemoveAjax(){
        if(isset($_SESSION["user"]) && isset($_POST["id_advert"])){
            $id = $_POST['id_advert'];
            $this->model->RemoveAdvert($_SESSION["user"]["user_login"], $id);
            return array();
        }
    }
    public function SuccessAddingAction(){
        $Content = $this->view->SuccesCreating();
        return array(
            "Title" => "Запис додано",
            "Content"=> $Content
        );
    }
    public function OpeningAction(){
        if(isset($_SESSION["user"]) && isset($_GET["id"])){
            $advert = $this->model->GetAdvert($_GET["id"]);
            $src = $this->model->GetSrcImages($_GET["id"]);

           return array( "Content" => $this->view->Opening($advert,$_SESSION["user"],$src),
               "Title" => "Оголошення №" . $advert[0][0]);
        }
    }
    public function  EditingAction(){
        if(isset($_SESSION["user"]) && isset($_GET["id"])){
            $id =  str_replace('"', '', $_GET["id"]);
            $id =  str_replace("'", '', $id);

            $advert = $this->model->GetAdvert($id);
            $src = $this->model->GetSrcImages($id);

            return array(
                "Content" => $this->view->Editing($advert,$src),
                "Title" => "Редагування оголошення №" . $id);
        }
    }
    public function UpdateAdvertAjax(){
        if(isset($_SESSION["user"]) && isset($_POST["advert"])){
            $form = json_decode($_POST['advert']);
            $this->model->UpdateAdvert($form, $_SESSION["user"]["user_login"]);

            return array(
               "location" => "http://carbuy/simpleUser/successEditing"
            );
        }
    }
    public function SuccessEditingAction(){
        return array(
            "Title" => "Оголошення відредаговано",
            "Content" => $this->view->SuccessEditing()
        );
    }
}
