<?php
class SimpleUser_View{

    public function UserOffice(){
        $tpl = new Template("./templates/simple_user/office.tpl");
        return $tpl->fetch();
    }

    public function CreateAdvert(){
        $main_tpl = new Template("./templates/simple_user/office.tpl");
        $tpl =  new Template("./templates/simple_user/creator.tpl");
        $inputs = new Template("./templates/simple_user/inputs.tpl");
        $input_content = $inputs->fetch();
        $tpl->setParams(array("inputs" => $input_content));
        $Content = $tpl->fetch();
        $main_tpl->setParams(array("Content" => $Content));

        $val = $main_tpl->fetch();
        return $val;
    }

    public function SuccesCreating(){
        $tpl = new Template("./templates/simple_user/goodcreating.tpl");
        return $tpl->fetch();
    }

    public function CreateAdvertPage($adverts,$images, $page, $len){
        $Content = "";
        for($i = 0; $i < count($adverts); $i++){
            $advert_page = new Template("./templates/simple_user/advert.tpl");

            $params = array(
                "link" => "../" . $images[$i],
                "brand" => $adverts[$i]["car_brand"],
                "model" => $adverts[$i]["car_model"],
                "run" => $adverts[$i]["car_run"],
                "price" => $adverts[$i]["car_price"],
                "year" => $adverts[$i]["car_year"],
                "engine" => $adverts[$i]['car_engine'],
                "id_advert" => $adverts[$i]["id_advert"],
                "date_advert" => $adverts[$i]["id_date_creation"],
                "opening" => "http://carbuy/simpleUser/opening?id=" . $adverts[$i]["id_advert"],
                "editing" => "http://carbuy/simpleUser/editing?id=" . $adverts[$i]["id_advert"]
            );
            $advert_page->setParams($params);
            $Content = $Content . $advert_page->fetch();

        }
        $main_page = new Template("./templates/simple_user/pageAdverts.tpl");

        $back_link = "http://carbuy/simpleUser/userAdverts?page=" . strval((int)$page - 1);
        $next_link = "http://carbuy/simpleUser/userAdverts?page=" . strval((int)$page + 1);

        if($page == 1){
            $back_link = null;
        }

        if($page == $len){
            $next_link = null;
        }
        if($len == 1){
            $page = "";
        }

        $main_page->setParams(array(
            "Content" => $Content,
            "page" => $page,
            "back_link" => $back_link,
            "next_link" => $next_link ));

        return $main_page->fetch();
    }

    public function Opening($advert,$user,$src){
        $start_position = 2;

        $gallery = new Template("./templates/main/gallery.tpl");

        $imgs = "";
        if(count($src) > $start_position){
            for($i = $start_position; $i < count($src); $i++){
                $imgs = $imgs . "<img src='http://carbuy/img/" . $advert[0]["id_advert"] . "/" . $src[$i] . "' />";
            }
        }
        else{
            $imgs = $imgs . "<img src='http://carbuy/img/no-car.png' />";
        }


        $gallery->setParams(array("imgs" => $imgs));

        $Content = $gallery->fetch();
        $advert = $advert[0];

        $tpl = new Template("./templates/main/opening.tpl");
        $tpl->setParams(array(
            "Content" => $Content,
            "login" => $user["user_login"],
            "phone" => $user["user_phone"],
            "email" => $user["user_email"],
            "date_registration" => $user["user_date_registration"],
            "date_creation" => $advert["id_date_creation"],
            "brand" => $advert["car_brand"],
            "model" => $advert["car_model"],
            "run" => $advert["car_run"],
            "year" => $advert["car_year"],
            "engine" => $advert["car_engine"],
            "transmission" => $advert["car_transmission"],
            "city" => $advert["car_city"],
            "price" => $advert["car_price"],
            "other" => $advert["other"]
        ));

        return $tpl->fetch();

    }

    public function Editing($advert,$src){
        $imgs = "";
        for($i = 2; $i < count($src); $i++){
            $imgs = $imgs . "<img src='http://carbuy/img/" . $advert[0]["id_advert"] . "/" . $src[$i] . "' />";
        }

        $advert = $advert[0];
        $main_tpl = new Template("./templates/simple_user/editing.tpl");
        $input_tpl = new Template("./templates/simple_user/inputs.tpl");
        $input_tpl->setParams(array(
            "brand" => $advert["car_brand"] ,
            "model" => $advert["car_model"],
            "run" => $advert["car_run"],
            "year" => $advert["car_year"],
            "engine" => $advert["car_engine"],
            "transmission" => $advert["car_transmission"] ,
            "city" => $advert["car_city"],
            "price" => $advert["car_price"],
            "other" => $advert["other"],
            "imgs" => $imgs
        ));
        $main_tpl->setParams(array("inputs" => $input_tpl->fetch()));
        return $main_tpl->fetch();
    }

    public function SuccessEditing(){
        $tpl = new Template("./templates/simple_user/successEditing.tpl");
        return $tpl->fetch();
    }

}