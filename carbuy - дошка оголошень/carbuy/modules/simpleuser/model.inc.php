<?php
class  SimpleUser_Model
{
    protected $DB;

    public function __construct()
    {
        $this->DB = Core::GetDB();
    }

    public function  CreateAdvert($form){

        $rows = array(
            'user_login' => $_SESSION['user']['user_login'],
            'id_date_creation' => date("Y-m-d"),
            'car_brand' => $form->brand,
            'car_model' => $form->model,
            'car_run' => $form->run,
            'car_year' => $form->year,
            'car_engine' => $form->engine,
            'car_transmission' => $form->transmition,
            'car_city' => $form->city,
            'car_price' => $form->price,
            'other' => $form->other);

        $this->DB->Insert('advert', $rows);
        return $this->DB->GetPDO()->lastInsertId('advert');
    }

    public function SaveImages($folder_name, $files){
        $main_img = true;
        $folder_link = "img/" . $folder_name;

        if (mkdir($folder_link, 0777, true)) {

            $expansion = array("png","jpeg","jpg","gif");

            foreach ($files as  $data){

                for($i = 0; $i < count($expansion); $i++){
                    $str = "data:image/" . $expansion[$i] . ";base64,";
                    if(stristr($data,$str)) {
                        $str = $expansion[$i];
                        break;
                    }
                }

                if($str){
                    $data = str_replace("data:image/" . $str . ";base64,", '', $data);
                    $data = str_replace(' ', '+', $data);
                    $data = base64_decode($data);
                    if($main_img){
                        $file = $folder_link . "/" . 'main' . '.' . $str;
                        $main_img = false;
                    }
                    else{
                        $file = $folder_link . "/" . rand() . '.' . $str;
                    }

                    $success = file_put_contents($file, $data);
                }

                $str = false;
            }
        }
    }

    public function CountAllAdverts($user_login){
       $count = $this->DB->CountRows("advert", "user_login", $user_login);
       return $count[0][0];
    }

    public function GetAdverts($start,$step,$login){
        $result = $this->DB->Select(array('*'), 'advert', array('user_login' => $login), false, array("start" => $start, "step" => $step));

        return $result;
    }

    public function GetMainImg($adverts){
        $images = array();
        $foldel_link = "img/";
        $imgs_name = scandir ( $foldel_link);

        for($i = 0; $i < count($adverts); $i++){
            $src = "img/" . $adverts[$i]["id_advert"];
            $imgs_name = scandir ($src);
            $src = $src . "/" . $imgs_name[count($imgs_name) - 1];
                if(file_exists($src) && $imgs_name[count($imgs_name) - 1] != ".." && $imgs_name[count($imgs_name) - 1] != "."  ){
                    array_push($images,$src);
                }
                else{
                    array_push($images,"/img/no-car.png");
                }

        }

        return $images;
    }

    public function RemoveAdvert($login, $advert_id){
        $advert_id = str_replace(array("\"",""), "", $advert_id);
        $this->DB->Delete("advert",array(
            "user_login" => $login,
            "id_advert" => $advert_id)
        );
    }

    public function GetAdvert($id){
      return $this->DB->Select(array("*"),"advert",array("id_advert" => $id),false,null);
    }

    public function GetSrcImages($id){
        $src = "img/" . $id;
        return scandir ($src);
    }

    public function UpdateAdvert($advert, $user_login){
        $advert_in_db = $this->DB->Select(array("*"),"advert",array("id_advert" => $advert->id),false,null);
        //$login = $login[0];
        if($advert_in_db[0]["user_login"] == $user_login){
            $this->UpdateImgs($advert->imgs, $advert->id);

            //$update_params = array("" => );
           //$this->DB->Update();
        }

    }

    protected function UpdateImgs($src, $id){
        $link = "img/" . $id;
        $imgs_in_folder = scandir ($link);
        array_shift($imgs_in_folder);
        array_shift($imgs_in_folder);
        $expansion = array("png","jpeg","jpg","gif");

        // перевірка старих картинок, якщо деякі видалилися, то видаляємо з папки
        $old_imgs = $src[0];
        $tmp = false;
        for( $i = 0; $i < count($imgs_in_folder); $i++){
            for( $j = 0; $j < count($old_imgs); $j++){
                $http_link = "http://carbuy/" . $link . "/" . $imgs_in_folder[$i];
                if( $http_link  == $old_imgs[$j]){
                    $tmp = true;
                }
            }
            if(!$tmp){
                unlink($link . "/" . $imgs_in_folder[$i]);
            }
            $tmp = false;
        }
        // декодування нових картинок і занесення до папки
        $new_imgs = $src[1];
        $main_img = true;
        $folder_link = $link;
        $files = $new_imgs;

        foreach ($files as  $data){
            for($i = 0; $i < count($expansion); $i++){
                $str = "data:image/" . $expansion[$i] . ";base64,";
                if(stristr($data,$str)) {
                    $str = $expansion[$i];
                    break;
                }
            }

            if($str){
                $data = str_replace("data:image/" . $str . ";base64,", '', $data);
                $data = str_replace(' ', '+', $data);
                $data = base64_decode($data);
                $file = $folder_link . "/" . rand() . '.' . $str;
                $success = file_put_contents($file, $data);
            }
            $str = false;
        }

        // пошук головного фото, якщо немає, то додаємо
        $this->CheckMainImg($link, $expansion);
    }

    function CheckMainImg($link, $expansion){
        $deleted_main_img = false;

        $imgs_in_folder = scandir ($link);
        array_shift($imgs_in_folder);
        array_shift($imgs_in_folder);

        for( $i = 0; $i < count($imgs_in_folder); $i++){
            for( $j = 0; $j < count($expansion); $j++){
                if($imgs_in_folder[$i] == "main." . $expansion[$j]){
                    $deleted_main_img = true;
                    break;
                }
            }
        }

        if(!$deleted_main_img && count($imgs_in_folder) > 0){
            $name = $imgs_in_folder[count($imgs_in_folder) - 1];
            $info = new SplFileInfo($name);
            $exp = $info->getExtension();
            rename ( $link . '/' . $name , $link . "/" . "main." . $exp );
        }
    }

}