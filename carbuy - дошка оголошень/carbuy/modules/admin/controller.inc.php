<?php
class Admin_Controller
{
    protected $view;
    protected $model;
    protected $user_status;

    public function __construct()
    {
        if (isset($_SESSION['user']['user_status']))
            $this->user_status = $_SESSION['user']['user_status'];
        $this->view = new Admin_View();
        $this->model = new Admin_Model();
    }


    public function OfficeAction()
    {
        if ($this->user_status == ADMIN) {
            return array(
                'Content' => $this->view->AdminOffice(),
                'Title' => 'Кабінет адміністратора'
            );
        }
    }

    public function UserControlAction()
    {
        if ($this->user_status == ADMIN) {
            return array(
                'Content' => $this->view->UserControl(),
                'Title' => 'Керування користувачами'
            );
        }
    }

    public function SearchUserAjax()
    {
        if ($this->user_status == ADMIN && isset($_POST["user"])) {
            $users = $this->model->SearchUser($_POST["user"]);
            return array(
                'Content' => $this->view->WrapTag($users, 'li', 'user_login'),
            );
        }
    }

    public function UserInfoAjax()
    {
        if ($this->user_status == ADMIN && isset($_POST["user"])) {
            $user = $this->model->GetUser($_POST["user"]);

            if ($user) {
                return array(
                    'Content' => $this->view->UserInfo($user),
                );
            } else {
                return array(
                    'Content' => '',
                );
            }


        }
    }

    public function ChangeStatusAjax()
    {
        if ($this->user_status == ADMIN && isset($_POST["status"]) && isset($_POST["login"])) {
            $this->model->ChangeStatus($_POST["login"], $_POST["status"]);
            return null;
        }
    }

    public function DeleteUserAjax()
    {
        if ($this->user_status == ADMIN && isset($_POST["login"])) {
            $this->model->DeleteUser($_POST["login"]);
            return null;
        }
    }

    public function AdvertControlAction()
    {
        if ($this->user_status == ADMIN) {
            return array(
                'Content' => $this->view->AdvertControl(),
                'Title' => 'Керування оголошеннями'
            );
        }
    }

    public function SearchAdvertAjax()
    {
        if ($this->user_status == ADMIN && isset($_POST["advert"])) {
            $advert = $this->model->SearchAdvert($_POST["advert"]);
            return array(
                'Content' => $this->view->WrapTag($advert, 'li', 'id_advert'),
            );
        }
    }

    public function AdvertInfoAjax()
    {
        if ($this->user_status == ADMIN && isset($_POST["advert"])) {
            $advert = $this->model->GetAdvert($_POST["advert"]);

            if ($advert) {
                return array(
                    'Content' => $this->view->AdvertInfo($advert),
                );
            } else {
                return array(
                    'Content' => '',
                );
            }

        }
    }

    public function DeleteAdvertAjax()
    {
        if ($this->user_status == ADMIN && isset($_POST["advert"])) {
            $this->model->DeleteAdvert($_POST["advert"]);
            return null;
        }
    }

    public function StatisticsAction()
    {
        if ($this->user_status == ADMIN) {
            $statistic = $this->model->CountStatistic();
            return array(
                "Title" => "Статистика",
                "Content" => $this->view->Statistics($statistic)
            );
        }
    }

    public function CreateReportAction(){

        $this->model->CreateReport();
        Die();
    }
}
