<?php
class Admin_View{
    public function AdminOffice(){
        $tpl = new Template("./templates/admin/office.tpl");
        return $tpl->fetch();
    }

    public function UserControl(){
        $main_tpl = new Template("./templates/admin/office.tpl");

        $tpl = new Template("./templates/admin/userControl.tpl");
        $main_tpl->setParams(array("Content" => $tpl->fetch()));

        return $main_tpl->fetch();
    }
    public function UserInfo($user){
        $tpl = new Template("./templates/admin/userInfo.tpl");
        $tpl->setParams($user);
        return $tpl->fetch();
    }
    public function WrapTag($params, $tag, $column_name){
        $res ='';
        for($i=0; $i<count($params); $i++){
            $res = $res . "<" . $tag . ">" . $params[$i][$column_name] . "</" . $tag . ">";

        }

        return $res;
    }
    public function AdvertControl(){
        $main_tpl = new Template("./templates/admin/office.tpl");

        $tpl = new Template("./templates/admin/advertControl.tpl");
        $main_tpl->setParams(array("Content" => $tpl->fetch()));

        return $main_tpl->fetch();
    }
    public function AdvertInfo($advert){
        $tpl = new Template("./templates/admin/advertInfo.tpl");
        $tpl->setParams($advert);
        return $tpl->fetch();
    }
    public function Statistics($statistic){
        $main_tpl = new Template("./templates/admin/office.tpl");

        $tpl = new Template("./templates/admin/statistics.tpl");
        $tpl->setParams($statistic);

        $main_tpl->setParams(array("Content" => $tpl->fetch()));
        return $main_tpl->fetch();
    }
}