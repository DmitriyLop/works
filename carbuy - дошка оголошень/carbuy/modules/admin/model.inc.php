<?php
class  Admin_Model
{
    protected $DB;

    public function __construct()
    {
        $this->DB = Core::GetDB();
    }
    public function  SearchUser($name){
        $logins = $this->DB->Select(array('user_login'), 'users', array('user_login' => $name) ,true,false);
        return $logins;
    }
    public function GetUser($login){
        $user = $this->DB->Select(array('*'), 'users', array('user_login' => $login) ,false,false);
        if(!empty($user)){
            return $user[0];
        }
        else{
            return null;
        }

    }
    public function ChangeStatus($login,$status){
        $login = trim($login," ");
        $this->DB->Update("users", array("user_status" => $status), array("user_login" => $login));
    }
    public function DeleteUser($login){
        $login = trim($login," ");
        $this->DB->Delete('users',array("user_login" => $login));
        $this->DB->Delete('advert',array("user_login"=> $login));
    }
    public function  SearchAdvert($advert_id){
        $id = $this->DB->Select(array('id_advert'), 'advert', array('id_advert' => $advert_id) ,true,false);
        return $id;
    }
    public function GetAdvert($advert){
        $advert = $this->DB->Select(array('*'), 'advert', array('id_advert' => $advert) ,false,false);
        if(!empty($advert)){
            return $advert[0];
        }
        else{
            return null;
        }

    }

    public function DeleteAdvert($advert){
        $advert = trim($advert," ");
        $this->DB->Delete('advert',array("id_advert"=> $advert));
    }

    public function CountStatistic(){
        //статистика користувачів
        $where_str =" WHERE user_date_registration LIKE '%" . date("Y-m") . "-%'";
        $registration_in_month = $this->DB->CountRowsWithRange('users', 'user_id', $where_str);

        $where_str = " WHERE user_date_registration LIKE '%" . date("Y-m") . "-%' and user_status = 1";
        $checked_status_in_month = $this->DB->CountRowsWithRange('users', 'user_id', $where_str);

        $where_str = " WHERE user_date_registration LIKE '%" . date("Y-m") . "-%' and user_status = 0";
        $blocked_status_in_month = $this->DB->CountRowsWithRange('users', 'user_id', $where_str);

        $all_users = $this->DB->CountRowsWithRange('users', 'user_id', ' ');

        //статистика оголошень
        $where_str = " WHERE id_date_creation = '" . date("Y-m-d") . "'";
        $adverts_today = $this->DB->CountRowsWithRange('advert', 'id_advert', $where_str);

        $where_str = " WHERE id_date_creation LIKE '%" . date("Y-m") . "-%'";
        $adverts_in_month = $this->DB->CountRowsWithRange('advert', 'id_advert', $where_str);

        $all_adverts = $this->DB->CountRowsWithRange('advert', 'id_advert', '');

        return array(
            "users_in_month" => $registration_in_month[0][0],
            "checked_users_in_month" => $checked_status_in_month[0][0],
            "blocked_users_in_month" => $blocked_status_in_month[0][0],
            "all_users" => $all_users[0][0],
            "adverts_today" => $adverts_today[0][0],
            "adverts_in_month" => $adverts_in_month[0][0],
            "all_adverts"  => $all_adverts[0][0]
        );
    }

    public function CreateReport(){

        $statistics = $this->CountStatistic();

        $data = array(
            array( $statistics["users_in_month"]),
            array( $statistics["checked_users_in_month"]),
            array( $statistics["blocked_users_in_month"]),
            array( $statistics["all_users"])
        );

        // Начало конфигурации
        $textColour = array( 0, 0, 0 );
        $headerColour = array( 100, 100, 100 );
        $tableHeaderTopTextColour = array( 255, 255, 255 );
        $tableHeaderTopFillColour = array( 125, 152, 179 );
        $tableHeaderTopProductTextColour = array( 0, 0, 0 );
        $tableHeaderTopProductFillColour = array( 143, 173, 204 );
        $tableHeaderLeftTextColour = array( 99, 42, 57 );
        $tableHeaderLeftFillColour = array( 184, 207, 229 );
        $tableBorderColour = array( 50, 50, 50 );
        $tableRowFillColour = array( 213, 170, 170 );
        $reportName = "Carbuy statistics " .date("Y-m-d");
        $reportNameYPos = 160;
        $logoFile = "img/carbuy.png";
        $logoXPos = 50;
        $logoYPos = 108;
        $logoWidth = 110;
        $columnLabels = array( 'Count' );
        $rowLabels = array( "New users(month)", "Users with status(1)(month)", "Users with status(0)(month)", "All users" );
        $chartXPos = 20;
        $chartYPos = 250;
        $chartWidth = 160;
        $chartHeight = 80;
        $chartXLabel = "TypeUser";
        $chartYLabel = "2009 Sales";
        $chartYStep = 20000;
        $chartColours = array(
                          array( 255, 100, 100 ),
                          array( 100, 255, 100 ),
                          array( 100, 100, 255 ),
                          array( 255, 255, 100 ),
                        );
        // Конец конфигурации

        //Создаем титульную страницу
        $pdf = new FPDF( 'P', 'mm', 'A4' );
        $pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
        $pdf->AddPage();

        // Логотип
        $pdf->Image( $logoFile, $logoXPos, $logoYPos, $logoWidth );

        // Название отчета
        $pdf->SetFont( 'Arial', 'B', 24 );
        $pdf->Ln( 100 );
        $pdf->Cell( 0, 30, $reportName, 0, 0, 'C' );


        // cторінка статистики юзерів!!!!!!!!!!!!!!!!!!!!!!!!!
        $pdf->AddPage();
        $pdf->SetTextColor( $headerColour[0], $headerColour[1], $headerColour[2] );
        $pdf->SetFont( 'Arial', '', 17 );
        $pdf->Cell( 0, 30, $reportName, 0, 0, 'C' );

        $pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
        $pdf->SetFont( 'Arial', '', 20 );
        $pdf->Write( 15, "User statistics");
        $pdf->Ln( 20 );

        $pdf->SetDrawColor( $tableBorderColour[0], $tableBorderColour[1], $tableBorderColour[2] );
        $pdf->Ln( 15);
        $pdf->SetFont( 'Arial', 'B', 15 );

        $pdf->SetTextColor( $tableHeaderTopProductTextColour[0], $tableHeaderTopProductTextColour[1], $tableHeaderTopProductTextColour[2] );
        $pdf->SetFillColor( $tableHeaderTopProductFillColour[0], $tableHeaderTopProductFillColour[1], $tableHeaderTopProductFillColour[2] );
        $pdf->Cell( 95, 12, "User type", 1, 0, 'C', true );
        $pdf->SetTextColor( $tableHeaderTopTextColour[0], $tableHeaderTopTextColour[1], $tableHeaderTopTextColour[2] );
        $pdf->SetFillColor( $tableHeaderTopFillColour[0], $tableHeaderTopFillColour[1], $tableHeaderTopFillColour[2] );

        for ( $i=0; $i<count($columnLabels); $i++ ) {
          $pdf->Cell( 95, 12, $columnLabels[$i], 1, 0, 'C', true );
        }

        $pdf->Ln( 12 );

        $fill = false;
        $row = 0;

        foreach ( $data as $dataRow ) {
          $pdf->SetFont( 'Arial', 'B', 15 );
          $pdf->SetTextColor( $tableHeaderLeftTextColour[0], $tableHeaderLeftTextColour[1], $tableHeaderLeftTextColour[2] );
          $pdf->SetFillColor( $tableHeaderLeftFillColour[0], $tableHeaderLeftFillColour[1], $tableHeaderLeftFillColour[2] );
          $pdf->Cell( 95, 24, " " . $rowLabels[$row], 1, 0, 'L', $fill );

          $pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
          $pdf->SetFillColor( $tableRowFillColour[0], $tableRowFillColour[1], $tableRowFillColour[2] );
          $pdf->SetFont( 'Arial', '', 15 );
          for ( $i=0; $i<count($columnLabels); $i++ ) {
            $pdf->Cell( 95, 24, (  number_format( $dataRow[$i] ) . " users" ), 1, 0, 'C', $fill );
          }
          $row++;
          $fill = !$fill;
          $pdf->Ln( 24);
        }

        // cторінка статистики оголошень!!!!!!!!!!!!!!!!!!!!!!!!!
        $data = array(
            array( $statistics["adverts_today"]),
            array( $statistics["adverts_in_month"]),
            array( $statistics["all_adverts"])
        );

        $rowLabels = array( "Create today", "Created in a month", "All adverts");

        $pdf->AddPage();
        $pdf->SetTextColor( $headerColour[0], $headerColour[1], $headerColour[2] );
        $pdf->SetFont( 'Arial', '', 17 );
        $pdf->Cell( 0, 30, $reportName, 0, 0, 'C' );

        $pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
        $pdf->SetFont( 'Arial', '', 20 );
        $pdf->Write( 15, "Advert statistics");
        $pdf->Ln( 20 );

        $pdf->SetDrawColor( $tableBorderColour[0], $tableBorderColour[1], $tableBorderColour[2] );
        $pdf->Ln( 15);

        $pdf->SetFont( 'Arial', 'B', 15 );

        $pdf->SetTextColor( $tableHeaderTopProductTextColour[0], $tableHeaderTopProductTextColour[1], $tableHeaderTopProductTextColour[2] );
        $pdf->SetFillColor( $tableHeaderTopProductFillColour[0], $tableHeaderTopProductFillColour[1], $tableHeaderTopProductFillColour[2] );
        $pdf->Cell( 95, 12, "Advert type", 1, 0, 'C', true );

        $pdf->SetTextColor( $tableHeaderTopTextColour[0], $tableHeaderTopTextColour[1], $tableHeaderTopTextColour[2] );
        $pdf->SetFillColor( $tableHeaderTopFillColour[0], $tableHeaderTopFillColour[1], $tableHeaderTopFillColour[2] );

        for ( $i=0; $i<count($columnLabels); $i++ ) {
            $pdf->Cell( 95, 12, $columnLabels[$i], 1, 0, 'C', true );
        }

        $pdf->Ln( 12 );


        $fill = false;
        $row = 0;

        foreach ( $data as $dataRow ) {
            $pdf->SetFont( 'Arial', 'B', 15 );
            $pdf->SetTextColor( $tableHeaderLeftTextColour[0], $tableHeaderLeftTextColour[1], $tableHeaderLeftTextColour[2] );
            $pdf->SetFillColor( $tableHeaderLeftFillColour[0], $tableHeaderLeftFillColour[1], $tableHeaderLeftFillColour[2] );
            $pdf->Cell( 95, 24, " " . $rowLabels[$row], 1, 0, 'L', $fill );

            $pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
            $pdf->SetFillColor( $tableRowFillColour[0], $tableRowFillColour[1], $tableRowFillColour[2] );
            $pdf->SetFont( 'Arial', '', 15 );
            for ( $i=0; $i<count($columnLabels); $i++ ) {
                $pdf->Cell( 95, 24, (  number_format( $dataRow[$i] ) . " adverts" ), 1, 0, 'C', $fill );
            }
            $row++;
            $fill = !$fill;
            $pdf->Ln( 24);
        }

        $pdf->Output();
    }

}