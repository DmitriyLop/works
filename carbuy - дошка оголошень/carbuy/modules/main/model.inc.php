<?php
class  Main_Model
{
    protected $DB;

    public function __construct()
    {
        $this->DB = Core::GetDB();
    }

    public function  SearchBrand($name){
        $brands = $this->DB->Select(array(' * '), 'brand', array('brand_name' => $name) ,true,false);
        return $brands;
    }

    public function SearchModel($model, $brand){
        $id_brand = $this->DB->Select(array('id_brand'),'brand', array("brand_name" => $brand), true, false);

        $models = $this->DB->Select(array('*'),'model',array("id_brand" => $id_brand[0]['id_brand'], 'model_name'=> $model),true, false);

        return $models;

    }

    public function GetLastAdverts($count){
        $table = "advert";
        $varibles = "*";
        $where = false;
        $order_by = "'id_advert' DESC";
        $limit = $count;

        return $this->DB->HandMadeSelect($table,$varibles,$where,$order_by, $limit);
    }

    public function GetSrcMainImg($adverts){
        $bg_src = "img/no-car.png";

        $images = array();
        $foldel_link = "img/";
        $imgs_name = scandir ( $foldel_link);

        for($i = 0; $i < count($adverts); $i++){
            $src = "img/" . $adverts[$i]["id_advert"];
            $imgs_name = scandir ($src);
            $name_main_img = $imgs_name[count($imgs_name) - 1];
            if($this->CheckMainImg($name_main_img)){
                $src = $src . "/" . $name_main_img;
            }else{
                $src = $bg_src;
            }
            array_push($images,"$src");
        }

        return $images;
    }

    public function GetAllSrc($id){
        $folder_name = "img/";
        $src = $folder_name . $id;
        $imgs_name = scandir ($src);

        array_shift($imgs_name);
        array_shift($imgs_name);

        for($i = 0; $i < count($imgs_name); $i++){
            $imgs_name[$i] = $folder_name . $id . "/" . $imgs_name[$i];
        }
        if(count($imgs_name) > 0){
            return $imgs_name;
        }
        else{
            return array("img/no-car.png");
        }

    }

    public function GetInfo($id){
        $advert = $this->DB->Select(array("*"),"advert",array("id_advert" => $id),false,false);
        $advert = $advert[0];

        $user = $this->DB->Select(array("*"),"users",array("user_login" => $advert["user_login"]),false,false);
        $user = $user[0];

        return array_merge($advert, $user);
    }

    public function GetAdvertsWithSearch($page, $brand, $model, $min_run, $max_run, $min_year, $max_year, $city, $min_price, $max_price){
        $name_table = "advert";
        $varible_with_count = " Count(id_advert)";

        $where_car = "";

        if($brand != "" && $model != ""){
            $where_car = "car_brand like '%$brand%' and car_model like '%$model%' and ";
        }else
            if($brand != "" && $model == ""){
                $where_car = "car_brand like '%$brand%' and ";
            }
        if($min_run == ""){
            $min_run = 1;
        }
        if($max_run == ""){
            $max_run = PHP_INT_MAX;
        }
        if($min_price == ""){
            $min_price = 1;
        }
        if($max_price == ""){
            $max_price = PHP_INT_MAX;
        }

        $where_str = " car_run >= $min_run and car_run <= $max_run and car_year >= $min_year and car_year <= $max_year and car_city like '%$city%' and car_price >= $min_price and car_price <= $max_price";
        $where_str = $where_car . $where_str;
        $count_adverts = $this->DB->HandMadeSelect($name_table,$varible_with_count,$where_str,null, null);
        $count_adverts = $count_adverts[0][0];


        if($count_adverts > 0){
            $adverts_on_page = 3;

            $count_pages =  ceil($count_adverts / $adverts_on_page);
            $_SESSION["search"]["count_pages"] = $count_pages;

            if($page <= $count_pages){
                $start_limit = ($page * $adverts_on_page) - $adverts_on_page;
                $limit = "$start_limit , $adverts_on_page";

                $adverts = $this->DB->HandMadeSelect($name_table,"*",$where_str,null,$limit);
                return $adverts;
            }
        }
            return null;
    }

    protected function CheckMainImg($name){
        $extention = array(".png",".jpeg",".jpg");
        for($i = 0; $i < count($extention); $i++){
            if($name == "main" . $extention[$i]){
                return true;
            }
        }
        return false;
    }



}