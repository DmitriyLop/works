<?php
class Main_Controller{
    protected $view;
    protected $model;
    protected $user_status;
    public function __construct()
    {
        $this->view = new Main_View();
        $this->model = new Main_Model();
    }

    public  function  SearchBrandAjax(){
        if(isset($_POST['brand']) && isset($_POST['referal'])){
           $result = $this->model->SearchModel($_POST['referal'], $_POST['brand']);

            return array("Content" => $this->view->WrapTag($result,'li','model_name'));
        }
        else if(isset($_POST["referal"])){ //Принимаем данные

            $referal = trim(strip_tags(stripcslashes(htmlspecialchars($_POST["referal"]))));
            $result = $this->model-> SearchBrand($referal);

            return array("Content" => $this->view->WrapTag($result,'li', 'brand_name'));
        }
    }

    public function OfficeAction()
    {
        if($this->user_status == SIMPLE_USER){
            return array(
                'Content' => $this->view->UserOffice(),
                'Title' => 'Власний кабінет'
            );
        }
    }

    public function MainAction(){
        $number_adverts = 15;
        $adverts = $this->model->GetLastAdverts($number_adverts);
        $links = $this->model->GetSrcMainImg($adverts);

       return $this->view->MainPage($adverts,$links);
    }

    public function SearchingAction(){
        return array(
            "Title" => "Пошук оголошення",
            "Content" => $this->view->Searching()
        );
    }

    public function OpenAction(){
        if(isset($_GET["id"])){
            $info = $this->model->GetInfo($_GET["id"]);
            $src = $this->model->GetAllSrc($info["id_advert"]);

            return array(
                "Title" => "Оголошення №" . $info["id_advert"],
                "Content" => $this->view->Open($info, $src)
            );

        }
    }

    public function AboutCompanyAction(){
        return array("Content" => $this->view->AboutCompany(),
            "Title" => "Компанія Carbuy"
        );
    }

    public function AdvertsAction(){
        if(isset($_GET["page"]) && isset($_POST["brand"]) && isset($_POST["model"]) && isset($_POST["min_run"]) && isset($_POST["max_run"])
            && isset($_POST["min_year"]) && isset($_POST["max_year"]) && isset($_POST["city"]) && isset($_POST["min_price"]) && isset($_POST["max_price"]) ){

            $_SESSION["search"]["brand"] = $_POST["brand"];
            $_SESSION["search"]["model"] = $_POST["model"];
            $_SESSION["search"]["min_run"] = $_POST["min_run"];
            $_SESSION["search"]["max_run"] = $_POST["max_run"];
            $_SESSION["search"]["min_year"] = $_POST["min_year"];
            $_SESSION["search"]["max_year"] = $_POST["max_year"];
            $_SESSION["search"]["city"] = $_POST["city"];
            $_SESSION["search"]["min_price"] = $_POST["min_price"];
            $_SESSION["search"]["max_price"] = $_POST["max_price"];

            $adverts  = $this->model->GetAdvertsWithSearch($_GET["page"], $_POST["brand"], $_POST["model"], $_POST["min_run"], $_POST["max_run"],
                $_POST["min_year"], $_POST["max_year"], $_POST["city"], $_POST["min_price"] , $_POST["max_price"]);

        }
        else if(isset($_SESSION["search"]) && isset($_GET["page"])){
            $adverts = $this->model->GetAdvertsWithSearch($_GET['page'], $_SESSION["search"]["brand"], $_SESSION["search"]["model"], $_SESSION["search"]["min_run"],
                $_SESSION["search"]["max_run"], $_SESSION["search"]["min_year"],$_SESSION["search"]["max_year"],$_SESSION["search"]["city"],
                $_SESSION["search"]["min_price"], $_SESSION["search"]["max_price"] );
        }

        if(!empty($adverts)){
            $srcs = $this->model->GetSrcMainImg($adverts);

            return array(
                "Title" => "Знайдені оголошення",
                "Content" => $this->view->CreatePage($adverts,$srcs, $_GET["page"],$_SESSION["search"]["count_pages"])
            );
        }

        return array(
          "Title" => "Оголошень не знайдено!"
        );

    }
}
