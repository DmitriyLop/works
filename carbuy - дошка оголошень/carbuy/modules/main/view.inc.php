<?php
class Main_View{

    public function UserOffice(){
        $tpl = new Template("./templates/simple_user/office.tpl");
        return $tpl->fetch();
    }
    public function WrapTag($params, $tag, $column_name){
        $res ='';
        for($i=0; $i<count($params); $i++){
            $res = $res . "<" . $tag . ">" . $params[$i][$column_name] . "</" . $tag . ">";

        }

        return $res;
    }
    public function MainPage($adverts, $links){
        $Content = "";
        for($i = 0; $i < count($adverts); $i++){
            $tpl = new Template("./templates/simple_user/advert.tpl");
            $tpl->setParams(array(
                "id_advert" => $adverts[$i]["id_advert"],
                "opening" => "http://carbuy/main/open?id=" . $adverts[$i]["id_advert"],
                "brand" => $adverts[$i]["car_brand"],
                "model" => $adverts[$i]["car_model"],
                "run" => $adverts[$i]["car_run"],
                "engine" => $adverts[$i]["car_engine"],
                "year" => $adverts[$i]["car_year"],
                "price" => $adverts[$i]["car_price"],
                "link" => $links[$i]
            ));
            $Content = $Content . $tpl->fetch();
        }
        return $Content;
    }
    public function Searching(){
        $tpl = new Template("./templates/main/searching.tpl");
        return $tpl->fetch();
    }
    public function AboutCompany(){
        $tpl = new Template("./templates/main/aboutCompany.tpl");
        return $tpl->fetch();
    }
    public function Open($info,$src){
        $imgs = "";

        for($i = 0; $i < count($src); $i++){
                $imgs = $imgs . "<img src='http://carbuy/" . $src[$i] . "' />";
        }

        $gallery = new Template("./templates/main/gallery.tpl");
        $gallery->setParams(array("imgs" => $imgs));

        $Content = $gallery->fetch();

        $tpl = new Template("./templates/main/opening.tpl");
        $tpl->setParams(array(
            "Content" => $Content,
            "login" => $info["user_login"],
            "phone" => $info["user_phone"],
            "email" => $info["user_email"],
            "date_registration" => $info["user_date_registration"],
            "date_creation" => $info["id_date_creation"],
            "brand" => $info["car_brand"],
            "model" => $info["car_model"],
            "run" => $info["car_run"],
            "year" => $info["car_year"],
            "engine" => $info["car_engine"],
            "transmission" => $info["car_transmission"],
            "city" => $info["car_city"],
            "price" => $info["car_price"],
            "other" => $info["other"]
        ));

        return $tpl->fetch();
    }

    public function CreatePage($adverts, $srcs, $page, $count_pages){
        $len_on_page = count($adverts);

        $Content = "";
        for($i = 0; $i < count($adverts); $i++){
            $advert_page = new Template("./templates/simple_user/advert.tpl");

            $params = array(
                "link" => "../" . $srcs[$i],
                "brand" => $adverts[$i]["car_brand"],
                "model" => $adverts[$i]["car_model"],
                "run" => $adverts[$i]["car_run"],
                "price" => $adverts[$i]["car_price"],
                "year" => $adverts[$i]["car_year"],
                "engine" => $adverts[$i]['car_engine'],
                "id_advert" => $adverts[$i]["id_advert"],
                "date_advert" => $adverts[$i]["id_date_creation"],
                "opening" => "http://carbuy/simpleUser/opening?id=" . $adverts[$i]["id_advert"]
            );

            $advert_page->setParams($params);
            $Content = $Content . $advert_page->fetch();

        }

        $main_page = new Template("./templates/simple_user/pageAdverts.tpl");

        $back_link = "http://carbuy/main/adverts?page=" . strval((int)$page - 1);
        $next_link = "http://carbuy/main/adverts?page=" . strval((int)$page + 1);

        if($page == 1){
            $back_link = null;
        }

        if((int)$page == $count_pages){
            $next_link = null;
        }


        $main_page->setParams(array(
            "Content" => $Content,
            "page" => $page,
            "back_link" => $back_link,
            "next_link" => $next_link ));

        return $main_page->fetch();

    }
}