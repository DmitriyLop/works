<?php
class Users_View{

    public function Registration(){
        $tpl = new Template("./templates/users/registration.tpl");
        return $tpl->fetch();
    }

    public function SuccessfulRegistration(){
        $tpl = new Template("./templates/users/successful_registration.tpl");
        return $tpl->fetch();
    }

    public  function Login(){
        $tpl = new Template("./templates/users/login.tpl");
        return $tpl->fetch();
    }

    public function  EmailMessage($token){
        $tpl = new Template("./templates/users/email_message.tpl");
        $tpl->setParams(array("Token" => $token));
        return $tpl->fetch();
    }

    public function  SuccessfulChangeStatus(){
        $tpl = new Template("./templates/users/successful_change_status.tpl");
        return $tpl->fetch();
    }

}