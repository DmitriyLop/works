<?php
class  Users_Model
{
    protected $DB;

    public function __construct()
    {
        $this->DB = Core::GetDB();
    }

    public function CheckLogin($login, $password)
    {
        if (!empty($login) && !empty($password)) {
            $data = $this->DB->Select(array('user_login', 'user_password'), 'users', array('user_login' => $login), false, false);
            if (!empty($data)) {
                $hash = $data[0]['user_password'];
                if (password_verify($password, $hash)) {
                    return true;
                }
            }
        } else {
            $data = $this->DB->Select(array('user_login'), 'users', array('user_login' => $login), false, false);
            if (!empty($data)) {
                return true;
            }
        }

        return false;
    }

    public function CreateUser($login, $password, $phone, $email, $token)
    {

        $name_table = 'users';
        $params = array(
            'user_login' => $login,
            'user_password' => password_hash($password, PASSWORD_DEFAULT),
            'user_phone' => $phone,
            'user_email' => $email,
            'user_status' => 0,
            'user_token' => $token,
            'user_date_registration' => date("Y-m-d")
        );

        $this->DB->Insert($name_table, $params);
    }

    public function SendMessage($email, $content)
    {
        $to = 'dmitrylopatyuk@ukr.net';
        $subject = 'ПІДТВЕРДЖЕННЯ РЕЄСТРАЦІЇ';
        $message = $content;
        $headers = 'From: sender@gmail.com' . "\r\n" .
            'Reply-To: sender@gmail.com' . "\r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=utf-8' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $message, $headers);
    }

    public  function  UpdateStatus($token){
        $set_params = array("user_status" => 1);
        $where_params = array("user_token" => $token, "user_status" => 0);
        $table_name = "users";
        $this->DB->Update($table_name, $set_params, $where_params);
    }

    public function GetUser($login, $password){
        $column_name = array('user_login', 'user_password', 'user_phone', 'user_email', 'user_status', 'user_token', 'user_date_registration');
        $table_name = "users";
        $where_values = array('user_login'=> $login);
        $data = $this->DB->Select($column_name, $table_name, $where_values, false, false);
        if($data)
            $hash = $data[0]['user_password'];
            if (password_verify($password, $hash)) {
                return $data[0];
            }

        return null;
    }
}