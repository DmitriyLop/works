<?php
class Users_Controller
{
    protected $view;
    protected $model;

    public function __construct()
    {
        $this->view = new Users_View();
        $this->model = new Users_Model();
    }

    public function LoginAction()
    {
        return array(
            'Content' => $this->view->Login(),
            'Title' => 'Авторизація'
        );
    }

    public function NewLocationAction(){
        if(isset($_POST['login']) && isset($_POST['password'])) {
            $login = $_POST['login'];
            $password = $_POST['password'];
            $user = $this->model->GetUser($login, $password);
            if (!empty($user)) {
                $_SESSION['user'] = $user;
                if($user['user_status'] == SIMPLE_USER){
                    return array("login" => true, 'location_page' => 'http://carbuy/simpleUser/office');
                }
                else if($user['user_status'] == ADMIN){
                    return array("login" => true, 'location_page' => 'http://carbuy/admin/office');
                }
                else{
                    return array("login" => true, 'location_page' => 'http://carbuy/');
                }

            }
        }
    }

    //для js (перевірка перед входом в профіль)
    public function CheckLoginAjax(){
        if(isset($_POST['login']) && isset($_POST['password'])){
            $check_result =  $this->model->CheckLogin(($_POST['login']),($_POST['password']));
            if($check_result){
               return $this->NewLocationAction();
            }
            else{
                return array( "login" => $check_result );
            }
        }
        else if(isset($_POST["login"])){
            return array( "login" => $this->model->CheckLogin($_POST['login'],null) );
        }
    }
    public function LogoutAction()
    {
       if(isset($_SESSION["user"])){
           session_destroy();
           header('Location: http://carbuy/');
       }
    }
    public function RegistrationAction(){
        return array(
            'Content' => $this->view->Registration(),
            'Title' => 'Реєстрація'
        );
    }
    public function  StartRegistrationAction(){

        if( isset($_POST['login']) && isset($_POST["password"]) && isset($_POST["phone"]) && isset($_POST["email"]) ){
            $login = htmlentities($_POST['login']);
            $password = htmlentities($_POST["password"]);
            $phone = htmlentities($_POST["phone"]);
            $email = htmlentities($_POST["email"]);

            //перевірка на існування даного логіна
            if(!$this->model->CheckLogin($_POST['login'],null)){
                $token = password_hash($password . $login, PASSWORD_BCRYPT);
                $this->model->SendMessage($email, $this->view->EmailMessage($token));
                $this->model->CreateUser($login,$password,$phone,$email, $token);
            }

            return array (
                'Content' => $this->view->SuccessfulRegistration(),
                'Title' => 'Дякуємо за реєстрацію'
            );

        }
    }
    public function  ChangeStatusAction(){
        //логіка перевірки токена і користувача....
        if(isset($_GET['token'])){
            $this->model->UpdateStatus(htmlentities($_GET['token']));
        }
        //повертаємо результат
        return array (
            'Content' => $this->view->SuccessfulChangeStatus(),
            'Title' => 'Реєстрацію підтверджено'
        );
    }
}
