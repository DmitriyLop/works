<script src="/scripts/users/registration.js" async></script>

<form id="registration" class="col-12 col-mg-8 col-lg-8 col-xl-7" action="http://carbuy/users/startRegistration" method="post">
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Логін</label>
        <div class="col-sm-9">
            <input  pattern = "^[a-zA-Z](.[a-zA-Z0-9_-]*)$" type="text" class="form-control" id="login" name="login"  placeholder="логін" required>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Пароль</label>
        <div class="col-sm-9">
            <input pattern = "^([a-zA-Z0123456789]){6,}$" type="password" class="form-control" id="password" name="password" placeholder="пароль" required>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Пароль (ще раз)</label>
        <div class="col-sm-9">
            <input pattern = "^([a-zA-Z0123456789]){6,}$" type="password"  name="secondpass" id="repeat-password" class="form-control" placeholder="пароль (ще раз)" required>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Номер телефону</label>
        <div class="col-sm-9">
            <input pattern = "^\+380\d{3}\d{2}\d{2}\d{2}$" type="tel" name="phone" class="form-control" id="phone" placeholder="+380....." required>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">E-mail</label>
        <div class="col-sm-9">
            <input pattern = "^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$" type="email" class="form-control" id="e-mail" name="email" placeholder="e-mail" required>
        </div>
    </div>
    <div class="form-group row">
        <div class="form-group row col-sm-12 col-lg-8 col-xl-8">
            <p id="message" class="col-sm-12 col-form-label message"></p>
        </div>
        <div class="col-sm-12 col-lg-4 col-xl-4">
            <button id="button" type="submit" class=" col-12 btn btn-primary">Зареєструватися</button>
        </div>
    </div>
</form>
