<table class="table col-10">
    <tr>
        <td>
            Номер оголошення:
        </td>
        <td id="advertId">
            <?php if(isset($id_advert)) echo $id_advert ?>
        </td>
    </tr>
    <tr>
        <td>
            Автор:
        </td>
        <td>
            <?php if(isset($user_login)) echo $user_login ?>
        </td>
    </tr>
    <tr>
        <td>
            Додано:
        </td>
        <td >
            <?php if(isset($id_date_creation)) echo $id_date_creation ?>
        </td>
    </tr>
    <tr>
        <td>
            Авто:
        </td>
        <td>
            <?php if(isset($car_brand) && isset($car_model)) echo $car_brand . " " . $car_model ?>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <a href="#" id="advertDelete" class="btn btn-danger">Видалити оголошення</a>
        </td>
    </tr>
</table>