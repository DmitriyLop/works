<table class="table">
    <tr>
        <th colspan="2">
            Користувачі
        </th>
    </tr>
    <tr>
        <td>
            Зареєстровано за місяць:
        </td>
        <td>
            <?php if(isset($users_in_month)) echo $users_in_month ?>
        </td>
    </tr>
    <tr>
        <td>
            З підтвердженим статусом (за місяць):
        </td>
        <td>
            <?php if(isset($checked_users_in_month)) echo $checked_users_in_month ?>
        </td>
    </tr>
    <tr>
        <td>
            Заблоковані (за місяць):
        </td>
        <td>
            <?php if(isset($blocked_users_in_month)) echo $blocked_users_in_month ?>
        </td>
    </tr>
    <tr>
        <td>
            Загальна кількість(за весь час):
        </td>
        <td>
            <?php if(isset($all_users)) echo $all_users ?>
        </td>
    </tr>
</table>

<table class="table">
    <tr>
        <th colspan="2">
            Оголошення
        </th>
    </tr>
    <tr>
        <td>
            Створено за сьогодні:
        </td>
        <td>
            <?php if(isset($adverts_today)) echo $adverts_today ?>
        </td>
    </tr>
    <tr>
        <td>
            Створено за місяць:
        </td>
        <td>
            <?php if(isset($adverts_in_month)) echo $adverts_in_month ?>
        </td>
    </tr>
    <tr>
        <td>
            Загальна кількість:
        </td>
        <td>
            <?php if(isset($all_adverts)) echo $all_adverts ?>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <a href="http://carbuy/admin/createReport" class="btn col-12 btn-success">Звітувати</a>
        </td>
    </tr>
</table>
