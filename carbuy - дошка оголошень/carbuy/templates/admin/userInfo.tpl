<table class="table col-10">
    <tr>
        <td>
            Логін:
        </td>
        <td id="userLogin">
            <?php if(isset($user_login)) echo $user_login ?>
        </td>
    </tr>
    <tr>
        <td>
            Телефон:
        </td>
        <td >
            <?php if(isset($user_phone)) echo $user_phone ?>
        </td>
    </tr>
    <tr>
        <td>
            Email:
        </td>
        <td>
            <?php if(isset($user_email)) echo $user_email ?>
        </td>
    </tr>
    <tr>
        <td>
            Статус:
        </td>
        <td>
            <?php if(isset($user_status)) echo $user_status ?>
        </td>
    </tr>
    <tr>
        <td>
            Дата реєстрації:
        </td>
        <td>
            <?php if(isset($user_date_registration)) echo $user_date_registration ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php if ($user_status == 0): ?>
                <a href="#" id="userUnblock" class="btn btn-primary">Розблокувати</a>
            <?php endif; ?>
            <?php if ($user_status == 1): ?>
            <a href="#" id="userBlock" class="btn btn-warning">Заблокувати</a>
            <?php endif; ?>
        </td>
        <td>
            <a href="#" id="userDelete" class="btn btn-danger">Видалити користувача</a>
        </td>
    </tr>
</table>