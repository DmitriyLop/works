<div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">Марка:</label>
    <div class="col-10">
        <input autocomplete="off" class="form-control" id="brand" type="text" value="<?php if(isset($brand)) echo $brand ?>">
        <ul class="search_result col-11"></ul>
    </div>
</div>
<div class="form-group row">
    <label for="example-search-input" class="col-2 col-form-label">Модель:</label>
    <div class="col-10">
        <input autocomplete="off" class="form-control" id="model" type="search" value="<?php if(isset($model)) echo $model ?>">
        <ul class="search_result col-11"></ul>
    </div>
</div>
<div class="form-group row">
    <label for="example-email-input" class="col-2 col-form-label">Пробіг (ТИС.КМ.):</label>
    <div class="col-10">
        <input class="form-control" type="number" id="run" value="<?php if(isset($run)) echo $run ?>">
    </div>
</div>
<div class="form-group row">
    <label for="example-url-input" class="col-2 col-form-label">Рік</label>
    <div class="col-10">
        <input class="form-control" id="year" type="number" value='<?php if(isset($year)) echo $year ?>'>
    </div>
</div>
<div class="form-group row">
    <label for="example-tel-input" class="col-2 col-form-label">Двигун:</label>
    <div class="col-10">
        <input class="form-control" type="text" id="engine" value='<?php if(isset($engine)) echo $engine ?>'>
    </div>
</div>
<div class="form-group row">
    <label for="example-password-input"  class="col-2 col-form-label">Трансмісія:</label>
    <div class="col-10">
        <input class="form-control" id="transmition" type="text" value="<?php if(isset($transmission)) echo $transmission ?>">
    </div>
</div>
<div class="form-group row">
    <label for="example-number-input" class="col-2 col-form-label">Місто:</label>
    <div class="col-10">
        <input class="form-control" type="text" id="city" value="<?php if(isset($city)) echo $city ?>">
    </div>
</div>
<div class="form-group row">
    <label for="example-date-input" class="col-2 col-form-label">Ціна (USD):</label>
    <div class="col-10">
        <input class="form-control" id="price" type="number" value="<?php if(isset($price)) echo $price ?>">
    </div>
</div>
<div class="form-group row">
    <label for="example-datetime-local-input" class="col-2 col-form-label">Додатковий опис:</label>
    <div class="col-10">
        <textarea class="form-control" id="info" rows="3" value="<?php if(isset($other)) echo $other ?>"></textarea>
    </div>
</div>
<div class="form-group row">
    <label for="example-date-input" class="col-2 col-form-label">Фото:</label>
    <div class="col-10">
        <input type="file" title =" " accept=".png,.jpeg,.jpg"  multiple class="form-control-file">
    </div>
</div>
<div class="form-group row">
    <div for="example-date-input" class="col-1 col-lg-2 col-xl-2 col-md-2 col-form-label"></div>
    <div class="col-10" id="gallery">
        <?php if(isset($imgs)) echo $imgs ?>
    </div>
</div>

<script src="/scripts/main/search_car.js" defer async></script>