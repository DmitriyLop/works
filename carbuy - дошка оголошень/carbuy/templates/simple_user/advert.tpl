
<div class="row advert">

    <div class="main-img col-11 col-md-12 col-lg-6 col-xl-5">
        <div class="date-advert">
            <span>Оголошення №<span class="id-advert"><?php if(isset($id_advert)) echo $id_advert ?></span> </span>
            <span><?php if(isset($date_advert)) echo ", опубліковано " . $date_advert ?></span>
        </div>
        <img class="" src="<?php if(isset($link)) echo $link ?>"/>
    </div>
    <table class="">
        <tr>
            <td colspan="2">
               <h4><?php if(isset($brand) && isset($model)) echo ($brand . "  " . $model); ?></h4>
            </td>
        </tr>
        <tr>
            <td>
                Пробіг:
            </td>
            <td>
                <?php if(isset($run)) echo $run . " тис. км." ?>
            </td>
        </tr>
        <tr>
            <td>
                Двигун:
            </td>
            <td>
                <?php if(isset($engine)) echo $engine ?>
            </td>
        </tr>
        <tr>
            <td>
                Рік:
            </td>
            <td>
                <?php if(isset($year)) echo $year . "рік" ?>
            </td>
        </tr>
        <tr>
            <td>
                Ціна:
            </td>
            <td>
                <?php if(isset($price)) echo $price . "$" ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <a class="open-advert btn btn-secondary a-all-advert" href="<?php if(isset($opening)) echo $opening ?>">Переглянути</a>
                <?php
                    if (isset($editing)){
                         echo  "<a class=\"delete-advert  btn btn-danger a-all-advert\" href='#'>Видалити</a>";
                         echo  "<a class=\"update-advert  btn btn-success a-all-advert\" href=" . $editing . "'>Редагувати</a>";
                    }
                ?>
            </td>
        </tr>
    </table>
</div>

