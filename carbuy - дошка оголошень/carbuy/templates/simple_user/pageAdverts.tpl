<div class="adverts" col-12>
   <?php echo $Content ?>
</div>

<nav  class="page-nav" id="pageNav">
    <?php if (isset($back_link)): ?>
        <a href='<?php if(!empty($back_link)) echo $back_link ?>'>Назад</a>
    <?php endif; ?>

    <strong><?php if(isset($page)) echo $page ?></strong>

    <?php if (isset($next_link)): ?>
        <a href='<?php if(!empty($next_link) && $next_link != null)  echo $next_link ?>'>Вперед</a>
    <?php endif; ?>

</nav>

<script src="/scripts/simpleUser/userAdverts.js" defer async></script>