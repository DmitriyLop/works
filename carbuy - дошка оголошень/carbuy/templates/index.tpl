<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?=$Title; ?></title>
    <link href="/styles/style.css" rel="stylesheet"/>
    <link href="/styles/usersStyle.css" rel="stylesheet"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</head>
<body>

<div class="main-header d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
        <h5 id="logo-text" class="my-0 font-weight-normal">CarBuy</h5>
        <img id="logo" class="my-0 mr-md-auto font-weight-normal" src="http://carbuy/img/logo.png">
    
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="http://carbuy/">Головна</a>
        <a class="p-2 text-dark" href="http://carbuy/main/searching">Знайти авто</a>
        <a class="p-2 text-dark" href="http://carbuy/main/aboutCompany">Про нас</a>
    </nav>
    <?php
    if (!isset($_SESSION['user'])){
        echo "<a class='btn btn-link' href='/users/login'>Вхід</a>";
        echo "<a class='btn btn-primary' href='/users/registration'>Реєстрація</a>";
    }
    else
        if ($_SESSION['user']['user_status'] == 0){
            echo "<a class='btn btn-link' href='/users/login'>Вхід</a>";
            echo "<a class='btn btn-primary' href='/users/registration'>Реєстрація</a>";
        }
        else{
                if ($_SESSION['user']['user_status'] == 1){
                     echo "<a class='btn btn-link' href='/simpleuser/office'>Власний кабінет</a>";
                }
                    else{
                         echo "<a class='btn btn-link' href='/admin/office'>Власний кабінет</a>";
                    }
                echo "<a class='btn btn-link' href='/users/logout'>Вийти</a>";
            }
    ?>

</div>

<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h1 class="display-4"><?=$Title; ?></h1>

</div>

<div class="container">
    <div class="content">
        <?php if(isset($Content)) echo $Content ?>
    </div>
    <footer class="pt-4 my-md-5 pt-md-5 border-top">
        <div class="row">
            <div class="col-6 col-md text-center">
                <h5>Ми в соціальних мережах</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Instagram</a></li>
                    <li><a class="text-muted" href="#">Facebook</a></li>
                    <li><a class="text-muted" href="#">Twitter</a></li>
                    <li><a class="text-muted" href="#">Telegram</a></li>
                    <li><a class="text-muted" href="#">YouTube</a></li>
                </ul>
            </div>
            <div class="col-6 col-md text-center">
                <h5>Ресурси</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Знайти авто</a></li>
                    <li><a class="text-muted" href="#">Про нас</a></li>
                    <li><a class="text-muted" href="#">Головна</a></li>
                </ul>
            </div>
            <div class=" about-company col-6 col-md text-center">
                <h5>Про</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Компанію</a></li>
                    <li><a class="text-muted" href="#">Команду</a></li>
                    <li><a class="text-muted" href="#">Політику</a></li>
                    <li><a class="text-muted" href="#">Статистику</a></li>
                </ul>
            </div>
        </div>
        <div class="col-12 col-md">
            <small class="d-block mb-3 text-muted text-center">&copy; 2017-2018</small>
        </div>
    </footer>
</div>

</body>
</html>