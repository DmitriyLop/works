<main role="main">

    <div class="container marketing">
        <hr class="featurette-divider">
        <!-- Three columns of text below the carousel -->
        <div class="row">
            <h2 class="col-12 text-center">Наші перевеги</h2>
            <div class="col-lg-4">
                <img class="rounded-circle" src="http://carbuy/img/tpl/speed.png" alt="Generic placeholder image" width="140" height="140">
                <h3 class="text-center">Швидкість</h3>
                <p>Швидка реєстрація і ви вже зможете створювати власні оголошення та продати автомобіль. Одразу після створення оголошення воно відображатиметься в пошуку нашого сайту.</p>
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <img class="rounded-circle" src="http://carbuy/img/tpl/power.png" alt="Generic placeholder image" width="140" height="140">
                <h3 class="text-center">Продуктивність</h3>
                <p>Щодня наші співробітники контролюють процес роботи сайту та покращують його для вас і успішних продажів авто. Щоденно ми намагаємося ставати кращими. Ми цінуємо свій та ваш час.</p>
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <img class="rounded-circle" src="http://carbuy/img/tpl/old.jpg" alt="Generic placeholder image" width="140" height="140">
                <h3 class="text-center">Досвід</h3>
                <p>Наші співробітники - досвідчені і молоді особистості, які вміють аналізувати і творити. Щоденне статистичне звітування дозволяє нам набиратися досвіду і покращувати наші послуги.</p>
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->



        <hr class="featurette-divider">
        <h2 class="col-12 text-center info-h">Наші функціональні можливості</h2>

        <div class="row featurette">
            <div class="col-md-7 col-lg-7 col-xl-7">
                <h3 class="featurette-heading">Пошук автомобіля.<span class="text-muted">Зручний інтерфейс пошуку.</span></h3>
                <p class="lead">Без реєстрації ви швидко зможете знайти потрібний автомобіль. За допомогою зручної форми є змога обрати необхідні параметри авто та знайти варіант, який сподобається вам.</p>
            </div>
            <div class="col-md-5 col-lg-5 col-xl-5">
                <img class=" info-img featurette-image img-fluid mx-auto" src="http://carbuy/img/tpl/search.png" alt="search_car">
            </div>
        </div>

        <div class="row featurette">
            <div class="col-md-7 col-lg-7 col-xl-7 order-md-2">
                <h3 class="featurette-heading">Продаж автомобіля.<span class="text-muted">Зареєструйся і створи оголошення.</span></h3>
                <p class="lead">Швидко зареєструвавшись ви моментально можете створити власне оголошення, детально описавши ваш авто. Додавши оголошення ви зможете переглядати ваші оголошення і контролювати їх.</p>
            </div>
            <div class="col-md-5 col-lg-5 col-xl-4 order-md-1">
                <img class=" info-img featurette-image img-fluid mx-auto" src="http://carbuy/img/tpl/transport.png" alt="buy car">
            </div>
        </div>

        <div class="row featurette">
            <div class="col-md-7 col-lg-7 col-xl-7">
                <h3 class="featurette-heading">Контроль продаж. <span class="text-muted">Редагування оголошень.</span></h3>
                <p class="lead">Контролюйте свої оголошення. Для кращих продаж ви маєте можливість редагувати створені оголошення, видаляти та переглядати їх. Аналізуйте відгуки та створюйте ідеальні оголошення.</p>
            </div>
            <div class="col-md-5 col-lg-5 col-xl-3">
                <img class="info-img featurette-image img-fluid mx-auto" src="http://carbuy/img/tpl/review.png" alt="review">
            </div>
        </div>

    </div><!-- /.container -->
