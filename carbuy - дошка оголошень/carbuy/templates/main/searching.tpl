<div class="search-form col-12 col-lg-10 col-xl-9">
    <form class="center-block" method="post" action="http://carbuy/main/adverts?page=1">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Марка</label>
                <input autocomplete="off" type="text" class="form-control" id="brand" name="brand"  placeholder="">
                <ul class="search_result col-11"></ul>
            </div>
            <div class="form-group col-md-6">
                <label>Модель</label>
                <input autocomplete="off" type="text" class="form-control" id="model" name="model"  placeholder="">
                <ul class="search_result col-11"></ul>
            </div>
        </div>
        <div class="form-group">
            <label  class="">Пробіг (тис. км.)</label>
            <input type="number" class="form-control" min="1" max="10000000" name="min_run" value="1"  placeholder="від">
            <input type="number" class="form-control" name="max_run" min="1" max="10000000" value="1000000" placeholder="до">
        </div>
        <div class="form-group">
            <label  class="">Ціна (USD)</label>
            <input type="number" class="form-control" min="1" max="10000000" name="min_price" value="1"  placeholder="від">
            <input type="number" class="form-control" name="max_price" min="1" max="10000000" value="1000000" placeholder="до">
        </div>
        <div class="form-group">
            <label  class="">Рік</label>

            <select class="form-control" name="min_year">
                <option selected value="1945">1945</option>
            </select>

            <select class="form-control" name="max_year">
                <option selected value="1945">1945</option>
            </select>
        </div>

        <div class="form-group">
            <label  class="">Місто</label>
            <input type="text" name="city" class="form-control">
        </div>
        <div class="col-md-2 offset-md-10 row-centered">
            <button type="submit" class=" col-12 btn btn-primary">Знайти</button>
        </div>
    </form>
</div>

<script src="/scripts/main/option_generator.js" defer async></script>
<script src="/scripts/main/search_car.js" defer async></script>
