<div class="row">
    <div class=" col-12 col-md-4 col-lg-4 col-xl-4">
        <table class=" table">
            <th class="text-cente" colspan="2">Інформація про автора</th>
            <tr>
                <td>
                    Автор:
                </td>
                <td>
                    <?php echo $login ?>
                </td>
            </tr>
            <tr>
                <td>
                    Email:
                </td>
                <td>
                    <?php echo $email ?>
                </td>
            </tr>
            <tr>
                <td>
                    Телефон:
                </td>
                <td>
                    <?php echo $phone ?>
                </td>
            </tr>
            <tr>
                <td>
                    На сайті з:
                </td>
                <td>
                    <?php echo $date_registration ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="col-md-8 col-lg-8 col-xl-8">
        <?php echo $Content ?>
        <table class="table table-hover">
            <tr class="table-primary">
                <td>
                    Марка:
                </td>
                <td>
                    <?php echo $brand ?>
                </td>
            </tr>
            <tr class="table-secondary">
                <td>
                    Модель:
                </td>
                <td>
                    <?php echo $model ?>
                </td>
            </tr>
            <tr class="table-success">
                <td>
                    Пробіг:
                </td>
                <td>
                    <?php echo $run . " (тис.км.)" ?>
                </td>
            </tr>
            <tr class="table-danger">
                <td>
                    Рік:
                </td>
                <td>
                    <?php echo $year ?>
                </td>
            </tr>
            <tr class="table-warning">
                <td>
                    Двигун:
                </td>
                <td>
                    <?php echo $engine ?>
                </td>
            </tr>
            <tr class="table-info">
                <td>
                    Трансмісія:
                </td>
                <td>
                    <?php echo $transmission ?>
                </td>
            </tr>
            <tr  class="table-primary">
                <td>
                    Місто:
                </td>
                <td>
                    <?php echo $city ?>
                </td>
            </tr>
            <tr class="table-secondary">
                <td>
                    Ціна:
                </td>
                <td>
                    <?php echo $price . "$" ?>
                </td>
            </tr>
            <tr class="table-success">
                <td>
                    Інші дані:
                </td>
                <td>
                    <?php echo $other ?>
                </td>
            </tr>
        </table>
    </div>
</div>