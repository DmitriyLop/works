<?php
class Core
{
    private static $IndexTpl;
    private static $DB;
    private static $request_type;
    private static $type_answer_data;

    public static function Init()
    {
        session_start();

        self::$type_answer_data = "tpl";

        self::$DB = new DB(DATABASE_HOST, DATABASE_USER, DATABASE_PASS, DATABASE_DBNAME);

        if (isset($_POST['ajax']) || isset($_GET['ajax'])) {
            self::$request_type = "Ajax";
            self::$type_answer_data = !empty($_POST['ajax']) ? $_POST['ajax'] : $_GET['ajax'];
            return;
        }

        self::$request_type = "Action";

        self::$IndexTpl = new Template('templates/index.tpl');
        self::$IndexTpl->setParam('Title', 'Нові оголошення');
    }

    public static function GetDB()
    {
        return self::$DB;
    }

    public static function Run()
    {
        if (isset($_GET['path'])) {
            $path = $_GET['path'];
            $pathParts = explode('/', $path);
            $className = ucfirst(array_shift($pathParts) . '_Controller');
            $methodName = ucfirst(array_shift($pathParts) . (self::$type_answer_data == 'tpl' ? 'Action' : 'Ajax'));

            if (class_exists($className)) {
                if (method_exists($className, $methodName)) {
                    $controller = new $className();
                    $params = $controller->$methodName();
                    if (self::$request_type == "Ajax") {
                        echo json_encode($params);
                        return;
                    }
                    self::$IndexTpl->setParams($params);
                } else {
                    echo 404;
                }
            } else {
                echo 404;
            }
        }
        else{
            include "modules/main/controller.inc.php";
            $controller = new Main_Controller();
            self::$IndexTpl->SetParams(array( "Content" => $controller->MainAction() ));
        }
    }

    public static function Done()
    {
        if (self::$request_type == "Action") {
            self::$IndexTpl->display();
        }
    }
}

?>