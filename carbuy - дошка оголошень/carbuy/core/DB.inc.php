<?php
class DB{
    protected $pdo;
    public function __construct($host, $user, $pass, $dbname)
    {
          $this->pdo = new PDO("mysql:host={$host};charset=utf8;dbname={$dbname}", $user, $pass);
    }
    public  function  GetPDO(){
        return $this->pdo;
    }
    public function Select($columns, $tableName, $where_varible, $like, $limit){
        if(empty($where_varible)){
            //доробити
            $stmt = $this->pdo->query('SELECT $columns  FROM . $tableName');
            $result =  $stmt->fetchAll();
        }
        else{
            //зроблено
            $column_str = '';

            for( $i = 0; $i < count($columns); $i++){
                $column_str = $column_str  . htmlentities($columns[$i]);
                if($i != count($columns) - 1)
                    $column_str = $column_str . ',';
            }

            $where_str = '';

            $keys = array_keys($where_varible);

            $comparator = ' = ';
            if($like){
                for( $i = 0; $i < count($keys); $i++){
                    $where_varible[$keys[$i]] = "%" . $where_varible[$keys[$i]] . "%" ;
                }
             $comparator = ' LIKE ';
            }

            for( $i = 0; $i < count($keys); $i++){
                $where_str = $where_str . htmlentities($keys[$i]) . $comparator . "'" . htmlentities($where_varible[$keys[$i]]) .  "'";
                if( $i != count($keys) - 1)
                    $where_str = $where_str . " and ";
            }

            $range = "";
            if(!empty($limit)){
                $range = " LIMIT " . $limit["start"] . " , " . $limit["step"];
            }
            $sql = 'SELECT ' . $column_str . ' FROM ' . $tableName . ' where ' .  $where_str . $range;

            $stmt = $this->pdo->query($sql);
            $result =  $stmt->fetchAll();
        }

        return $result;

    }
    public function Insert($table, array $array)
    {

        $db = $this->pdo;

        $count = count($array);
        $fields = NULL;

        foreach ($array as $key => $value) {
            $fields .= "{$key},";
            $values[] = $value;
        }

        $fields = substr($fields, 0, -1);
        $fields = "({$fields})";
        $placeholders = "(?";

        for ($i = 1; $i <= $count - 1; $i++) {
            $placeholders .= ",?";
        }

        $placeholders .= ")";

        $sql = ("INSERT INTO {$table} {$fields} VALUES $placeholders");

        try {
            $stmt = $db->prepare($sql);
            $db->beginTransaction();
            for ($i = 1; $i <= $count; $i++) {
                $stmt->bindValue($i, $values[$i - 1]);
                //echo $values[$i - 1];
            }
            $stmt->execute();
            //$db->commit();
        } catch (PDOException $e) {
            $db->rollBack();
        }


    }
    public function  Update($table, $table_params, $where_params ){
        $db = $this->pdo;
        $row_keys = array_keys($table_params);
        $where_keys = array_keys($where_params);

        $set_str =" SET ";

        for( $i = 0; $i < count($table_params); $i++){
            $set_str = $set_str . $row_keys[$i] . ' = ' . $table_params[$row_keys[$i]];
            if($i < count($table_params) - 1)
                $set_str = $set_str . ", ";
        }

        $where_str = " WHERE ";

        for($i = 0; $i < count($where_params); $i++){
            $where_str = $where_str . $where_keys[$i] . " = " . "'" . $where_params[$where_keys[$i]] . "'";
            if($i < count($where_params) - 1){
                $where_str = $where_str . " and ";
            }
        }

        try {
            $sql = "UPDATE " . $table . $set_str . $where_str;
            $stmt = $db->prepare($sql);
            $stmt->execute();
        } catch (PDOException $e) {
            $db->rollBack();
        }
    }
    public function Delete($table, $where_params){
        $db = $this->pdo;
        $where_keys = array_keys($where_params);

        $where_str = " WHERE ";

        for($i = 0; $i < count($where_params); $i++) {
            $where_str = $where_str . $where_keys[$i] . " = " . "'" . $where_params[$where_keys[$i]] . "'";
            if ($i < count($where_params) - 1) {
                $where_str = $where_str . " and ";
            }
        }

            try {
                $sql = "DELETE FROM " .  $table . $where_str;
                $stmt = $db->prepare($sql);
                $stmt->execute();
            } catch (PDOException $e) {
                $db->rollBack();
            }

    }
    public function  CountRows($table, $count_varible, $where_varible){
        $table = htmlentities($table);
        $count_varible = htmlentities($count_varible);
        $where_varible = htmlentities($where_varible);

        $sql = 'SELECT ' . "COUNT(" .  $count_varible . ') FROM ' . $table . ' where ' . $count_varible . " = " . "'" . $where_varible . "'";

        $stmt = $this->pdo->query($sql);
        $result =  $stmt->fetchAll();

        return $result;
    }
    public function  CountRowsWithRange($table, $count_varible, $where_str){
        $table = htmlentities($table);
        $count_varible = htmlentities($count_varible);
        $where_str = htmlentities($where_str);

        $sql = 'SELECT ' . "COUNT(" .  $count_varible . ') FROM ' . $table . ' ' . $where_str;

        $stmt = $this->pdo->query($sql);
        $result =  $stmt->fetchAll();

        return $result;
    }

    public function HandMadeSelect($table,$varibles,$where,$order_by, $limit){
        $table = htmlentities($table);
        $varibles = htmlentities($varibles);
        //$where = htmlentities($where);
        $order_by = htmlentities($order_by);
        $limit = htmlentities($limit);

        if($order_by && $limit && $where){
            $sql = " SELECT " . $varibles . " FROM " . $table . " WHERE " . $where . " ORDER BY " . $order_by . " LIMIT " . $limit;
        }
        else if($order_by && $where){
            $sql = " SELECT " . $varibles . " FROM " . $table . " WHERE " . $where . " ORDER BY " . $order_by;
        }
        else if($limit && $where){
            $sql = " SELECT " . $varibles . " FROM " . $table . " WHERE " . $where . " LIMIT " . $limit;
        }
        else if($where){
            $sql = " SELECT " . $varibles . " FROM " . $table . " WHERE " . $where;
        }
        else if($order_by && $limit){
            $sql = " SELECT " . $varibles . " FROM " . $table . " ORDER BY " . $order_by . " LIMIT " . $limit;
        }

        try {
            $stmt = $this->pdo->query($sql);
            $result = $stmt->fetchAll();
            return $result;
        }
        catch (PDOException $e){
            return null;
        }

    }
}

?>