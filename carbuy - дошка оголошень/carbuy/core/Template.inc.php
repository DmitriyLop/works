<?php

class Template
{
    protected $Params;
    protected $FileName;

    public function __construct($fileName)
    {
        $this->Params = array();
        $this->FileName = $fileName;
    }

    public function setParam($name, $value)
    {
        $this->Params[$name] = $value;
    }

    public function setParams($values)
    {
        if($values){
            $names = array_keys($values);

            for ($i = 0; $i < count($names); $i++) {
                $this->Params[$names[$i]] = $values[$names[$i]];
            }
        }

    }

    public function fetch()
    {
        extract($this->Params);
        ob_start();
        include($this->FileName);
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    public function display()
    {
        echo $this->fetch();
    }
}

?>