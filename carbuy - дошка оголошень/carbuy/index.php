<?php
include ('config/database.inc.php');
include ('config/user_status.inc.php');
require('Fpdf/fpdf.php');

error_reporting(E_ALL);
set_include_path('core');
spl_autoload_extensions('.inc.php');
spl_autoload_register();
spl_autoload_register(function ($className){
    $parts = explode('_',$className);
    $path = 'modules/'.strtolower(array_shift($parts)).'/'.strtolower(array_shift($parts)).'.inc.php';
    if (is_file($path)){
        include ($path);
    }
});

Core::Init();
Core::Run();
Core::Done();