var brand = document.getElementById('brand');
var model = document.getElementById('model');
var run = document.getElementById('run');
var year = document.getElementById('year');
var engine = document.getElementById('engine');
var transmition = document.getElementById('transmition');
var city = document.getElementById('city');
var price = document.getElementById('price');
var otherInfo = document.getElementById('info');


var input = document.querySelector('.form-control-file');
var gallery = document.querySelector('#gallery');
var images = [];
var form = document.querySelector('.form');
var request = new XMLHttpRequest();

function getForm(){
    return {'brand' : brand.value,
            'model' : model.value,
            'run' : run.value,
            'year' : year.value,
            'engine' : engine.value,
            'transmition' : transmition.value,
            'city' : city.value,
            'price' : price.value,
            'other' : otherInfo.value};
}

form.addEventListener('submit', function (event) {
    event.preventDefault();
    //event.target.disabled = true;

    var xhr = new XMLHttpRequest();
    xhr.open("POST", '/simpleUser/newAdvert', true);

    //Передает правильный заголовок в запросе
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function () {//Вызывает функцию при смене состояния.
        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
            var answer = JSON.parse(xhr.responseText);
            window.location = answer.location;
        }
    };
    var rows = getForm();
    xhr.send(`ajax=params&images=${JSON.stringify(images)}&advert=${JSON.stringify(rows)}`);

});

input.addEventListener('change', function (e) {
    var files = e.target.files;
    for (let i = 0; i < files.length; i++) {
        var file = files[i];
        var reader = new FileReader();
        reader.readAsDataURL(file);

        reader.onloadend = function () {
            let img = this.result;
            images.push(img)
            CreatePreview(gallery, img);
        }
    }
});

function CreatePreview(gallery, src) {
    var div = document.createElement('div');
    div.classList.add("col-xl-3");
    div.classList.add("col-lg-3");
    div.classList.add("col-md-4");
    div.classList.add("col-sm-6");
    div.classList.add("col-12");

    var img = document.createElement('img');
    img.classList.add("img-preview");
    img.classList.add('col-11');
    img.setAttribute('src', src);

    var button  = document.createElement("button");
    button.classList.add('col-12');
    button.innerHTML = 'Видалити';

    div.appendChild(img);
    div.appendChild(button);
    gallery.appendChild(div);
}

gallery.addEventListener('click',function (event) {
    if(event.target.tagName ='BUTTON'){
        var father = event.target.parentNode
        for(let i = 0; i < gallery.children.length; i++){
            if(gallery.children[i] == father){
                images.splice(i,1);
            }
        }
        father.remove();
    }
});

