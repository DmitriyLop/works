
var minElem = document.getElementsByName("min_year");
minElem  = minElem[0];
var maxElem = document.getElementsByName("max_year");
maxElem = maxElem[0];


var max_year = new Date();
max_year = max_year.getFullYear();
var min_year = 1946;

for(let i = min_year; i <= max_year; i++){
    var option = document.createElement("option");
    option.innerHTML = i;
    option.setAttribute('value',i);

    minElem.appendChild(option);

    var secondOption = option.cloneNode(true);

    if( i == max_year){
        secondOption.setAttribute("selected","selected");
    }

    maxElem.appendChild(secondOption);
}