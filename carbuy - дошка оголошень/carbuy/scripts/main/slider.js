slider = document.getElementById("slider");
backButton = document.getElementById("backImg");
backButton.style.display = "none";
nextButton = document.getElementById("nextImg");


counter = document.getElementsByClassName("img-counter");
counter = counter[0];

imgs = slider.getElementsByTagName("img");
counter.innerHTML = "1 з " + imgs.length;


src = [];
for(let i = 0; i < imgs.length; i++){
    src[i] = imgs[i].getAttribute("src");
    imgs[i].style.display = "none";
}

slider.style.background =  "url(\"" + src[0] + "\")" + " no-repeat";
slider.style.backgroundSize ="cover";

var imgLen = src.length;

if(imgLen == 1 ){
    nextButton.style.display = "none";
}

var imgNumber = 1;
slider.addEventListener("click",function (event) {
   if(event.target.id == "backImg"){
        if(imgNumber > 1){
            --imgNumber;
            slider.style.background =  "url(\"" + src[imgNumber - 1] + "\")" + " no-repeat";
            counter.innerHTML = imgNumber + " з " + imgs.length;
            slider.style.backgroundSize ="cover";
        }
        if(imgNumber == 1){
            backButton.style.display = "none";
        }
        if(imgNumber == imgLen - 1) {
            nextButton.style.display = "inline-block";
        }
   }
   else
       if(event.target.id == "nextImg"){
            if(imgNumber < imgLen){
                ++imgNumber;
                slider.style.background =  "url(\"" + src[imgNumber - 1] + "\")" + " no-repeat";
                slider.style.backgroundSize ="cover";
                counter.innerHTML = imgNumber + " з " + imgs.length;
            }
            if(imgNumber == imgLen){
                nextButton.style.display = "none";
            }
            if(imgNumber == 2){
                backButton.style.display = "inline-block";
            }
       }
});