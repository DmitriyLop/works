
var mainBlock = document.querySelector(".adverts");

mainBlock.addEventListener("click",checkEvents);

function checkEvents(event){
    if(event.target.classList.contains("delete-advert")){
            event.preventDefault();
           deleteAdvert(event.target);
    }
}

function deleteAdvert(target) {
 var block = getParentElement(target,5);
 var id_advert = block.querySelector(".id-advert");
 id_advert = id_advert.innerHTML;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", '/simpleUser/remove', true);

    //Передает правильный заголовок в запросе
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function () {//Вызывает функцию при смене состояния.
        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
            block.remove();
        }
    };
    alert(id_advert);
    xhr.send(`ajax=params&id_advert=${JSON.stringify(id_advert)}`);
}


function getParentElement(element,step) {
    for(let i = 0; i < step; i++){
        element = element.parentNode;
    }
    return element;
}