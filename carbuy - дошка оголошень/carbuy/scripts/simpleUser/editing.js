var gallery = document.getElementById("gallery");
var imgs = gallery.getElementsByTagName("img");
var input = document.querySelector('.form-control-file');

var sendButton = document.getElementById("checkLogin");
sendButton.addEventListener("click",function (event) {
    event.preventDefault();
    SendForm();
});

var newImgs = [];
var xhr = new XMLHttpRequest();
createPreview(gallery,imgs);

function createPreview(gallery, imgs) {
    var len = imgs.length;
    for(let i = 0; i < len; i++){
        var div = document.createElement('div');
        div.classList.add("col-xl-3");
        div.classList.add("col-lg-3");
        div.classList.add("col-mg-3");
        div.classList.add("col-12");


        var img = imgs[i].cloneNode(true);
        img.classList.add("img-preview");
        img.classList.add('col-11');

        var button  = document.createElement("button");
        button.classList.add("but-del");
        button.classList.add('col-12');
        button.innerHTML = 'Видалити';

        div.appendChild(img);
        div.appendChild(button);
        gallery.appendChild(div);
    }

    var galleryLen = gallery.children.length;
    var minusEl = 0;
    for (let i = 0; i < galleryLen; i++){
        console.log(gallery);
        var children = gallery.children[i - minusEl];
        if(children.tagName != "DIV"){
            gallery.children[i - minusEl].remove();
            ++minusEl;
        }
    }
}

gallery.addEventListener("click",function (event) {
    if(event.target.classList.contains("but-del")){
       event.preventDefault();
        deletePreview(event.target);
   }
   else if( event.target.classList.contains("new-but-del")){
       event.preventDefault();
        for(let i = 0; i < newImgs.length; i++){
            if(newImgs[i] == event.target.parentNode.children[0].getAttribute("src")){
                console.log(newImgs[i]);
                newImgs.splice(i,1);
                console.log(newImgs[i]);
                break;
            }
        }
        deletePreview(event.target);
   }

});

input.addEventListener('change', function (e) {
    var files = e.target.files;
    for (let i = 0; i < files.length; i++) {
        var file = files[i];
        var reader = new FileReader();
        reader.readAsDataURL(file);

        reader.onloadend = function () {
            let img = this.result;
            newImgs.push(img)
            createNewPreview(gallery, img);
        }
    }
});

function createNewPreview(gallery, src) {
    var div = document.createElement('div');
    div.classList.add("col-xl-3");
    div.classList.add("col-lg-3");
    div.classList.add("col-mg-3");
    div.classList.add("col-12");

    var img = document.createElement('img');
    img.classList.add("img-preview");
    img.classList.add('col-11');
    img.setAttribute('src', src);

    var button  = document.createElement("button");
    button.classList.add('col-12');
    button.classList.add("new-but-del");
    button.innerHTML = 'Видалити';

    div.appendChild(img);
    div.appendChild(button);
    gallery.appendChild(div);
}

function deletePreview(element, imgElements, deleteOnServer) {
    element = element.parentNode;
    element.remove();
}

function SendForm(){
    xhr.open("POST", '/simpleUser/updateAdvert', true);
    
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {//Вызывает функцию при смене состояния.
        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
            var answer = JSON.parse(xhr.responseText);
            window.location = answer.location;
        }
    };
    
    var advert = writeRowsAdvert();
    xhr.send(`ajax=params&advert=${JSON.stringify(advert)}`);
}

function writeRowsAdvert() {
    var brand = document.getElementById('brand');
    var model = document.getElementById('model');
    var run = document.getElementById('run');
    var year = document.getElementById('year');
    var engine = document.getElementById('engine');
    var transmition = document.getElementById('transmition');
    var city = document.getElementById('city');
    var price = document.getElementById('price');
    var otherInfo = document.getElementById('info');

    var id = document.getElementsByTagName("h1");
    id = id[0].innerHTML;
    id = id.replace("Редагування оголошення №","");

    imgs = connectedImgs();

    return {
        'id' : id,
        'brand' : brand.value,
        'model' : model.value,
        'run' : run.value,
        'year' : year.value,
        'engine' : engine.value,
        'transmition' : transmition.value,
        'city' : city.value,
        'price' : price.value,
        'other' : otherInfo.value,
        'imgs'  : imgs
        };
}

 function connectedImgs() {
    var oldImages = [];
    for(let i = 0; i < imgs.length; i++){
        oldImages.push(imgs[i].getAttribute("src"));
    }


    return [oldImages, newImgs];
 }