var user = document.getElementById('user');
var uls = document.getElementsByClassName('search_result');
uls[0].addEventListener('click', writeAnswer);

user.addEventListener('change',sendMessage);
user.addEventListener('keyup',sendMessage);
user.addEventListener('input',sendMessage);
user.addEventListener('click',sendMessage);

var xhr = new XMLHttpRequest();

function sendMessage(event) {
    event.preventDefault();

    if(event.target.id == 'user'){
        var ul = uls[0];
        var message = 'user=' + event.target.value;
    }

    var input = event.target.value;

    xhr.open("POST", 'http://carbuy/admin/searchUser', true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function () {//Вызывает функцию при смене состояния.
        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
            var answer = JSON.parse(xhr.responseText);
            ul.innerHTML = answer.Content;
            ul.style.display = 'block';
        }
    };

    if(user.value.length > 1){
        xhr.send('ajax=params&' + message);
    }

}
function writeAnswer(event){
        if(event.target.tagName = "LI"){
            if(event.target.parentNode == uls[0]){
                user.value = event.target.innerHTML;
                uls[0].style.display = 'none';
            }
        }
    }

// натиск на кнопку знайти користувача і виведення контенту
var searchButton = document.getElementById("searchUser");
searchButton.addEventListener("click",openUserInfo);

var info = document.getElementById("userInfo");

function openUserInfo(event) {
    var message = 'user=' + user.value;
    xhr.open("POST", 'http://carbuy/admin/userInfo', true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function () {//Вызывает функцию при смене состояния.
        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {

            var answer = JSON.parse(xhr.responseText);
            if(answer.Content){
                info.innerHTML = answer.Content;
            }
            else{
                screenMessage("Користувача не знайдено!", info);
            }
        }
    };

        xhr.send('ajax=params&' + message);
}

function screenMessage(message, info) {
    var h4 = document.createElement("h4");
    h4.innerHTML = message;
    info.innerHTML = "";

    info.appendChild(h4);
}

// обробка подій блокування, розблокування, видалення


info.addEventListener("click",function (event) {
    var login = document.getElementById("userLogin");
    login = login.innerHTML;
    login = login.replace("\n","");

    if(event.target.id == "userBlock"){
        editStatus(login,0,"Користувач заблокований!");
    }
    else if( event.target.id == "userUnblock"){
        editStatus(login,1,"Користувач розблокований!");
    }
    else  if(event.target.id == "userDelete"){
        deleteUser("Користувача видалено!",login);
    }
});

function editStatus(login,status,mess) {
    var message = 'status=' + status + "&" + "login=" + login;
    xhr.open("POST", 'http://carbuy/admin/changeStatus', true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function () {//Вызывает функцию при смене состояния.
        screenMessage(mess, info);
    };

    xhr.send('ajax=params&' + message);
}

function deleteUser(message,login) {
    var message = 'login=' + login;
    xhr.open("POST", 'http://carbuy/admin/deleteUser', true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function () {//Вызывает функцию при смене состояния.
        screenMessage(message, info);
    };

    xhr.send('ajax=params&' + message);
}