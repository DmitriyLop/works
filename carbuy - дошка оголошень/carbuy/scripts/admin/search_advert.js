var advert = document.getElementById('advert');
var uls = document.getElementsByClassName('search_result');
uls[0].addEventListener('click', writeAnswer);

advert.addEventListener('change',sendMessage);
advert.addEventListener('keyup',sendMessage);
advert.addEventListener('input',sendMessage);
advert.addEventListener('click',sendMessage);

var xhr = new XMLHttpRequest();

function sendMessage(event) {
    event.preventDefault();

    if(event.target.id == 'advert'){
        var ul = uls[0];
        var message = 'advert=' + event.target.value;
    }

    var input = event.target.value;

    xhr.open("POST", 'http://carbuy/admin/searchAdvert', true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function () {//Вызывает функцию при смене состояния.
        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
            var answer = JSON.parse(xhr.responseText);
            ul.innerHTML = answer.Content;
            ul.style.display = 'block';
        }
    };

    if(advert.value.length > 0){
        xhr.send('ajax=params&' + message);
    }

}
function writeAnswer(event){
    if(event.target.tagName = "LI"){
        if(event.target.parentNode == uls[0]){
            advert.value = event.target.innerHTML;
            uls[0].style.display = 'none';
        }
    }
}

// натиск на кнопку знайти користувача і виведення контенту
var searchButton = document.getElementById("searchAdvert");
searchButton.addEventListener("click",openAdvertInfo);

var info = document.getElementById("advertInfo");


function screenMessage(message, info) {
    var h4 = document.createElement("h4");
    h4.innerHTML = message;
    info.innerHTML = "";

    info.appendChild(h4);
}

function openAdvertInfo(event) {
    event.preventDefault();
    var message = 'advert=' + advert.value;
    var input = event.target.value;

    xhr.open("POST", 'http://carbuy/admin/advertInfo', true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function () {//Вызывает функцию при смене состояния.
        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
            var answer = JSON.parse(xhr.responseText);
            if(answer.Content){
                info.innerHTML = answer.Content;
            }
            else{
                screenMessage("Оголошення не знайдено!",info);
            }
        }
    };

    if(advert.value.length > 0){
        xhr.send('ajax=params&' + message);
    }

}


//обробка видалення
info.addEventListener("click",function (event) {
   if(event.target.id =="advertDelete"){
       var $id = document.getElementById("advertId");
       event.preventDefault();

       screenMessage("Оголошення видалено!",info);
       var message = 'advert=' + advert.value;

       xhr.open("POST", 'http://carbuy/admin/deleteAdvert', true);
       xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

       xhr.send('ajax=params&' + message);
   }

});
