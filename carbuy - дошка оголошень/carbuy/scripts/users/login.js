var button = document.getElementById("checkLogin");
var login = document.getElementsByName('login');
var password = document.getElementsByName('password');
login = login[0];
password = password[0];


button.addEventListener("click", SendMessage);
function SendMessage(event) {
    event.preventDefault();
    var xhr = new XMLHttpRequest();
    xhr.open("POST", 'http://carbuy/users/CheckLogin', true);

    //Передает правильный заголовок в запросе
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function () {//Вызывает функцию при смене состояния.
        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
            var answer = JSON.parse(xhr.responseText);
            console.log(answer);
            var message = document.getElementsByClassName('message');
            if(answer.login == false){
                message[0].innerHTML = "Пароль, або логін введено не правильно";
            }
            else{
                message[0].innerHTML = "";
                button.removeEventListener('click',SendMessage, false);
                window.location = answer.location_page;
            }
        }
    };

    xhr.send(`ajax=params&login=${login.value}&password=${password.value}`);

}