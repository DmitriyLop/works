var registBlock = document.getElementById('registration');
var button = document.getElementById('button');
var login = registBlock.querySelector('[type="text"]');
var firstPass = registBlock.querySelector('[type="password"]');
var secondPass = registBlock.querySelector('[name="secondpass"]');
var email = registBlock.querySelector('[type="email"]');
var phone = registBlock.querySelector('[type="tel"]');
var message = registBlock.getElementsByClassName("message");

function CheckForm(login, firstPass, secondPass, email, phone) {
    login = login.replace(' ','');
    firstPass = firstPass.replace(' ','');
    secondPass = secondPass.replace(' ','');
    email = email.replace(' ','');
    phone = phone.replace(' ','');

    if(login != "" && firstPass != "" && secondPass != "" && email != "" && phone !="" ){
        if(!checkLogin(login)){
            message[0].innerHTML = "Логін не відповідає шаблону. Будь ласка, змініть логін!";
            return false;
        }
        if(!checkPassword(firstPass)){
            message[0].innerHTML = "Пароль не повинен містити пробілів та бути менше 6 символів. Символи для паролю повинні бути лише з англійської абетки або цифри!";
            return false;
        }
        if(!checkPhone(phone)){
            message[0].innerHTML = "Введена комбінація в полі мобільного номера не є номером. Будь ласка, змініть номер!Приклад введеноного номера: +380688457994";
            return false;
        }
        if(!checkEmail(email)){
            message[0].innerHTML = "Введений e-mail не відповідає шаблону. Будь ласка, змініть email!";
            return false;
        }

        if(firstPass == secondPass){
            return true;
        }
        else {
            message[0].innerHTML = "Поля з паролями не є однаковими!";
        }
    } else {
        message[0].innerHTML = "Обов'язкове заповнення всіх полів!";
    }

    return false;
}




function checkLogin(login){
    pattern = /^[a-zA-Z](.[a-zA-Z0-9_-]*)$/;
    return pattern.test(login);
}

function checkPassword(password, repeatPassword) {
    pattern = /^([a-zA-Z0123456789]){6,}$/;
    return pattern.test(password);
}

function checkPhone(phone) {
    pattern = /^\+380\d{3}\d{2}\d{2}\d{2}$/;
    return pattern.test(phone);
}

function checkEmail(email) {
    pattern = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
    return pattern.test(email);
}














button.addEventListener('click', SendMessageOnServer);
function SendMessageOnServer (event) {

    event.preventDefault();
    //перевіряємо поля вводу, якщо все good, то посилаємо запит на перевірку існування логіну
    if (CheckForm(login.value, firstPass.value, secondPass.value, email.value, phone.value)) {

        var xhr = new XMLHttpRequest();
        xhr.open("POST", 'http://carbuy/users/CheckLogin', true);
        //Передает правильный заголовок в запросе
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        xhr.onreadystatechange = function () {//Вызывает функцию при смене состояния.
            if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                var answer = JSON.parse(xhr.responseText);
                console.log(answer);
                var message = document.getElementsByClassName('message');
                if (answer.login == true) {
                    message[0].innerHTML = "Логін вже існує";
                }
                else {
                    message[0].innerHTML = "";
                    button.removeEventListener('click', SendMessageOnServer, false);
                    button.click();
                }
            }
        };

    xhr.send(`ajax=params&login=${login.value}`);
    }
}

