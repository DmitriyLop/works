﻿var doorModel;
var doorPrice;
var treeType;
var sumPrice;
var treeCoeff;
var demontazh;
var montazh;
var Color;
var theColor;
var re;
var name;
var phone;
var mail;
var outDoorM;
var otherOptions = [];
var theTree;
var doorSq;


function Step1Time() {
    document.getElementsByClassName('selectTree')[0].style.display = 'none';
    document.getElementsByClassName('selecttype')[0].style.display = 'block';

    theTree = document.doorcalc.tree;
    for (var i = 0; i < theTree.length; i++) {

        if (theTree[i].checked) {
            treeType = theTree[i].value;
            if (treeType === 'Вільха') {
                treeCoeff = 1.2;
            } else if (treeType === 'Сосна') {
                treeCoeff = 1;
            } else if (treeType === 'Дуб') {
                treeCoeff = 2;
            } else if (treeType === 'Махонь') {
                treeCoeff = 6.5;
            }
        }
    }
}

function Step1() {
    setTimeout(Step1Time, 100);
}

function Step222() {
    document.getElementsByClassName('selecttype')[0].style.display = 'none';
    document.getElementsByClassName('selectmodel')[0].style.display = 'block';

}

function Step223() {
    setTimeout(Step22, 100);
}

function Step21() {
    document.getElementsByClassName('selecttype')[0].style.display = 'none';
    document.getElementsByClassName('selectinnermodel')[0].style.display = 'block';

}

function Step22() {
    document.getElementsByClassName('selecttype')[0].style.display = 'none';
    document.getElementsByClassName('selectoutsidemodel')[0].style.display = 'block';

}


function Step3() {
    var dHeightH = document.forms["doorcalc"].elements["height"].value,
        dWidthH = document.forms["doorcalc"].elements["width"].value,
        doorAmt = document.getElementById("kolvo").value;

    dWidthH = parseFloat(dWidthH);
    dHeightH = parseFloat(dHeightH);
    doorAmt = parseInt(doorAmt);

    if (doorAmt < 1) {
        alert('Amount should be higher than 0');
        return;
    }

    if (!dWidthH || !dHeightH || !doorAmt) {
        alert('Please, enter sizes and amount');
        return;
    }


    document.getElementsByClassName('sizes')[0].style.display = 'none';
    document.getElementsByClassName('options')[0].style.display = 'block';
}

function Step4() {
    document.getElementsByClassName('options')[0].style.display = 'none';
    document.getElementsByClassName('options2')[0].style.display = 'block';

}

function Step5() {
    document.getElementsByClassName('options2')[0].style.display = 'none';
    document.getElementsByClassName('options3')[0].style.display = 'block';

}

function backStep1() {
    document.getElementsByClassName('selectTree')[0].style.display = 'block';
    document.getElementsByClassName('selecttype')[0].style.display = 'none';

}

function backStep11() {
    document.getElementsByClassName('selecttype')[0].style.display = 'block';
    document.getElementsByClassName('selectinnermodel')[0].style.display = 'none';

}

function backStep12() {
    document.getElementsByClassName('selecttype')[0].style.display = 'block';
    document.getElementsByClassName('selectoutsidemodel')[0].style.display = 'none';

}

function backStep2() {
    document.getElementsByClassName('selecttype')[0].style.display = 'block';
    document.getElementsByClassName('sizes')[0].style.display = 'none';
}

function backStep3() {
    document.getElementsByClassName('sizes')[0].style.display = 'block';
    document.getElementsByClassName('options')[0].style.display = 'none';
}

function backStep4() {
    document.getElementsByClassName('options')[0].style.display = 'block';
    document.getElementsByClassName('options2')[0].style.display = 'none';

}

function backStep5() {
    document.getElementsByClassName('selectTree')[0].style.display = 'block';
    document.getElementsByClassName('options3')[0].style.display = 'none';

}

function closeCalc() {
    document.getElementById('windowcalc').style.display = 'none';
    document.getElementById('SignupForm').reset();
    document.getElementsByClassName('selectTree')[0].style.display = 'block';
    document.getElementsByClassName('selecttype')[0].style.display = 'none';
    document.getElementsByClassName('sizes')[0].style.display = 'none';
    document.getElementsByClassName('options')[0].style.display = 'none';
    document.getElementsByClassName('options2')[0].style.display = 'none';
    document.getElementsByClassName('options3')[0].style.display = 'none';
    document.getElementsByClassName('selectinnermodel')[0].style.display = 'none';
}


function dmodel() {
    setTimeout(dmodelTime, 100);
}

function dmodelTime() {

    theGroup = document.doorcalc.doormodel;
    for (var i = 0; i < theGroup.length; i++) {

        if (theGroup[i].checked) {
            doorModel = theGroup[i].value;
            doorType = 'Двері міжкімнатні';

            if (doorModel === 'aberden' || doorModel === 'abeba' || doorModel === 'avignon' ||
                doorModel === 'bali' || doorModel === 'bangkok' || doorModel === 'barselona' ||
                doorModel === 'boston' || doorModel === 'brussel' || doorModel === 'canberra' ||
                doorModel === 'caspian' || doorModel === 'capri' || doorModel === 'cambridge' ||
                doorModel === 'detroit' || doorModel === 'den_haag' || doorModel === 'delta' ||
                doorModel === 'calcutta' || doorModel === 'calais' || doorModel === 'canton' ||
                doorModel === 'cannes' || doorModel === 'nastralia' || doorModel === 'classic' ||
                doorModel === 'noterdam' || doorModel === 'lyric' || doorModel === 'london' || doorModel === 'liverpool' ||
                doorModel === 'lagos' || doorModel === 'lausanne' || doorModel === 'leipzig') {
                doorPrice = 2873

            } else if (doorModel === 'austin' || doorModel === 'aurora' || doorModel === 'bermuda' ||
                doorModel === 'attika' || doorModel === 'delhi' || doorModel === 'djakarta' || doorModel === 'dallas' ||
                doorModel === 'dominica' || doorModel === 'dunbar' || doorModel === 'dublin' || doorModel === 'dover' ||
                doorModel === 'lublin' || doorModel === 'lisbonn' || doorModel === 'lyons' || doorModel === 'lviv' ||
                doorModel === 'barbados' || doorModel === 'balaton' || doorModel === 'beaumont' || doorModel === 'breda' ||
                doorModel === 'bremen' || doorModel === 'bristol' || doorModel === 'agave' || doorModel === 'accra' ||
                doorModel === 'adonis' || doorModel === 'antwerp' || doorModel === 'alaska' || doorModel === 'akropolis' ||
                doorModel === 'arctic' || doorModel === 'aspen' || doorModel === 'halle' || doorModel === 'harvard' ||
                doorModel === 'hondo' || doorModel === 'jamajka' || doorModel === 'jaffa' || doorModel === 'java' || doorModel === 'jura') {
                doorPrice = 3289

            } else if (doorModel === 'elan' || doorModel === 'apollo' || doorModel === 'asuncion' ||
                doorModel === 'advance' || doorModel === 'alp' || doorModel === 'athen' || doorModel === 'rio' || doorModel === 'rige' ||
                doorModel === 'rey' || doorModel === 'rome' || doorModel === 'el_paso' || doorModel === 'eldorado' || doorModel === 'everet' ||
                doorModel === 'etna' || doorModel === 'ethnic' || doorModel === 'olympus' || doorModel === 'ontario' || doorModel === 'florence' ||
                doorModel === 'figaro' || doorModel === 'elba' || doorModel === 'elegance' || doorModel === 'effect' || doorModel === 'ottawa' || doorModel === 'oxford') {
                doorPrice = 3583

            } else if (doorModel === 'georgia' || doorModel === 'geneva' || doorModel === 'glasgow' || doorModel === 'orlando' || doorModel === 'oslo') {
                doorPrice = 3721

            } else if (doorModel === 'edem' || doorModel === 'edmonton' || doorModel === 'elizabeth' || doorModel === 'empire' ||
                doorModel === 'elite' || doorModel === 'espirit' || doorModel === 'fargo' || doorModel === 'fes' ||
                doorModel === 'exclusiv' || doorModel === 'ferrara' || doorModel === 'excelent') {
                doorPrice = 3427
            }
        }

    }
    doorModel = doorModel.toUpperCase();
    document.getElementsByClassName('centerimage')[0].innerHTML = '<img src="/images/doors/inside/' + doorModel + '.png" alt="" class="transdoor"><span>' + doorModel + '</span>';

    document.getElementsByClassName('selectinnermodel')[0].style.display = 'none';
    document.getElementsByClassName('sizes')[0].style.display = 'block';
}

function dModelOut() {
    setTimeout(dModelOutTime, 100);
}

function dModelOutTime() {

    outDoorM = document.doorcalc.dooroutmodel;
    for (var i = 0; i < outDoorM.length; i++) {

        if (outDoorM[i].checked) {
            doorModel = outDoorM[i].value;
            doorType = 'Двері зовнішні';

            if (doorModel === 'aberdeen' || doorModel === 'abeba' || doorModel === 'avignon' ||
                doorModel === 'bali' || doorModel === 'bangkok' || doorModel === 'detroit' ||
                doorModel === 'den_haag' || doorModel === 'delta' || doorModel === 'barcelona' ||
                doorModel === 'boston' || doorModel === 'brussel' || doorModel === 'cambridge' ||
                doorModel === 'canberra' || doorModel === 'caspian' || doorModel === 'calcutta' ||
                doorModel === 'calais' || doorModel === 'canton' ||
                doorModel === 'cannes' || doorModel === 'nastralia' || doorModel === 'classic' ||
                doorModel === 'rotterdam' || doorModel === 'london' || doorModel === 'liverpool' ||
                doorModel === 'lagos' || doorModel === 'lausanne' || doorModel === 'leipzig') {
                doorPrice = 5696
            } else if (doorModel === 'denver' || doorModel === 'lyric' || doorModel === 'bermuda' ||
                doorModel === 'delhi' || doorModel === 'djakarta' || doorModel === 'dallas' ||
                doorModel === 'dominica' || doorModel === 'dunbar' || doorModel === 'dublin' || doorModel === 'dover' ||
                doorModel === 'lublin' || doorModel === 'lisbonn' || doorModel === 'lyons' || doorModel === 'lviv' ||
                doorModel === 'barbados' || doorModel === 'balaton' || doorModel === 'beaumont' || doorModel === 'istanbul' ||
                doorModel === 'iowa' || doorModel === 'agave' || doorModel === 'accra' ||
                doorModel === 'adonis' || doorModel === 'antwerp' || doorModel === 'alaska' || doorModel === 'acropolis' ||
                doorModel === 'arctic' || doorModel === 'aspen' || doorModel === 'halle' || doorModel === 'harvard' ||
                doorModel === 'hondo' || doorModel === 'jamajka' || doorModel === 'jaffa' || doorModel === 'java' || doorModel === 'jura') {
                doorPrice = 6554

            } else if (doorModel === 'bonn' || doorModel === 'havana' || doorModel === 'berlin' ||
                doorModel === 'bern' || doorModel === 'ceylon' || doorModel === 'davos' || doorModel === 'breda' ||
                doorModel === 'bremen' || doorModel === 'bristol' || doorModel === 'apollo' || doorModel === 'asuncion' ||
                doorModel === 'advance' || doorModel === 'amsterdam' || doorModel === 'alp' || doorModel === 'athens' ||
                doorModel === 'elan' || doorModel === 'rio' || doorModel === 'riga' || doorModel === 'rome' ||
                doorModel === 'hinckley' || doorModel === 'harlem' || doorModel === 'jasper' || doorModel === 'jena' ||
                doorModel === 'jordan' || doorModel === 'indiana' || doorModel === 'indonesia' || doorModel === 'izmir' ||
                doorModel === 'morocco' || doorModel === 'mauritius' || doorModel === 'medan' || doorModel === 'marathon' ||
                doorModel === 'mexico' || doorModel === 'mont_blanc' || doorModel === 'napoli' || doorModel === 'nelson' ||
                doorModel === 'lille' || doorModel === 'micado' || doorModel === 'minor' || doorModel === 'ibiza' ||
                doorModel === 'isabella' || doorModel === 'major' || doorModel === 'muse' || doorModel === 'melody' ||
                doorModel === 'attraction' || doorModel === 'illusion') {

                doorPrice = 7181

            } else if (doorModel === 'georgia' || doorModel === 'geneva' || doorModel === 'glasgow' ||
                doorModel === 'potsdam' || doorModel === 'prague' || doorModel === 'paris' ||
                doorModel === 'parnas' || doorModel === 'pontiac' || doorModel === 'singapore' || doorModel === 'seville' ||
                doorModel === 'seoul' || doorModel === 'stockholm' || doorModel === 'samos' || doorModel === 'santiago' ||
                doorModel === 'saigon' || doorModel === 'sydney' || doorModel === 'simplon') {

                doorPrice = 7405

            } else if (doorModel === 'edem' || doorModel === 'edmonton' || doorModel === 'elizabeth' || doorModel === 'empire' ||
                doorModel === 'elite' || doorModel === 'espirit' || doorModel === 'eldorado' || doorModel === 'el_paso' ||
                doorModel === 'edward' || doorModel === 'eton' || doorModel === 'everest' || doorModel === 'etna' ||
                doorModel === 'ethnic' || doorModel === 'fargo' || doorModel === 'fes' || doorModel === 'exclusive' ||
                doorModel === 'ferrara' || doorModel === 'excelent' || doorModel === 'florence' || doorModel === 'fiji' ||
                doorModel === 'elton' || doorModel === 'figaro' || doorModel === 'elba' || doorModel === 'elegance' ||
                doorModel === 'effect' || doorModel === 'olympus' || doorModel === 'ontario' || doorModel === 'orlando' ||
                doorModel === 'ottawa' || doorModel === 'oxford' || doorModel === 'oslo') {
                doorPrice = 6640
            }
        }

    }
    doorModel = doorModel.toUpperCase();
    document.getElementsByClassName('centerimage')[0].innerHTML = '<img src="/images/doors/outdoor/' + doorModel + '.png" alt="" class="transdoor"><span>' + doorModel + '</span>';

    document.getElementsByClassName('selectoutsidemodel')[0].style.display = 'none';
    document.getElementsByClassName('sizes')[0].style.display = 'block';
}


/**
 * Probable not needed. It is not used at all.
 */
//function tType() {
//
//    theTree = document.doorcalc.tree;
//    for (var i = 0; i < theTree.length; i++) {
//
//        if (theTree[i].checked) {
//            treeType = theTree[i].value;
//            if (treeType === 'Вільха') {
//                treeCoeff = 1.2;
//            } else if (treeType === 'Сосна') {
//                treeCoeff = 1;
//            } else if (treeType === 'Дуб') {
//                treeCoeff = 2;
//            } else if (treeType === 'Махонь') {
//                treeCoeff = 6.5;
//            }
//        }
//    }
//}


/**
 * Та сама формула тут. Другої в цьому файлі не знайшов.
 * У файлі windowcalculator.js є інша формула:
 *  sumPrice = winPrice * winAmt * treeCoeff;
 *  sumPrice = sumPrice + demontazh + montazh;
 *  Це одна формула, у другому рядку до порахованної суми додається ціна за опції.
 * Прошу зазначити, що ця формула не враховую розміри вікна чи дверей.
 */
function treeCalc() {

    dHeightH = document.forms["doorcalc"].elements["height"].value;
    dWidthH = document.forms["doorcalc"].elements["width"].value;
    doorAmt = document.getElementById("kolvo").value;

    dWidthH = parseFloat(dWidthH);
    dHeightH = parseFloat(dHeightH);

    dWidth = dWidthH / 1000;
    dHeight = dHeightH / 1000;

    // Square
    doorSq = dHeight * dWidth;

    // Total price formula.
    //sumPrice = doorSq * doorPrice * treeCoeff * doorAmt;
    sumPrice = doorSq * doorPrice * treeCoeff * doorAmt;

    checkbox = document.getElementsByName("dooroptions");
    otherOptions = [];
    for (var i = 0; i < checkbox.length; i++) { // пробегаем весь массив
        if (checkbox[i].checked) {
            otherOptions.push(checkbox[i].value);
        }

        if (checkbox[3].checked) {
            demontazh = 0; //можна дописати sumPrice*0.05 якщо треба рахувати демонтаж
        } else {
            demontazh = 0
        }
        if (checkbox[4].checked) {
            montazh = 0;  //можна дописати sumPrice*0.1 якщо треба рахувати монтаж
        } else {
            montazh = 0
        }
    }

    // Total price with options
    sumPrice = sumPrice + demontazh + montazh;
    sumPrice = sumPrice.toFixed(2);


    document.getElementById("result").value = sumPrice + ' грн';
}


function selColor() {
    setTimeout(selColorTime, 100);
}

function selColorTime() {

    theColor = document.getElementsByName("selcolor");
    for (var i = 0; i < theColor.length; i++) {

        if (theColor[i].checked) {
            Color = theColor[i].value;
        }
    }

    document.getElementsByClassName('selectcolor')[0].style.background = "#" + Color;
    document.getElementsByClassName('transdoor')[0].style.background = "#" + Color;
    document.getElementById('colors').style.overflow = 'hidden';
    document.getElementById('colors').style.height = "20px";
    document.getElementById('colors').scrollTop = 0;

    //alert(Color);
}

/**
 * Ajax request cannot get file, especially POST request. Tricks can be done using Block,
 * but it somehow distorts data, so pdf fonts could not be embedded.
 */
//function AjaxFormRequest(result_id) {
//    name = $('input[name*="name"]').val();
//    phone = $('input[name*="phone"]').val();
//    mail = $('input[name*="mail"]').val();
//    if (name == "" || phone == "" || mail == "") {
//        alert('Заповніть форму')
//    } else {
//        $.ajax({
//            type: "POST",
//            url: "doorsend.php",
//            data: {
//                name: name, phone: phone, mail: mail, treeType: treeType, sumPrice: sumPrice, dWidthH: dWidthH,
//                dHeightH: dHeightH, doorModel: doorModel, doorAmt: doorAmt, doorType: doorType,
//            }
//        }).done(function (msg) {
//            $('#' + result_id).html(msg);
//        });
//        //emailAdress = document.getElementById('mailad').value;
//        // re = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,6}$/i;
//        // alert(re.mail ? 'Правильный адрес' : 'Неправильна адреса пошти');
//
//        document.getElementsByClassName('options2')[0].style.display = 'none';
//        document.getElementsByClassName('options3')[0].style.display = 'block';
//        document.getElementById('SignupForm').reset();
//    }
//}

/**
 * Isolates scope.
 */
(function () {

    function init() {
        initEvents();
    }

    function initEvents() {
        /**
         * On DOM created.
         */
        $(function () {
            var heightInput = $('input[name="height"]'),
                widthInput = $('input[name="width"]'),
                quantityInput = $('input[name="kolvo"]');

            heightInput.on('focusout', function (e) {
                var value = parseInt(heightInput.val());

                if (value < 1200) {
                    heightInput.val(1200);
                }
                else if (value > 3000) {
                    heightInput.val(3000);
                }
            });

            widthInput.on('focusout', function (e) {
                var value = parseInt(widthInput.val());

                if (value < 500) {
                    widthInput.val(500);
                }
                else if (value > 2000) {
                    widthInput.val(2000);
                }
            });

            quantityInput.on('focusout', function (e) {
                var value = parseInt(quantityInput.val());

                if (value < 0) {
                    quantityInput.val(0);
                }
            });

            heightInput.keypress(function (e) {
                var chr = String.fromCharCode(e.which);

                if ("0123456789".indexOf(chr) < 0) {
                    return false;
                }
            });

            widthInput.keypress(function (e) {
                var chr = String.fromCharCode(e.which);

                if ("0123456789".indexOf(chr) < 0) {
                    return false;
                }
            });

            quantityInput.keypress(function (e) {
                var chr = String.fromCharCode(e.which);

                if ("0123456789".indexOf(chr) < 0) {
                    return false;
                }
            });


            // Doors invoice save to pdf
            $('#createPdf').on('click', onCreatePdfButtonClicked);

            /**
             * Create pdf button click handler
             * @param {Event} e
             */
            function onCreatePdfButtonClicked(e) {
                var name = $('input[name*="name"]').val(),
                    phone = $('input[name*="phone"]').val(),
                    mail = $('input[name*="mail"]').val(),
                    price = doorPrice * treeCoeff * doorSq;

                price = price.toFixed(2);

                if (name == "" || phone == "" || mail == "") {
                    alert('Заповніть форму')
                } else {
                    navigateToUrl('/api/doorsSave.php?name=' + name + '&phone=' + phone + '&mail=' + mail +
                        '&treeType=' + treeType + '&sumPrice=' + sumPrice + '&dWidthH=' + dWidthH +
                        '&dHeightH=' + dHeightH + '&doorModel=' + doorModel + '&doorAmt=' + doorAmt +
                        '&doorType=' + doorType + '&price=' + price + '&otherOptions=' + otherOptions);

                    document.getElementsByClassName('options2')[0].style.display = 'none';
                    document.getElementsByClassName('options3')[0].style.display = 'block';
                    document.getElementById('SignupForm').reset();
                }
            }

            /**
             * Imitates link activity.
             * @param {String} url
             */
            function navigateToUrl(url) {
                var f = document.createElement("FORM");
                f.action = url;

                var indexQM = url.indexOf("?");
                if (indexQM >= 0) {
                    // the URL has parameters => convert them to hidden form inputs
                    var params = url.substring(indexQM + 1).split("&");
                    for (var i = 0; i < params.length; i++) {
                        var keyValuePair = params[i].split("=");
                        var input = document.createElement("INPUT");
                        input.type = "hidden";
                        input.name = keyValuePair[0];
                        input.value = keyValuePair[1];
                        f.appendChild(input);
                    }
                }

                document.body.appendChild(f);
                f.submit();

                // remove form from dom
                $(f).remove();
            }

        });

    }

    init();

})();