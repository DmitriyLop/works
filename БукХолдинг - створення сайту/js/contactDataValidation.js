/**
 * Created by mitfity on 30.08.2015.
 */

(function () {

    function init() {
        initEvents();
    }

    function initEvents() {
        $(function () {
            var phoneInput = $('input[name="phone"]');

            phoneInput.keypress( function (e) {
                var chr = String.fromCharCode(e.which);
                if ("0123456789-+() ".indexOf(chr) < 0) {
                    return false;
                }
            });
        });

    }

    init();

})();