/*
written by alexander shurkayev <alshur@ya.ru> | http://htmlcssjs.ru
*/

// далее у нас следует массив из улиц, содержащих дома, разделённые запятой
var aHouseValues = new Array(
"12/15,18,123",
"2,4",
"2/8,10/12",
"3",
"2,4,12,5/6,8"
);

// ф-ция, возвращающая массив домов по заданной улице
function getHouseValuesByStreet(index){
    var sHouseValues = aHouseValues[index];
    return sHouseValues.split(","); // преобразуем строку в массив домов
}

// главная ф-ция, выводящая динамически список домов
function MkHouseValues(index){
    var aCurrHouseValues = getHouseValuesByStreet(index);
    var nCurrHouseValuesCnt = aCurrHouseValues.length;
    var oHouseList = document.forms["address"].elements["house"];
    var oHouseListOptionsCnt = oHouseList.options.length;
    oHouseList.length = 0; // удаляем все элементы из списка домов
    for (i = 0; i < nCurrHouseValuesCnt; i++){
        // далее мы добавляем необходимые дома в список
        if (document.createElement){
            var newHouseListOption = document.createElement("OPTION");
            newHouseListOption.text = aCurrHouseValues[i];
            newHouseListOption.value = aCurrHouseValues[i];
            // тут мы используем для добавления элемента либо метод IE, либо DOM, которые, alas, не совпадают по параметрам…
            (oHouseList.options.add) ? oHouseList.options.add(newHouseListOption) : oHouseList.add(newHouseListOption, null);
        }else{
            // для NN3.x-4.x
            oHouseList.options[i] = new Option(aCurrHouseValues[i], aCurrHouseValues[i], false, false);
        }
    }
}

// инициируем изменение элементов в списке домов, в зависимости от текущей улицы
MkHouseValues(document.forms["address"].elements["street"].selectedIndex);
//-->