(function () {

    var languagesDropdown = $('.mainpage .mainlang'),
        fixedMenuLanguagesDropdown = $('#menu .mainlang'),
        actions = {
            1: function () {
                languagesDropdown.css('height', 'auto');
                languagesDropdown.css('outline', '1px solid #aaa');
                languagesDropdown.css('boxShadow', '0px 0px 8px #777');
                fixedMenuLanguagesDropdown.css('height', 'auto');
                fixedMenuLanguagesDropdown.css('outline', '1px solid #aaa');
                fixedMenuLanguagesDropdown.css('boxShadow', '0px 0px 8px #777');
            },
            2: function () {
                languagesDropdown.css('height', '24px');
                languagesDropdown.css('outline', 'none');
                languagesDropdown.css('boxShadow', 'none');
                fixedMenuLanguagesDropdown.css('height', '24px');
                fixedMenuLanguagesDropdown.css('outline', 'none');
                fixedMenuLanguagesDropdown.css('boxShadow', 'none');
            }
        },
        counter = 0,
        currentLanguage,
        isMobile = false,
        menu = $('#menu');

    function init() {
        // device detection
        if($(window).width() <= 640) {
            isMobile = true;
        }

        $(function() {
            $('.slicknav_menu').scrollToFixed();
        });

        if (!isMobile) {
            currentLanguage = $('.mainlang .currentlang');
            // Filter languages when script loads
            //filterLanguagesAlphabetically(languagesDropdown);
            if (menu.css('display') !== 'none') {
                currentLanguage = $('#menu .language .currentlang');
                //filterLanguagesAlphabetically(fixedMenuLanguagesDropdown);
            }
        }
        else {
            var languages = $('.language', menu);

            currentLanguage = $('#menu .language .currentlang');
            languages.css({
                display: 'none'
            });

            $('p', languages).each(function (index, el) {
                var $el = $(el),
                    setCurrentLang = $el.hasClass('currentlang') ? ' class="currentlang lang"' :
                        ' class="lang"';

                menu.append('<li' + setCurrentLang + '>' + $el.text() + '</li>');
            });
        }

        if (!isMobile) {
            selectLanguage(currentLanguage);
        }

        initEvents();
    }

    function initEvents() {
        if (!isMobile) {
            languagesDropdown.on('click', onLanguagesDropdownClick);
            languagesDropdown.on('click', 'p:not(.currentlang)', onOptionalLanguageClick);
            fixedMenuLanguagesDropdown.on('click', onLanguagesDropdownClick);
            fixedMenuLanguagesDropdown.on('click', 'p:not(.currentlang)', onOptionalLanguageClick);
            return;
        }

        $(function () {
            var slickMenu = $('.slicknav_menu');

            currentLanguage = $('.slicknav_menu li.currentlang');
            fixedMenuLanguagesDropdown = slickMenu;
            //filterLanguagesAlphabetically(null, $('.lang:not(.currentlang)', fixedMenuLanguagesDropdown));
            slickMenu.on('click', 'li.lang:not(.currentlang)', onOptionalLanguageClick);
        });
    }

    /**
     * Returns true if browser is ie mobile.
     * @returns {boolean}
     */
    function isIEMobile() {
        var regExp = new RegExp("IEMobile", "i");

        return !!navigator.userAgent.match(regExp);
    }

    /**
     * It is called when languages dropdown list is clicked.
     * @param e
     */
    function onLanguagesDropdownClick(e) {
        currentLanguage.toggleClass('expanded');
        actions[counter = (counter % 2) + 1]();
    }

    /**
     * Selects language in drop-down menu
     * @param selectedLanguage
     */
    function selectLanguage(selectedLanguage) {
        var topLanguage = $('.mainlang p:first-child');

        if (topLanguage !== currentLanguage && !isMobile) {
            topLanguage.before(currentLanguage);
        }

        currentLanguage.removeClass('currentlang expanded');
        selectedLanguage.addClass('currentlang expanded');

        if (!isMobile) {
            currentLanguage.before(selectedLanguage);
        }
        currentLanguage = selectedLanguage;


        if (!isMobile && menu.css('display') !== 'none') {
            //filterLanguagesAlphabetically(fixedMenuLanguagesDropdown);
        }
        //filterLanguagesAlphabetically(languagesDropdown);

        changePageLanguage(currentLanguage.text());
    }

    /**
     * Changes language of page. Changes location to /lang/sample.html
     * @param {String} language
     */
    function changePageLanguage(language) {
        var location = $(window.location),
            pathName = location.attr('pathname'),
            pathParts = pathName.split("/"),
            firstPathPart,
            otherPathParts,
            targetLanguageFirstPathName,
            targetLocation,
            targetLanguageFirstPathPart;

        pathName.toLowerCase();
        firstPathPart = pathParts[1];

        otherPathParts = pathParts.splice(2, Number.MAX_VALUE);

        language = language.toLowerCase();
        if (language === 'русский' || language === 'російська') {
            targetLanguageFirstPathName = 'ru';
        }
        else if (language === 'english') {
            targetLanguageFirstPathName = 'en';
        }
        else if (language === 'deutsch') {
            targetLanguageFirstPathName = 'de';
        }
        else {
            targetLanguageFirstPathName = '';
        }

        targetLanguageFirstPathPart = targetLanguageFirstPathName ? '/' + targetLanguageFirstPathName : '';
        if (otherPathParts.length === 0) {
            targetLocation = targetLanguageFirstPathPart +
                location.attr('pathname');
        }
        else {
            targetLocation = targetLanguageFirstPathPart;
            otherPathParts.forEach(function (part) {
                targetLocation += '/' + part;
            });
        }

        if (location.attr('pathname') !== targetLocation) {
            window.location = targetLocation;
        }
    }

    /**
     * It is called when language is selected.
     * @param e
     */
    function onOptionalLanguageClick(e) {
        var selectedLanguage = $(this);
        selectLanguage(selectedLanguage);
    }

    /**
     * Filters languages list alphabetically.
     */
    function filterLanguagesAlphabetically(languagesDropdown, options) {
        var options = options || $('p:not(.currentlang)', languagesDropdown),
            arr = options.map(function (_, o) {
                return {t: $(o).text(), v: o.value};
            }).get();

        arr.sort(function (o1, o2) {
            var t1 = o1.t.toLowerCase(), t2 = o2.t.toLowerCase();

            return t1 > t2 ? 1 : t1 < t2 ? -1 : 0;
        });

        options.each(function (i, o) {
            o.value = arr[i].v;
            $(o).text(arr[i].t);
        });
    }

    init();

})();


// Probably not needed
//var pagelang = document.getElementsByClassName('lang')[0];
//
//var pageaction = {
//    1: function() {
//       pagelang.style.height = "auto";
//       pagelang.style.outline = "1px solid #aaa";
//       pagelang.style.boxShadow = "0px 0px 8px #777";
//    },
//    2: function() {
//       pagelang.style.height = "24px"
//       pagelang.style.outline = "none";
//       pagelang.style.boxShadow = "none";
//    }
//};
//
//var counter2 = 0;
//
//pagelang.onclick = function() {
//    pageaction[counter2 = (counter2 % 2) + 1]();
//};