var winModel;
var widthFromForm;
var winWidth;
var winPrice;
var theGroup;
var winSelOptions;
var optionPrice;
var sumPrice;
var winAmt;
var winNumber;
var aHouseValues;
var treeCoeff;
var treeType;
var optionDemontazh;
var optionMontazh;
var optionPidvikon;


function winCalc2() {

    theGroup = document.wincalc.model;
    for (var i = 0; i < theGroup.length; i++) {
        if (theGroup[i].checked) {
            if (theGroup[i].value === 'Глухе') {
                winModel = 'Глухе';
                document.getElementById("height").innerHTML = "<option>0</option><option>600</option><option>750</option><option>900</option><option>1050</option>";
                document.getElementById("width").innerHTML = "<option>0</option><option>600</option><option>1050</option><option>1200</option><option>1350</option>";
                document.getElementsByClassName('centerimage')[0].innerHTML = '<img src="/images/windows/gluhe.png" alt="">';
                aHouseValues = new Array(
                    "0",
                    "600",
                    "1050",
                    "1050,1200",
                    "1350"
                );
            } else if (theGroup[i].value === 'Поворотно-відкідне') {
                winModel = 'Поворотно-відкідне';
                document.getElementById("height").innerHTML = "<option>0</option><option>600</option><option>750</option><option>900</option><option>1050</option>";
                document.getElementById("width").innerHTML = "<option>0</option><option>900</option><option>1050</option><option>1200</option><option>1350</option>";
                document.getElementsByClassName('centerimage')[0].innerHTML = '<img src="/images/windows/povorotno-vidkidne.png" alt="">';
                aHouseValues = new Array(
                    "0",
                    "1200",
                    "900,1050",
                    "1350",
                    "1350"
                );
            } else if (theGroup[i].value === 'Глухе-поворотно-відкідне') {
                winModel = 'Глухе-поворотно-відкідне';
                document.getElementById("height").innerHTML = "<option>0</option><option>1050</option><option>1200</option><option>1350</option><option>1500</option>";
                document.getElementById("width").innerHTML = "<option>0</option><option>1500</option><option>1650</option><option>1800</option>";
                document.getElementsByClassName('centerimage')[0].innerHTML = '<img src="/images/windows/gluhe-povorotno-vidkidne.png" alt="">';
                aHouseValues = new Array(
                    "0",
                    "1500",
                    "1050",
                    "1650,1800",
                    "1800"
                );
            } else if (theGroup[i].value === 'Поворотне-поворотно-відкідне') {
                winModel = 'Поворотне-поворотно-відкідне';
                document.getElementById("height").innerHTML = "<option>0</option><option>1200</option><option>1350</option><option>1500</option><option>1650</option>";
                document.getElementById("width").innerHTML = "<option>0</option><option>1500</option><option>1650</option><option>1800</option>";
                document.getElementsByClassName('centerimage')[0].innerHTML = '<img src="/images/windows/povorotne-povorotno-vidkidne.png" alt="">';
                aHouseValues = new Array(
                    "0",
                    "1500",
                    "1500,1650",
                    "1800",
                    "1800"
                );
            } else if (theGroup[i].value === 'Поворотно-відкідне-глухе-поворотно-відкідне') {
                winModel = 'Поворотно-відкідне-глухе-поворотно-відкідне';
                document.getElementById("height").innerHTML = "<option>0</option><option>1650</option><option>1800</option><option>1950</option><option>2100</option>";
                document.getElementById("width").innerHTML = "<option>0</option><option>1800</option><option>2100</option>";
                document.getElementsByClassName('centerimage')[0].innerHTML = '<img src="/images/windows/povorotno-vidkidne-gluhe-povorotno-vidkidne.png" alt="">';
                aHouseValues = new Array(
                    "0",
                    "1800",
                    "1800,2100",
                    "2100",
                    "2100"
                );
            } else if (theGroup[i].value === 'Глухе-поворотно-відкідне-глухе') {
                winModel = 'Глухе-поворотно-відкідне-глухе';
                document.getElementById("height").innerHTML = "<option>0</option><option>1650</option><option>1800</option><option>1950</option><option>2100</option>";
                document.getElementById("width").innerHTML = "<option>0</option><option>1800</option><option>2100</option>";
                document.getElementsByClassName('centerimage')[0].innerHTML = '<img src="/images/windows/gluhe-povorotno-vidkidne-gluhe.png" alt="">';
                aHouseValues = new Array(
                    "0",
                    "1800",
                    "1800,2100",
                    "2100",
                    "2100"
                );
            }
        }
    }

}

// ф-ция, возвращающая массив домов по заданной улице
function getHouseValuesByStreet(index) {
    var sHouseValues = aHouseValues[index];
    return sHouseValues.split(","); // преобразуем строку в массив домов
}

// главная ф-ция, выводящая динамически список домов
function MkHouseValues(index) {
    var aCurrHouseValues = getHouseValuesByStreet(index);
    var nCurrHouseValuesCnt = aCurrHouseValues.length;
    var oHouseList = document.forms["wincalc"].elements["width"];
    var oHouseListOptionsCnt = oHouseList.options.length;
    oHouseList.length = 0; // удаляем все элементы из списка домов
    for (var i = 0; i < nCurrHouseValuesCnt; i++) {
        // далее мы добавляем необходимые дома в список
        if (document.createElement) {
            var newHouseListOption = document.createElement("OPTION");
            newHouseListOption.text = aCurrHouseValues[i];
            newHouseListOption.value = aCurrHouseValues[i];
            // тут мы используем для добавления элемента либо метод IE, либо DOM, которые, alas, не совпадают по параметрам…
            (oHouseList.options.add) ? oHouseList.options.add(newHouseListOption) : oHouseList.add(newHouseListOption, null);
        } else {
            // для NN3.x-4.x
            oHouseList.options[i] = new Option(aCurrHouseValues[i], aCurrHouseValues[i], false, false);
        }
    }
}

// $(function () {
//     // инициируем изменение элементов в списке домов, в зависимости от текущей улицы
//     MkHouseValues(document.forms["wincalc"].elements["height"].selectedIndex);
// });

function winCalc() {
    setTimeout(winCalc2, 200);
}

function priceCalc() {


    winAmt = document.getElementById("kolvo").value;

    widthFromForm = document.getElementById("height");
    winWidth = widthFromForm.options[widthFromForm.selectedIndex].value;

    heightFromForm = document.getElementById("width");
    winHeight = heightFromForm.options[heightFromForm.selectedIndex].value;

    if (winModel === 'Глухе' && winWidth === '600' && winHeight === '600') {
        winPrice = 1637;
    } else if (winModel === 'Глухе' && winWidth === '750' && winHeight === '1050') {
        winPrice = 2136;
    } else if (winModel === 'Глухе' && winWidth === '900' && winHeight === '1050') {
        winPrice = 2705;
    } else if (winModel === 'Глухе' && winWidth === '900' && winHeight === '1200') {
        winPrice = 2865;
    } else if (winModel === 'Глухе' && winWidth === '1050' && winHeight === '1350') {
        winPrice = 3013;
    } else if (winModel === 'Поворотно-відкідне' && winWidth === '750' && winHeight === '900') {
        winPrice = 4286;
    } else if (winModel === 'Поворотно-відкідне' && winWidth === '900' && winHeight === '1350') {
        winPrice = 5695;
    } else if (winModel === 'Поворотно-відкідне' && winWidth === '600' && winHeight === '1200') {
        winPrice = 5770;
    } else if (winModel === 'Поворотно-відкідне' && winWidth === '750' && winHeight === '1050') {
        winPrice = 4343;
    } else if (winModel === 'Поворотно-відкідне' && winWidth === '1050' && winHeight === '1350') {
        winPrice = 5770;
    } else if (winModel === 'Глухе-поворотно-відкідне' && winWidth === '1050' && winHeight === '1500') {
        winPrice = 7332;
    } else if (winModel === 'Глухе-поворотно-відкідне' && winWidth === '1200' && winHeight === '1500') {
        winPrice = 7552;
    } else if (winModel === 'Глухе-поворотно-відкідне' && winWidth === '1350' && winHeight === '1650') {
        winPrice = 8039;
    } else if (winModel === 'Глухе-поворотно-відкідне' && winWidth === '1350' && winHeight === '1800') {
        winPrice = 9138;
    } else if (winModel === 'Глухе-поворотно-відкідне' && winWidth === '1500' && winHeight === '1800') {
        winPrice = 8850;
    } else if (winModel === 'Поворотне-поворотно-відкідне' && winWidth === '1200' && winHeight === '1500') {
        winPrice = 8077;
    } else if (winModel === 'Поворотне-поворотно-відкідне' && winWidth === '1350' && winHeight === '1500') {
        winPrice = 9844;
    } else if (winModel === 'Поворотне-поворотно-відкідне' && winWidth === '1350' && winHeight === '1650') {
        winPrice = 8964;
    } else if (winModel === 'Поворотне-поворотно-відкідне' && winWidth === '1500' && winHeight === '1800') {
        winPrice = 9774;
    } else if (winModel === 'Поворотне-поворотно-відкідне' && winWidth === '1650' && winHeight === '1800') {
        winPrice = 11612;
    } else if (winModel === 'Глухе-поворотно-відкідне-глухе' && winWidth === '1650' && winHeight === '1800') {
        winPrice = 8461;
    } else if (winModel === 'Глухе-поворотно-відкідне-глухе' && winWidth === '1800' && winHeight === '1800') {
        winPrice = 9184;
    } else if (winModel === 'Глухе-поворотно-відкідне-глухе' && winWidth === '1800' && winHeight === '2100') {
        winPrice = 9461;
    } else if (winModel === 'Глухе-поворотно-відкідне-глухе' && winWidth === '1950' && winHeight === '2100') {
        winPrice = 10200;
    } else if (winModel === 'Глухе-поворотно-відкідне-глухе' && winWidth === '2100' && winHeight === '2100') {
        winPrice = 10558;
    } else if (winModel === 'Поворотно-відкідне-глухе-поворотно-відкідне' && winWidth === '1650' && winHeight === '1800') {
        winPrice = 11972;
    } else if (winModel === 'Поворотно-відкідне-глухе-поворотно-відкідне' && winWidth === '1800' && winHeight === '1800') {
        winPrice = 13951;
    } else if (winModel === 'Поворотно-відкідне-глухе-поворотно-відкідне' && winWidth === '1800' && winHeight === '2100') {
        winPrice = 14378;
    } else if (winModel === 'Поворотно-відкідне-глухе-поворотно-відкідне' && winWidth === '1950' && winHeight === '2100') {
        winPrice = 14455;
    } else if (winModel === 'Поворотно-відкідне-глухе-поворотно-відкідне' && winWidth === '2100' && winHeight === '2100') {
        winPrice = 16842;
    }
    ;

    checkbox = document.getElementsByName("winoptions"); // получаем массив чекбоксов
    var str = "";
    for (var i = 0; i < checkbox.length; i++) { // пробегаем весь массив
        str += checkbox[i].checked + " "; // записываем каждое значение в строку и разделяем пробелом

    }

    if (checkbox[0].checked && !checkbox[1].checked && !checkbox[2].checked ||
        !checkbox[0].checked && checkbox[1].checked && !checkbox[2].checked ||
        !checkbox[0].checked && !checkbox[1].checked && checkbox[2].checked) {
        optionPrice = 500;
    } else if (checkbox[0].checked && checkbox[1].checked && !checkbox[2].checked ||
        checkbox[0].checked && !checkbox[1].checked && checkbox[2].checked ||
        !checkbox[0].checked && checkbox[1].checked && checkbox[2].checked) {
        optionPrice = 1000;
    } else if (checkbox[0].checked && checkbox[1].checked && checkbox[2].checked) {
        optionPrice = 1500;
    } else if (!checkbox[0].checked && !checkbox[1].checked && !checkbox[2].checked) {
        optionPrice = 0;
    }

    checkbox = document.getElementsByName("winoptions");
    for (i = 0; i < checkbox.length; i++) { // пробегаем весь массив
        if (checkbox[1].checked) {
            demontazh = sumPrice * 0.05;
            optionDemontazh = "З демонтажем";
        } else {
            demontazh = 0;
            optionDemontazh = "Без демонтажу";
        }
        ;
        if (checkbox[2].checked) {
            montazh = sumPrice * 0.1;
            optionMontazh = "З монтажем";
        } else {
            montazh = 0;
            optionMontazh = "Без монтажу";
        }
        ;
        if (checkbox[0].checked) {
            optionPidvikon = "З підвіконням";
        } else {
            optionPidvikon = "Без підвіконня";
        }
        ;
    }


    sumPrice = winPrice * winAmt * treeCoeff;
    sumPrice = sumPrice + demontazh + montazh;
    sumPrice = sumPrice.toFixed(2);

    document.getElementById("result").value = sumPrice + ' грн';
};

function Step1() {
    setTimeout(Step1Time, 100);
}

function Step1Time() {
    document.getElementsByClassName('selectTree')[0].style.display = 'none';
    document.getElementsByClassName('selectmodel')[0].style.display = 'block';

    theTree = document.wincalc.tree;
    for (i = 0; i < theTree.length; i++) {

        if (theTree[i].checked) {
            treeType = theTree[i].value;
            if (treeType === 'Вільха') {
                treeCoeff = 1.2;
            } else if (treeType === 'Сосна') {
                treeCoeff = 1;
            } else if (treeType === 'Дуб') {
                treeCoeff = 2;
            } else if (treeType === 'Махонь') {
                treeCoeff = 6.5;
            }
        }
    }

}

function AjaxFormRequest(result_id) {
    var name = $('input[name*="name"]').val();
    var phone = $('input[name*="phone"]').val();
    var mail = $('input[name*="mail"]').val();
    if (name == "" || phone == "" || mail == "") {
        alert('Заповніть форму')
    } else {
        $.ajax({
            type: "POST",
            url: "send.php",
            data: {
                name: name,
                phone: phone,
                mail: mail,
                treeType: treeType,
                sumPrice: sumPrice,
                winWidth: winWidth,
                winHeight: winHeight,
                winModel: winModel,
                winAmt: winAmt,
                sumPrice: sumPrice,
                optionPidvikon: optionPidvikon,
                optionDemontazh: optionDemontazh,
                optionMontazh: optionMontazh,
            }
        }).done(function (msg) {
            $('#' + result_id).html(msg);
        });
        document.getElementsByClassName('options2')[0].style.display = 'none';
        document.getElementsByClassName('options3')[0].style.display = 'block';
        document.getElementById('SignupForm').reset();
    }
}

function Step22() {
    document.getElementsByClassName('selectmodel')[0].style.display = 'none';
    document.getElementsByClassName('sizes')[0].style.display = 'block';
}

function Step2() {
    setTimeout(Step22, 100);
}


function Step3() {
    var winAmt = document.getElementById("kolvo").value,
        widthFromForm = document.getElementById("height"),
        winWidth = widthFromForm.options[widthFromForm.selectedIndex].value,
        heightFromForm = document.getElementById("width"),
        winHeight = heightFromForm.options[heightFromForm.selectedIndex].value;

    winAmt = parseInt(winAmt);
    winWidth = parseInt(winWidth);
    winHeight = parseInt(winHeight);

    if (winAmt < 1) {
        alert('Amount should be higher than 0');
        return;
    }

    if (winHeight === 0 || winWidth === 0) {
        alert('Height and/or width should be higher than 0');
        return;
    }

    if (!winAmt || !winHeight || !winWidth) {
        alert('Please enter sizes and amount');
        return;
    }

    document.getElementsByClassName('sizes')[0].style.display = 'none';
    document.getElementsByClassName('options')[0].style.display = 'block';
}

function Step4() {
    document.getElementsByClassName('options')[0].style.display = 'none';
    document.getElementsByClassName('options2')[0].style.display = 'block';

}

function Step5() {
    document.getElementsByClassName('options2')[0].style.display = 'none';
    document.getElementsByClassName('options3')[0].style.display = 'block';

}

function backStep1() {
    document.getElementsByClassName('selectTree')[0].style.display = 'block';
    document.getElementsByClassName('selectmodel')[0].style.display = 'none';

}

function backStep2() {
    document.getElementsByClassName('selectmodel')[0].style.display = 'block';
    document.getElementsByClassName('sizes')[0].style.display = 'none';
}

function backStep3() {
    document.getElementsByClassName('sizes')[0].style.display = 'block';
    document.getElementsByClassName('options')[0].style.display = 'none';
}

function backStep4() {
    document.getElementsByClassName('options')[0].style.display = 'block';
    document.getElementsByClassName('options2')[0].style.display = 'none';

}

function backStep5() {
    document.getElementsByClassName('selectTree')[0].style.display = 'block';
    document.getElementsByClassName('options3')[0].style.display = 'none';

}

function closeCalc() {
    document.getElementById('windowcalc').style.display = 'none';
    document.getElementById('SignupForm').reset();
    document.getElementsByClassName('selectTree')[0].style.display = 'block';
    document.getElementsByClassName('selectmodel')[0].style.display = 'none';
    document.getElementsByClassName('sizes')[0].style.display = 'none';
    document.getElementsByClassName('options')[0].style.display = 'none';
    document.getElementsByClassName('options2')[0].style.display = 'none';
    document.getElementsByClassName('options3')[0].style.display = 'none';

}


(function () {

    function init() {
        initEvents();
    }

    function initEvents() {
        $(function () {
            var quantityInput = $('input[name="kolvo"]');

            quantityInput.on('focusout', function (e) {
                var value = parseInt(quantityInput.val());

                if (value < 0) {
                    quantityInput.val(0);
                }
            });

            quantityInput.keypress(function (e) {
                var chr = String.fromCharCode(e.which);

                if ("0123456789".indexOf(chr) < 0) {
                    return false;
                }
            });
        });

    }

    init();

})();