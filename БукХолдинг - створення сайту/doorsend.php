<?php
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="test.xlsx"');
header('Cache-Control: max-age=0');

$name = $_POST["name"];
$phone = $_POST["phone"];
$mail = $_POST["mail"];
$treeType = $_POST["treeType"];
$doorModel = $_POST["doorModel"];
$doorType = $_POST["doorType"];
$doorAmt = $_POST["doorAmt"];
$dHeightH = $_POST["dHeightH"];
$dWidthH = $_POST["dWidthH"];
#$winoptions = $_POST["winoptions"];
$sumPrice = $_POST["sumPrice"];
/*$optionPidvikon = $_POST["optionPidvikon"];
$optionDemontazh = $_POST["optionDemontazh"];
$optionMontazh = $_POST["optionMontazh"];*/


$sendto = "vashkiv@ukr.net";
$subject = "Нове замовлення";
$msg =
    "Ім'я: " . $name . "\r\n" .
    "Телефон: " . $phone . "\r\n" .
    "Пошта: " . $mail . "\r\n" .
    "Дерево: " . $treeType . "\r\n" .
    "Модель: " . $doorModelOutt . $doorModel . "\r\n" .
    "Висота: " . $dHeightH . "\r\n" .
    "Ширина: " . $dWidthH . "\r\n" .
    "Кількість: " . $doorAmt . "\r\n" .
    "Ціна: " . $sumPrice . "\r\n" . "Опції: " . $optionPidvikon . " " . $optionDemontazh . " " . $optionMontazh . "\r\n"
;

$headers .= "Content-Type: text/plain;charset=utf-8 \r\n";


//if (mail($sendto, $subject, $msg, $headers)) {
//    echo "ok";
//} else {
//    echo "no";
//};


echo "Ім'я: " . $name . "<br>" .
    "Телефон: " . $phone . "<br>" .
    "Пошта: " . $mail . "<br>" .
    "Дерево: " . $treeType . "<br>" .
    "модель: " . $doorModelOutt . $doorModel . "<br>" .
    "Висота: " . $dHeight . "<br>" .
    "Ширина: " . $dWidth . "<br>" .
    "Кількість: " . $doorAmt . "<br>";


require_once 'php/PHPExcel.php'; // Подключаем библиотеку PHPExcel
$phpexcel = new PHPExcel(); // Создаём объект PHPExcel
/* Каждый раз делаем активной 1-ю страницу и получаем её, потом записываем в неё данные */
$page = $phpexcel->setActiveSheetIndex(0);


$arHeadStyle = array(
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'BFBFBF')
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER
    ),
);

$arFontStyle = array(
    'font' => array(
        'bold' => true,
        'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE,
        'name' => 'arial',
        'size' => '10',
    ),
);

$arBorderStyle = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
    'font' => array(
        'bold' => true,
        'size' => 10,
        'name' => arial,
    ),
);

$arItogoStyle = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
    'font' => array(
        'bold' => true,
        'size' => 11,
        'name' => arial,
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT
    ),
);

$arItogooStyle = array(

    'font' => array(
        'bold' => true,
        'size' => 11,
        'name' => arial,
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT
    ),
);

$arBorderrStyle = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
    'font' => array(
        'bold' => false,
        'size' => 10,
        'name' => arial,
    ),
);

$arDataDoStyle = array(

    'font' => array(
        'bold' => true,
        'size' => 10,
        'name' => arial,
    ),

    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT
    ),
);

$arRightStyle = array(

    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT
    ),
);

$arAlignStyle = array(
    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER
    ),
    'font' => array(
        'bold' => true,
        'size' => 12,
        'name' => arial,
    ),
);

$arVypysavStyle = array(
    'borders' => array(
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);

$page->getStyle('A16:I16')->applyFromArray($arHeadStyle);
$page->getStyle('B1')->applyFromArray($arFontStyle);
$page->getStyle('B8')->applyFromArray($arFontStyle);
$page->getStyle('B10')->applyFromArray($arFontStyle);
$page->getStyle('B11')->applyFromArray($arFontStyle);
$page->getStyle('A16:I16')->applyFromArray($arBorderStyle);
$page->getStyle('A17:I17')->applyFromArray($arBorderrStyle);
$page->getStyle('I18:I20')->applyFromArray($arItogoStyle);
$page->getStyle('H18:H20')->applyFromArray($arItogooStyle);
$page->getStyle('B10')->applyFromArray($arFontStyle);
$page->getStyle('B11')->applyFromArray($arFontStyle);
$page->getStyle('A13')->applyFromArray($arAlignStyle);
$page->getStyle('A14')->applyFromArray($arAlignStyle);
$page->getStyle('H29')->applyFromArray($arDataDoStyle);
$page->getStyle('G26:H26')->applyFromArray($arVypysavStyle);

$page->getStyle('I17')->applyFromArray($arRightStyle);
$page->getStyle('H17')->applyFromArray($arRightStyle);


$page->mergeCells('C1:H1');
$page->mergeCells('C3:H3');
$page->mergeCells('A13:I13');
$page->mergeCells('A14:I14');
$page->mergeCells('C16:E16');
$page->mergeCells('C17:E17');

$page->getColumnDimension('A')->setWidth(4);
$page->getColumnDimension('B')->setWidth(15);
$page->getColumnDimension('C')->setWidth(17);
$page->getColumnDimension('D')->setWidth(13);
$page->getColumnDimension('E')->setWidth(11);
$page->getColumnDimension('F')->setWidth(6);
$page->getColumnDimension('G')->setWidth(10);
$page->getColumnDimension('H')->setWidth(15);
$page->getColumnDimension('I')->setWidth(18);

for ($i = 1; $i < 13; $i++) {
    $page->getRowDimension($i)->setRowHeight(13);
};
for ($k = 15; $k > 15, $k < 19; $k++) {
    $page->getRowDimension($k)->setRowHeight(13);
};


$PDV = $sumPrice / 5;
$sumaBezPDV = $sumPrice - $PDV;
$cinaBezPDV = $sumaBezPDV / $doorAmt;

$sumPrice = number_format($sumPrice, 2, ',', '');
$PDV = number_format($PDV, 2, ',', '');
$sumaBezPDV = number_format($sumaBezPDV, 2, ',', '');
$cinaBezPDV = number_format($cinaBezPDV, 2, ',', '');


// Делаем активной первую страницу и получаем её
$page->setCellValue("B1", "Постачальник"); // Добавляем в ячейку A1 слово "Hello"
$page->setCellValue("C1", 'ТзОВ торгово-промислова компанія "Бук-Холдінг"'); // Добавляем в ячейку A2 слово "World!"
$page->setCellValue("C2", "ЄДРПОУ 30538776, тел. 032 2926174");
$page->setCellValue("C3", 'Р/р 26003000005236 в  ПАТ "УКРСОЦБАНК" МФО 300023');
$page->setCellValue("C4", "ІПН 305387713045, номер свідоцтва 17831691");
$page->setCellValue("C5", "Є платником податку на прибуток на загальних підставах");
$page->setCellValue("C6", "Адреса вул. Коперніка 20/3 м.Львів 79000");
$page->setCellValue("B8", "Одержувач");
$page->setCellValue("C8", $name);
$page->setCellValue("C9", 'тел. ' . $phone);
$page->setCellValue("B10", "Платник");
$page->setCellValue("C10", "той же");
$page->setCellValue("B11", "Замовлення");
$page->setCellValue("C11", 'замовлення (Дверні конструкції)');
$page->setCellValue("A13", 'Рахунок-фактура № БХФ-000049');
$page->setCellValue("A14", 'від 31 Липня 2015 р.');
$page->setCellValue("A16", '№');
$page->setCellValue("B16", 'Артикул');
$page->setCellValue("C16", 'Назва');
$page->setCellValue("F16", 'Од.');
$page->setCellValue("G16", 'Кількість');
$page->setCellValue("H16", 'Ціна без ПДВ');
$page->setCellValue("I16", 'Сума без ПДВ');
$page->setCellValue("A17", '1');
$page->setCellValue("C17", $doorType . ' ' . $dWidthH . '*' . $dHeightH);
$page->setCellValue("F17", 'шт.');
$page->setCellValue("A22", 'Всього на суму:');
$page->setCellValue("A23", '');
$page->setCellValue("A24", 'ПДВ:      ' . $PDV . ' грн.');
$page->setCellValue("E26", 'Виписав(ла): ');
$page->setCellValue("H29", 'Рахунок дійсний до сплати до 7.08.15');

$page->setCellValue("G17", $doorAmt);
$page->setCellValue("H17", $cinaBezPDV);
$page->setCellValue("I17", $sumaBezPDV);


$page->setCellValue("I18", $sumaBezPDV);
$page->setCellValue("I19", $PDV);
$page->setCellValue("I20", $sumPrice);

$page->setCellValue("H18", 'Разом без ПДВ:');
$page->setCellValue("H19", 'ПДВ:');
$page->setCellValue("H20", 'Всього з ПДВ:');


//$page->getColumnDimension('B')->setAutoSize(true);
$page->setTitle("Замовлення"); // Ставим заголовок "Test" на странице
/* Начинаем готовиться к записи информации в xlsx-файл */

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');


/* Записываем в файл */
$objWriter->save("test.xlsx");

/**
 * Response from the server.
 * @param string $data
 * @param int $status
 * @param string $errorMessage
 */
function _response($data, $status = 200, $errorMessage = '') {
    $errorMessage = ($errorMessage) ? $errorMessage : _requestStatus($status);
    header("HTTP/1.1 " . $status . " " . $errorMessage);
    echo json_encode($data, JSON_NUMERIC_CHECK);
}

/**
 * Returns default message text on status code.
 * @param int $code
 * @return string
 */
function _requestStatus($code) {
    $status = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Moved Temporarily',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Time-out',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Large',
        415 => 'Unsupported Media Type',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Time-out',
        505 => 'HTTP Version not supported'
    );

    return ($status[$code]) ? $status[$code] : 'Code message not specified.';
}

?>
