<?php
  require_once 'php/PHPExcel.php'; // Подключаем библиотеку PHPExcel
  $phpexcel = new PHPExcel(); // Создаём объект PHPExcel
  /* Каждый раз делаем активной 1-ю страницу и получаем её, потом записываем в неё данные */
  $page = $phpexcel->setActiveSheetIndex(0);


  $arHeadStyle = array(
    'fill'  => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'BFBFBF')
      ),
    'alignment'  => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER
      ),
    );

  $arFontStyle = array(
    'font'  => array(
        'bold'  => true,
        'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE,
        'name' => 'arial',
        'size' => '10',
      ),
    );

  $arBorderStyle = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
          ),
        ),
        'font'=>array(
          'bold' => true,
          'size' => 10,
          'name' => arial,
          ),
  );

  $arItogoStyle = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
          ),
        ),
        'font'=>array(
          'bold' => true,
          'size' => 11,
          'name' => arial,
          ),
        'alignment'  => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT
      ),
  );

  $arItogooStyle = array(
        
        'font'=>array(
          'bold' => true,
          'size' => 11,
          'name' => arial,
          ),
        'alignment'  => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT
      ),
  );

  $arBorderrStyle = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
          ),
        ),
        'font'=>array(
          'bold' => false,
          'size' => 10,
          'name' => arial,
          ),
  );

   $arAlignStyle = array(
    'alignment'  => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER
      ),
    'font'=>array(
      'bold' => true,
      'size' => 12,
      'name' => arial,
      ),
    );

  $page->getStyle('A16:I16')->applyFromArray($arHeadStyle);
  $page->getStyle('B1')->applyFromArray($arFontStyle);
  $page->getStyle('B8')->applyFromArray($arFontStyle);
  $page->getStyle('B10')->applyFromArray($arFontStyle);
  $page->getStyle('B11')->applyFromArray($arFontStyle);
  $page->getStyle('A16:I16')->applyFromArray($arBorderStyle);
  $page->getStyle('A17:I17')->applyFromArray($arBorderrStyle);
  $page->getStyle('I18:I20')->applyFromArray($arItogoStyle);
  $page->getStyle('H18:H20')->applyFromArray($arItogooStyle);




  $page->getStyle('A13')->applyFromArray($arAlignStyle);
  $page->getStyle('A14')->applyFromArray($arAlignStyle);


  $page->mergeCells('C1:H1');
  $page->mergeCells('C3:H3');
  $page->mergeCells('A13:I13');
  $page->mergeCells('A14:I14');
  $page->mergeCells('C16:E16');
  $page->mergeCells('C17:E17');

  $page->getColumnDimension('A')->setWidth(4);
  $page->getColumnDimension('B')->setWidth(15);
  $page->getColumnDimension('C')->setWidth(17);
  $page->getColumnDimension('D')->setWidth(13);
  $page->getColumnDimension('E')->setWidth(11);
  $page->getColumnDimension('F')->setWidth(6);
  $page->getColumnDimension('G')->setWidth(10);
  $page->getColumnDimension('H')->setWidth(15);
  $page->getColumnDimension('I')->setWidth(18);

  for ($i=1; $i < 13; $i++) { 
    $page->getRowDimension($i)->setRowHeight(13);
  };
  for ($k=15; $k > 15, $k < 19; $k++) { 
    $page->getRowDimension($k)->setRowHeight(13);
  };







   // Делаем активной первую страницу и получаем её
  $page->setCellValue("B1", "Постачальник"); // Добавляем в ячейку A1 слово "Hello"
  $page->setCellValue("C1", 'ТзОВ торгово-промислова компанія "Бук-Холдінг"'); // Добавляем в ячейку A2 слово "World!"
  $page->setCellValue("C2", "ЄДРПОУ 30538776, тел. 032 2926174");
  $page->setCellValue("C3", 'Р/р 26003000005236 в  ПАТ "УКРСОЦБАНК" МФО 300023');
  $page->setCellValue("C4", "ІПН 305387713045, номер свідоцтва 17831691");
  $page->setCellValue("C5", "Є платником податку на прибуток на загальних підставах");
  $page->setCellValue("C6", "Адреса вул. Коперніка 20/3 м.Львів 79000");
  $page->setCellValue("B8", "Одержувач");
  $page->setCellValue("C8", 'ТзОВ "Ріля Україна "');
  $page->setCellValue("C9", "тел.");
  $page->setCellValue("B10", "Платник");
  $page->setCellValue("C10", "той же");
  $page->setCellValue("B11", "Замовлення");
  $page->setCellValue("C11", '');
  $page->setCellValue("A13", 'Рахунок-фактура № БХФ-000049');
  $page->setCellValue("A14", 'від 30 Липня 2015 р.');
  $page->setCellValue("A16", '№');
  $page->setCellValue("B16", 'Артикул');
  $page->setCellValue("C16", 'Назва');
  $page->setCellValue("F16", 'Од.');
  $page->setCellValue("G16", 'Кількість');
  $page->setCellValue("H16", 'Ціна без ПДВ');
  $page->setCellValue("I16", 'Сума без ПДВ');
  $page->setCellValue("A17", '1');
  $page->setCellValue("C17", 'Брикети паливні');
  $page->setCellValue("F17", 'т.');
  $page->setCellValue("G17", '5,000');
  $page->setCellValue("H17", '1150,00');
  $page->setCellValue("I17", '5750,00');

  $page->setCellValue("I18", '5750,00');
  $page->setCellValue("I19", '1150,00');
  $page->setCellValue("I20", '6900,00');

  $page->setCellValue("H18", 'Разом без ПДВ:');
  $page->setCellValue("H19", 'ПДВ:');
  $page->setCellValue("H20", 'Всього з ПДВ:');

  
  //$page->getColumnDimension('B')->setAutoSize(true);
  $page->setTitle("Test"); // Ставим заголовок "Test" на странице
  /* Начинаем готовиться к записи информации в xlsx-файл */  
  $objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');

  /* Записываем в файл */
  $objWriter->save("test.xlsx");
?>