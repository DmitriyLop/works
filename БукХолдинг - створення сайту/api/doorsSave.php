﻿<?php
/**
 * Created by PhpStorm.
 * User: mitfity
 * Date: 01.09.2015
 * Time: 4:00
 */

require_once 'vendor/Underscore-php/underscore.php';

// include DOMPDF's default configuration
require_once 'vendor/dompdf/dompdf_config.inc.php';

$name = $_GET["name"];
$phone = $_GET["phone"];
$mail = $_GET["mail"];
$treeType = $_GET["treeType"];
$doorModel = $_GET["doorModel"];
$doorType = $_GET["doorType"];
$doorAmt = $_GET["doorAmt"];
$dHeightH = $_GET["dHeightH"];
$dWidthH = $_GET["dWidthH"];
$sumPrice = $_GET["sumPrice"];
$price = $_GET["price"];
$sumInWords = mb_ucfirst(num2str($sumPrice), "utf8");
$factureNumber = sprintf('%06d', getFactureNumber());
$otherOptions = implode( ", ", explode( ",", $_GET["otherOptions"] ) );

$sendTo = "info@bukholding.com.ua, $mail";
$subject = "Нове замовлення";
$msg =
    "Ім'я: " . $name . "\r\n" .
    "Телефон: " . $phone . "\r\n" .
    "Пошта: " . $mail . "\r\n" .
    "Дерево: " . $treeType . "\r\n" .
    "Тип двері: " . $doorType . "\r\n" .
    "Модель: " . $doorModel . "\r\n" .
    "Висота: " . $dHeightH . "\r\n" .
    "Ширина: " . $dWidthH . "\r\n" .
    "Кількість: " . $doorAmt . "\r\n" .
    "Ціна: " . $sumPrice . "\r\n" .
    "Опції: " . $otherOptions . "\r\n"
;
$mailHeaders = "Content-Type: text/plain;charset=utf-8 \r\n";
// do not echo anything because you will distort pdf file
mail($sendTo, $subject, $msg, $mailHeaders);


header('Content-Type: charset=utf-8');
setlocale(LC_ALL, 'Ukrainian');
$currentDate = strftime("%d %m %Y р.", time());
$pastAWeek = strftime("%d %m %Y р.", strtotime("+7 day"));


$file = file_get_contents("templates/invoice.html");

$template = __::template($file);
$html = $template(
    array(
        'name' => $name,
        'phone' => $phone,
        'email' => $mail,
        'doorType' => $doorType,
        'doorModel' => $doorModel,
        'doorAmount' => $doorAmt,
        'price' => $price,
        'sumPrice' => $sumPrice,
        'dHeightH' => $dHeightH,
        'dWidthH' => $dWidthH,
        'sumInWords' => $sumInWords,
        'factureNumber' => $factureNumber,
        'currentDate' => $currentDate,
        'pastAWeek' => $pastAWeek
    )
);

header("Content-type:application/pdf");
//header("Content-Disposition:attachment;filename='sample.pdf'");

//$html = mb_convert_encoding($html, 'HTML-ENTITIES', "UTF-8");
$dompdf = new DOMPDF();
$dompdf->load_html($html, "UTF-8");
$dompdf->render();
$dompdf->stream("doorsInvoice.pdf");

function getFactureNumber() {
    $factureFile = fopen("factureNumber.txt", "r");
    $factureNumber = 0;

    if (!$factureFile) {
        $factureFile = fopen("factureNumber.txt", "w") or die("Unable to open file!");
        $txt = 1;
        fwrite($factureFile, $txt);
        fclose($factureFile);

        return $txt;
    }

    $factureNumber = (int) fread($factureFile, filesize("factureNumber.txt"));
    fclose($factureFile);

    $lastInvoiceFile = fopen("lastInvoice.txt", "r");
    $lastInvoice = 0;
    if (!$lastInvoiceFile) {
        $lastInvoiceFile = fopen("lastInvoice.txt", "w") or die("Unable to open file!");
        $txt = time();
        fwrite($lastInvoiceFile, $txt);
        fclose($lastInvoiceFile);
        $lastInvoice = $txt;
    }
    $lastInvoice = (int) fread($lastInvoiceFile, filesize("lastInvoice.txt"));
    fclose($lastInvoiceFile);

    if (date("w") == 1 && $lastInvoice < strtotime( "previous monday" )) {
        $factureFile = fopen("factureNumber.txt", "w") or die("Unable to open file!");
        $txt = 1;
        fwrite($factureFile, $txt);
        fclose($factureFile);
        $factureNumber = $txt;

        $lastInvoiceFile = fopen("lastInvoice.txt", "w") or die("Unable to open file!");
        $txt = time();
        fwrite($lastInvoiceFile, $txt);
        fclose($lastInvoiceFile);

        return $factureNumber;
    }

    $factureNumber += 1;
    $factureFile = fopen("factureNumber.txt", "w") or die("Unable to open file!");
    fwrite($factureFile, $factureNumber);
    fclose($factureFile);

    $lastInvoiceFile = fopen("lastInvoice.txt", "w") or die("Unable to open file!");
    $txt = time();
    fwrite($lastInvoiceFile, $txt);
    fclose($lastInvoiceFile);

    return $factureNumber;
}

/**
 * Возвращает сумму прописью
 * @uses morph(...)
 */
function num2str($num) {
    $nul='ноль';
    $ten=array(
        array('','одна','дві','три','чотири','п’ять','шість','сім', 'вісім','дев’ять'),
        array('','одна','дві','три','чотири','п’ять','шість','сім', 'вісім','дев’ять'),
    );
    $a20=array('десять','одинадцять','дванадцять','тринадцять','чотирнадцять' ,'п’ятнадцять','шістнадцять',
        'сімнадцять','вісімнадцять','дев’ятнадцять');
    $tens=array(2=>'двадцять','тридцять','сорок','п’ятдесят','шістдесят','сімдесят' ,'вісімдесят','дев’яносто');
    $hundred=array('','сто','двісті','триста','чотириста','п’ятсот','шістсот', 'сімсот','вісімсот','дев’ятьсот');
    $unit=array( // Units
        array('копійки' ,'копійки' ,'копійок',  1),
        array('гривня'   ,'гривні'   ,'гривень'    ,0),
        array('тисяча'  ,'тисячі'  ,'тисяч'     ,1),
        array('мільйон' ,'мільйона','мільйонів' ,0),
        array('мільярд','мільярда','мільярдів',0),
    );
    //
    list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
    $out = array();
    if (intval($rub)>0) {
        foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
            if (!intval($v)) continue;
            $uk = sizeof($unit)-$uk-1; // unit key
            $gender = $unit[$uk][3];
            list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
            // mega-logic
            $out[] = $hundred[$i1]; # 1xx-9xx
            if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
            else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
            // units without rub & kop
            if ($uk>1) $out[]= morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
        } //foreach
    }
    else $out[] = $nul;
    $out[] = morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
    $out[] = $kop.' '.morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
    return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
}

/**
 * Склоняем словоформу
 */
function morph($n, $f1, $f2, $f5) {
    $n = abs(intval($n)) % 100;
    if ($n>10 && $n<20) return $f5;
    $n = $n % 10;
    if ($n>1 && $n<5) return $f2;
    if ($n==1) return $f1;
    return $f5;
}

/**
 * ucfirst with multibyte support
 * @param $string
 * @param $encoding
 * @return string
 */
function mb_ucfirst($string, $encoding)
{
    $strlen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strlen - 1, $encoding);
    return mb_strtoupper($firstChar, $encoding) . $then;
}

function _response($data, $status = 200, $errorMessage = '') {
    $errorMessage = ($errorMessage) ? $errorMessage : _requestStatus($status);
    header("HTTP/1.1 " . $status . " " . $errorMessage);
    echo json_encode($data, JSON_NUMERIC_CHECK);
}

function _requestStatus($code) {
    $status = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Moved Temporarily',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Time-out',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Large',
        415 => 'Unsupported Media Type',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Time-out',
        505 => 'HTTP Version not supported'
    );

    return ($status[$code]) ? $status[$code] : 'Code message not specified.';
}
